package esb.navitas.smartherm;

import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import android.util.Log;

public class Again_Crypt {

	static char[] HEX_CHARS = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
			'9', 'a', 'b', 'c', 'd', 'e', 'f' };

	String mString_probeid;
	private String iv = "fedcba987654ashu";// Dummy iv (CHANGE IT!)
	private IvParameterSpec ivspec;
	private SecretKeySpec keyspec;
	private Cipher cipher;

	private String SecretKey = "0123456789abashu";// Dummy secretKey (CHANGE
													// IT!)

	public static String key_optimize(String key, int limit_length, char change) {
		String new_key = key;
		int key_length = key.length();
		if (key_length > 16) {
			new_key = key.substring(0, 16);
		} else if (key_length < 16) {
			new_key = String.format("%1$-" + limit_length + "s", key).replace(
					' ', change);
			// System.out.printf(padRight(key, 16));

			Log.d("new_key====", "" + new_key);

		}

		return new_key;

	}

	public Again_Crypt() {
		if (Bussiness_Id_Class.mString_probeid != null) {
			try {
				//Log.i("After Encryption...", Bussiness_Id_Class.mString_probeid);
				// iv=key_optimize(Bussiness_Id_Class.mString_probeid,16,'F');
				ivspec = new IvParameterSpec(iv.getBytes());
				// iv = Bussiness_Id_Class.mString_probeid.substring(0, 16);
				//Log.i("After Encryption...", iv);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Log.d("iv id exist:==", "" + iv);
			try {
				// SecretKey = Bussiness_Id_Class.mString_probeid.substring(0,
				// 16)
				SecretKey = key_optimize(Bussiness_Id_Class.mString_probeid,
						16, 'F');

			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Log.d("SecretKey id exist:==", "" + SecretKey);
			ivspec = new IvParameterSpec(iv.getBytes());

			keyspec = new SecretKeySpec(SecretKey.getBytes(), "AES");

			try {
				cipher = Cipher.getInstance("AES/CBC/NoPadding");
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			Log.d("iv id not exist:==", "" + iv);
			Log.d("SecretKey id not exist:==", "" + SecretKey);
			ivspec = new IvParameterSpec(iv.getBytes());

			keyspec = new SecretKeySpec(SecretKey.getBytes(), "AES");

			try {
				cipher = Cipher.getInstance("AES/CBC/NoPadding");
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public byte[] encrypt(String text) throws Exception {
		if (text == null || text.length() == 0)
			throw new Exception("Empty string");

		byte[] encrypted = null;

		try {
			cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);

			encrypted = cipher.doFinal(padString(text).getBytes());
		} catch (Exception e) {
			throw new Exception("[encrypt] " + e.getMessage());
		}
		//Log.i("encryptedencryptedencryptedencryptedencrypted==", "" + encrypted);

		return encrypted;
	}

	public byte[] decrypt(String code) throws Exception {
		if (code == null || code.length() == 0)
			throw new Exception("Empty string");

		byte[] decrypted = null;

		try {
			cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);

			decrypted = cipher.doFinal(hexToBytes(code));
			// Remove trailing zeroes
			if (decrypted.length > 0) {
				int trim = 0;
				for (int i = decrypted.length - 1; i >= 0; i--)
					if (decrypted[i] == 0)
						trim++;

				if (trim > 0) {
					byte[] newArray = new byte[decrypted.length - trim];
					System.arraycopy(decrypted, 0, newArray, 0,
							decrypted.length - trim);
					decrypted = newArray;
				}
			}
			//Log.i("decrypted before exception==", "" + decrypted);
		} catch (Exception e) {
			throw new Exception("[decrypt] " + e.getMessage());
		}

		//Log.i("decrypted before decrypted==", "" + decrypted);
		return decrypted;
	}

	public static String bytesToHex(byte[] buf) {
		char[] chars = new char[2 * buf.length];
		for (int i = 0; i < buf.length; ++i) {
			chars[2 * i] = HEX_CHARS[(buf[i] & 0xF0) >>> 4];
			chars[2 * i + 1] = HEX_CHARS[buf[i] & 0x0F];
		}
		return new String(chars);
	}

	public static byte[] hexToBytes(String str) {
		if (str == null) {
			return null;
		} else if (str.length() < 2) {
			return null;
		} else {
			int len = str.length() / 2;
			byte[] buffer = new byte[len];
			for (int i = 0; i < len; i++) {
				buffer[i] = (byte) Integer.parseInt(
						str.substring(i * 2, i * 2 + 2), 16);
			}
			return buffer;
		}
	}

	private static String padString(String source) {
		char paddingChar = 0;
		int size = 16;
		int x = source.length() % size;
		int padLength = size - x;

		for (int i = 0; i < padLength; i++) {
			source += paddingChar;
		}

		return source;
	}
}
