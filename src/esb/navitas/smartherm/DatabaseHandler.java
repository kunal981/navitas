package esb.navitas.smartherm;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "navitas.db";

	// Contacts table name navitas
	private static final String TABLE_LISTING = "listing";
	private static final String TABLE_FOOD = "foodlist";
	private static final String TABLE_SUPPLIER = "supplier";

	private static final String TABLE_ITEM_LIST = "fooditemlist";

	private static final String TABLE_SERVICES = "services";
	
	
	private static final String TABLE_CHILLED="chilled";
	
	
	
	private static final String TABLE_OFFLINE="tableoffline";
	// column name=========
	private static final String LISTING_UNIQUE_ID = "_uniqueidlisting";

	private static final String LISTING_EMAIL = "_email";
	private static final String LISTING_ID = "_id";
	private static final String LISTING_USERNAME = "_username";
	private static final String LISTING_IMAGE = "_image";

	// column name=========
	private static final String FOOD_UNIQUE_ID = "_uniqueidfood";
	private static final String FOOD_LOCALMANAGERID = "_localmanagerid";
	private static final String FOOD_ITEM = "_item";
	private static final String FOOD_GROUP_ID = "_groupid";
	private static final String FOOD_ITEM_ID = "_itemid";
	private static final String FOOD_GROUP = "_group";
	private static final String FOOD_ITEM_CHILL = "_itemchill";

	// column name=========
	private static final String SUPPLIER_UNIQUE_ID = "_uniqueidsupplier";
	private static final String SUPPLIER_EMAIL = "_supplieremail";
	private static final String SUPPLIER_NAME = "_suppliername";

	private static final String SUPPLIER_IMAGE = "_supplierimage";

	private static final String NEW_FOOD_GROUP_ID = "_newfoodgroupid";
	private static final String NEW_FOOD_ITEM_ID = "_newfooditemid";
	private static final String NEW_FOOD_ITEM_NAME = "_newitemname";

	private static final String UNIQUE_NEW_FOOD_ITEM_NAME = "_unique";

	// services all columns===
	private static final String SERVICES_ID = "_servicesid";

	private static final String UNIQUE_SERVICES_ID = "_uniqueservicesid";

	private static final String SERVICES_NAME = "_servicesname";
	
	
	
	private static final String CHILL_ITEM_ID = "_itemid";
	
	private static final String CHILL_USER_ID = "_userid";
	private static final String CHILL_TIME = "_chilltime";
	
	


	private static final String MSTRING_ENC_PROBEID = "probeid";
	
	private static final String MSTRING_ENC_FIANLPIN = "finalpin";
	
	private static final String MSTRING_SERVCIEID = "serviceid";
	
	private static final String MSTRING_USER_ID = "userid";
	
	private static final String MSTRING_FOODID = "foodid";
	
	private static final String MSTRING_TEMPERATURE = "temp";
	
	private static final String MSTRING_STATUS = "staus";
	
	private static final String MSTRING_UPLOAD = "upload";
	private static final String MSTRING_COLUMN_ID = "colunmid";
	


	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_LISTING + "("
				+ LISTING_UNIQUE_ID + " INTEGER PRIMARY KEY," + LISTING_ID
				+ " TEXT ," + LISTING_EMAIL + " TEXT ," + LISTING_USERNAME
				+ " TEXT ," + LISTING_IMAGE + " TEXT )";

		String CREATE_ALL_DATA_TABLE = "CREATE TABLE " + TABLE_FOOD + "("
				+ FOOD_UNIQUE_ID + " INTEGER PRIMARY KEY,"
				+ FOOD_LOCALMANAGERID + " TEXT ," + FOOD_ITEM + " TEXT ,"
				+ FOOD_ITEM_ID + " TEXT ," + FOOD_GROUP + " TEXT,"
				+ FOOD_GROUP_ID + " TEXT)";

		String CREATE_SUPPLIER_TABLE = "CREATE TABLE " + TABLE_SUPPLIER + "("
				+ SUPPLIER_UNIQUE_ID + " INTEGER PRIMARY KEY," + SUPPLIER_EMAIL
				+ " TEXT ," + SUPPLIER_NAME + " TEXT ," + SUPPLIER_IMAGE
				+ " TEXT)";

		String CREATE_FOOD_NEW_TABLE = "CREATE TABLE " + TABLE_ITEM_LIST + "("
				+ UNIQUE_NEW_FOOD_ITEM_NAME + " INTEGER PRIMARY KEY,"
				+ NEW_FOOD_GROUP_ID + " TEXT ," + NEW_FOOD_ITEM_NAME
				+ " TEXT ," + NEW_FOOD_ITEM_ID + " TEXT  ," + FOOD_ITEM_CHILL
				+ " TEXT DEFAULT FALSE)";

		String CREATE_SERVICE_TABLE = "CREATE TABLE " + TABLE_SERVICES + "("
				+ UNIQUE_SERVICES_ID + "TEXT PRIMARY KEY," + SERVICES_ID
				+ " TEXT ," + SERVICES_NAME + " TEXT)";
		
		

		String CREATE_CHILL_TABLE = "CREATE TABLE " + TABLE_CHILLED + "("
			 + CHILL_ITEM_ID + " TEXT ," + CHILL_USER_ID + " TEXT," + CHILL_TIME + " TEXT)";
		


		String CREATE_OFFLINEDATA = "CREATE TABLE " + TABLE_OFFLINE + "( " + MSTRING_COLUMN_ID
			+ " integer primary key autoincrement,"
			 + MSTRING_ENC_PROBEID + " TEXT ," + MSTRING_ENC_FIANLPIN + " TEXT ," + MSTRING_SERVCIEID + " TEXT ," + MSTRING_USER_ID + " TEXT ," + MSTRING_FOODID + " TEXT ," + MSTRING_TEMPERATURE + " TEXT ," + MSTRING_STATUS + " TEXT, " + MSTRING_UPLOAD + " TEXT DEFAULT FALSE)";
		
		
		


		db.execSQL(CREATE_CONTACTS_TABLE);

		db.execSQL(CREATE_ALL_DATA_TABLE);
		db.execSQL(CREATE_SUPPLIER_TABLE);
		db.execSQL(CREATE_FOOD_NEW_TABLE);

		db.execSQL(CREATE_SERVICE_TABLE);
		db.execSQL(CREATE_CHILL_TABLE);
		db.execSQL(CREATE_OFFLINEDATA);
		
		

		//Log.i("", "on create database");
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LISTING);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_FOOD);

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUPPLIER);

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEM_LIST);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SERVICES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHILLED);
		
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_OFFLINE);
		
		//Log.i("", "on upgrade database");
		// Create tables again
		onCreate(db);
	}

	// Adding new contact item adding===============
	public void Add_Listing_Item(String s, String s2, String s3) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(NEW_FOOD_GROUP_ID, s); // Contact Name
		values.put(NEW_FOOD_ITEM_NAME, s2);
		values.put(NEW_FOOD_ITEM_ID, s3);

		//Log.i("value from Add_Contact", s);
		// Inserting Row
		db.insert(TABLE_ITEM_LIST, null, values);
		db.close(); // Closing database connection
	}
	
	
	// Adding new contact item adding===============
	public void Add_OFFLINE_DATA(String probe_id,String pin_id,String service_id, String user_id, String food_id, String temp, String status ) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(MSTRING_ENC_PROBEID, probe_id); // Contact Name
		values.put(MSTRING_ENC_FIANLPIN, pin_id);
		values.put(MSTRING_SERVCIEID, service_id);
		values.put(MSTRING_USER_ID, user_id);
		values.put(MSTRING_FOODID, food_id);
		values.put(MSTRING_TEMPERATURE, temp);
		values.put(MSTRING_STATUS, status);

	
		// Inserting Row
		db.insert(TABLE_OFFLINE, null, values);
		db.close(); // Closing database connection
	}
	

	// adding services item in local database========
	public void Add_Service(String id_, String name_) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(SERVICES_ID, id_); // Contact Name
		values.put(SERVICES_NAME, name_);

		//Log.i("SERVICES_ID in database", id_);
		//Log.i("SERVICES_NAME in database", name_);
		// Inserting Row
		db.insert(TABLE_SERVICES, null, values);
		db.close(); // Closing database connection
	}

	/**
	 * All CRUD(Create, Read, Update, Delete) Operations
	 */

	// Adding new contact
	public void Add_Listing(String s, String s2, String s3, String s4) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(LISTING_ID, s); // Contact Name
		values.put(LISTING_EMAIL, s2);
		values.put(LISTING_USERNAME, s3);
		values.put(LISTING_IMAGE, s4);
		//Log.i("value from Add_Contact", s);
		// Inserting Row
		db.insert(TABLE_LISTING, null, values);
		db.close(); // Closing database connection
	}

	// insert all data=======
	public void Add_Food(String s, String s2, String s3, String mString_,
			String mString) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(FOOD_LOCALMANAGERID, s); // Contact Name
		values.put(FOOD_ITEM, s2);
		values.put(FOOD_ITEM_ID, s3);
		values.put(FOOD_GROUP, mString_);
		values.put(FOOD_GROUP_ID, mString);

		// Inserting Row
		db.insert(TABLE_FOOD, null, values);
		db.close(); // Closing database connection
	}

	// insert all data=======
	public void Add_Supplier(String s, String s2, String s3) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(SUPPLIER_EMAIL, s); // Contact Name
		values.put(SUPPLIER_NAME, s2);
		values.put(SUPPLIER_IMAGE, s3);

		// Inserting Row
		db.insert(TABLE_SUPPLIER, null, values);
		db.close(); // Closing database connection
	}
	
	
	// Adding new contact item adding===============
	public void Add_CHILL_ITEM(String user_id, String item_id,String Time) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(CHILL_ITEM_ID, item_id); // Contact Name
		values.put(CHILL_USER_ID, user_id);
		values.put(CHILL_TIME, Time);
		

	

		db.insert(TABLE_CHILLED, null, values);
		db.close(); // Closing database connection
	}
	
	

	public void delete_table_data() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_LISTING, null, null);
		db.delete(TABLE_FOOD, null, null);
		db.delete(TABLE_SUPPLIER, null, null);
		db.delete(TABLE_ITEM_LIST, null, null);
		// db.delete(TABLE_SERVICES, null, null);

		db.close();
	}
	
	
	// fetch services====

	public Cursor fetch_offline_data() throws SQLException {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor mCursor = db.query(false, TABLE_OFFLINE, new String[] {
				MSTRING_COLUMN_ID, MSTRING_ENC_PROBEID ,MSTRING_ENC_FIANLPIN,MSTRING_SERVCIEID,MSTRING_FOODID,MSTRING_USER_ID,MSTRING_TEMPERATURE,MSTRING_STATUS}, null,
				null, null, null, null, null);

		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	// fetch

	public Cursor fetch_notification() throws SQLException {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor mCursor = db
				.query(false, TABLE_FOOD, new String[] { FOOD_GROUP_ID,
						FOOD_GROUP }, null, null, null, null, null, null);

		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	// fetch services====

	public Cursor fetch_Services(String mString) throws SQLException {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor mCursor = db.query(false, TABLE_SERVICES, new String[] {
				SERVICES_ID, SERVICES_NAME }, SERVICES_NAME + " = ?",
				new String[] { mString }, null, null, null, null);

		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	public void REMOVE_CHILL_ITEM(String item_id,String user_id) {
		SQLiteDatabase db = this.getReadableDatabase();
	int result=db.delete(TABLE_CHILLED,
				CHILL_ITEM_ID + " = " + item_id +" AND "+CHILL_USER_ID+" = "+user_id, null);
	//Log.i("delete USerr   ",""+result+" "+user_id+" "+item_id);
	}
	
	
	
	public void REMOVE_OFFLINE_ITEM(String column_id) {
		SQLiteDatabase db = this.getReadableDatabase();
	int result=db.delete(TABLE_OFFLINE,
				MSTRING_COLUMN_ID + " = " + column_id, null);

	}
	
	public Cursor fetch_menu_item(String mString) throws SQLException {

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor mCursor = db.query(false, TABLE_FOOD, new String[] { FOOD_ITEM,
				FOOD_ITEM_ID, FOOD_GROUP_ID }, FOOD_GROUP_ID + " = ?",
				new String[] { mString }, null, null, null, null);

		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	public Cursor fetch_menu_item_list(String mString) throws SQLException {
		//Log.i("mString====", "" + mString);

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor mCursor = db.query(false, TABLE_ITEM_LIST, new String[] {
				NEW_FOOD_GROUP_ID, NEW_FOOD_ITEM_NAME, NEW_FOOD_ITEM_ID ,FOOD_ITEM_CHILL},
				NEW_FOOD_GROUP_ID + " = ?", new String[] { mString }, null,
				null, null, null);

		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}
	
	
	
	public Cursor fetch_chill_list(String mString,String user_id) throws SQLException {
		//Log.i("mString====", "" + mString);

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor mCursor = db.query(false, TABLE_CHILLED, new String[] {
				CHILL_ITEM_ID, CHILL_USER_ID,CHILL_TIME },
				CHILL_ITEM_ID + " = ? AND "+CHILL_USER_ID +" = ?", new String[] { mString,user_id }, null,
				null, null, null);

		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	public boolean update_chill_item(String string_update, String item_id) {
		ContentValues values = new ContentValues();
		values.put(FOOD_ITEM_CHILL, string_update);

		SQLiteDatabase db = this.getReadableDatabase();

		int value=db.update(TABLE_ITEM_LIST, values, NEW_FOOD_ITEM_ID + " = " + item_id,
				null);
		//Log.i("Value",""+value+" item_id "+item_id +" "+" string_update "+string_update);
		return true;
	}

	

	public Cursor fetch_supplier() throws SQLException {

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor mCursor = db.query(false, TABLE_SUPPLIER, new String[] {
				SUPPLIER_NAME, SUPPLIER_EMAIL }, null, null, null, null, null,
				null);

		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	public Cursor fetch_path() throws SQLException {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor mCursor = db.query(false, TABLE_LISTING, new String[] {
				LISTING_ID, LISTING_USERNAME }, null, null, null, null, null,
				null);

		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}
}
