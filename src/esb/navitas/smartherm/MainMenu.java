package esb.navitas.smartherm;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

//public class MainMenu extends ActionBarActivity
public class MainMenu extends Activity {
	TextView probeView;
	globals global;
	private final static int PROBE_UPDATE = 3;
	ProgressDialog mProgressDialog;
	BroadcastReceiver batteryLevelReceiver;

	private ProbeUpdateTask probeUpdateTask;

	private boolean running = false;

	// Handler to take care of UI actions called from other threads
	private Handler myHandler = new Handler() {

		/*
		 * @see android.os.Handler#handleMessage(android.os.Message)
		 */
		public void handleMessage(Message msg) {
			switch (msg.what) {
			// Check which kind of message was received to perform required
			// actions.
			case (PROBE_UPDATE):
				probeAPI.check_probe(probeView);
				global.probeID = global.newProbeID;
				break;
			}
		}
	};
	Handler handler_battery_update;
	TextView textView_battery_percent;

	private void getBatteryPercentage() {

		batteryLevelReceiver = new BroadcastReceiver() {
			public void onReceive(Context context, Intent intent) {
				context.unregisterReceiver(this);
				int currentLevel = intent.getIntExtra(
						BatteryManager.EXTRA_LEVEL, -1);
				int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
				int status = intent
						.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
				int level = -1;
				if (currentLevel >= 0 && scale > 0) {
					level = (currentLevel * 100) / scale;
				}

				textView_battery_percent.setText("" + level + "%");

				intent = context.registerReceiver(null, new IntentFilter(
						Intent.ACTION_BATTERY_CHANGED));
				// int plugged =
				// intent.getIntExtra(BatteryManager.EXTRA_PLUGGED,
				// -1);

				int status_battery = intent.getIntExtra(
						BatteryManager.EXTRA_STATUS, -1);
				boolean bCharging = status_battery == BatteryManager.BATTERY_STATUS_CHARGING;
				// boolean bCharging = status ==
				// BatteryManager.BATTERY_STATUS_CHARGING ||
				// status == BatteryManager.BATTERY_STATUS_FULL || status ==
				// BatteryManager.BATTERY_STATUS_DISCHARGING || status ==
				// BatteryManager.BATTERY_STATUS_NOT_CHARGING;

				float battery_status = level / (float) scale;
				if (battery_status <= 0.2) {

					if (bCharging) {

						imageView_battery
								.setImageResource(R.drawable.ic_action_battery_one);

					} else {

						imageView_battery
								.setImageResource(R.drawable.ic_action_battery1);
					}

				} else if (battery_status > 0.2 && battery_status <= 0.4) {

					if (bCharging) {

						imageView_battery
								.setImageResource(R.drawable.ic_action_battery_two);

					} else {

						imageView_battery
								.setImageResource(R.drawable.ic_action_battery2);
					}

				} else if (battery_status > 0.4 && battery_status <= 0.6) {

					if (bCharging) {

						imageView_battery
								.setImageResource(R.drawable.ic_action_battery_three);

					} else {

						imageView_battery
								.setImageResource(R.drawable.ic_action_battery3);
					}

				} else if (battery_status > 0.6 && battery_status <= 0.8) {

					if (bCharging) {

						imageView_battery
								.setImageResource(R.drawable.ic_action_battery_four);

					} else {

						imageView_battery
								.setImageResource(R.drawable.ic_action_battery4);
					}

				} else if (battery_status > 0.8 && battery_status < 1.0) {

					if (bCharging) {

						imageView_battery
								.setImageResource(R.drawable.ic_action_battery_five);

					} else {

						imageView_battery
								.setImageResource(R.drawable.ic_action_battery5);
					}

				} else {

					if (bCharging) {

						imageView_battery
								.setImageResource(R.drawable.ic_action_battery_six);

					} else {

						imageView_battery
								.setImageResource(R.drawable.ic_action_battery6);
					}

				}
			}

		};

		IntentFilter batteryLevelFilter = new IntentFilter(
				Intent.ACTION_BATTERY_CHANGED);
		registerReceiver(batteryLevelReceiver, batteryLevelFilter);

	}

	final Runnable r = new Runnable() {
		public void run() {
			currentTimeString = new SimpleDateFormat("hh:mm aa")
					.format(new Date());

			// textView is the TextView view that should display it
			textView_timer.setText(currentTimeString);

			// int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL,
			// -1);
			// int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE,
			// -1);
			// int status =
			// batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS,
			// BatteryManager.BATTERY_STATUS_UNKNOWN);

			boolean wifiEnabled = wifiManager.isWifiEnabled();
			if (wifiEnabled) {
				imageView_wifi.setVisibility(View.VISIBLE);
				int numberOfLevels = 5;
				WifiInfo wifiInfo = wifiManager.getConnectionInfo();
				int level_ = WifiManager.calculateSignalLevel(
						wifiInfo.getRssi(), numberOfLevels);
				System.out.println("Bars =" + level_);
				if (level_ < 2)
					imageView_wifi
							.setImageResource(R.drawable.ic_action_network_wifi_1);
				else if (level_ == 2)
					imageView_wifi
							.setImageResource(R.drawable.ic_action_network_wifi_2);
				else if (level_ == 3)
					imageView_wifi
							.setImageResource(R.drawable.ic_action_network_wifi_3);
				else
					imageView_wifi
							.setImageResource(R.drawable.ic_action_network_wifi);

			} else {
				imageView_wifi.setVisibility(View.GONE);
			}
			getBatteryPercentage();

			handler_battery_update.postDelayed(this, 1000);
		}
	};

	/**
	 * Initialises background asynchronous task that take care of listening for
	 * interrupt events on board button and perform required actions on LED GPIO
	 * and graphics.
	 */
	private void initializeTask() {
		// Set global running variable to true.

		running = true;
		// Declare task
		probeUpdateTask = new ProbeUpdateTask();
		// Start task.
		probeUpdateTask.execute();
	}

	/**
	 * Stop asynchronous background task that were taking care of checking GPIO
	 * button interrupt events.
	 */
	private void stopTask() {
		// Set global running variable to false.
		running = false;
		// Give time to propagate stop request.
		try {
			Thread.sleep(1000);

			// Cancel tasks.
			probeUpdateTask.cancel(true);
			probeUpdateTask = null;

		} catch (InterruptedException e) {
		} catch (NullPointerException e) {
			// TODO: handle exception
		}
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub

		handler_battery_update.removeCallbacksAndMessages(null);
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		// Stop background task

		stopTask();
		super.onDestroy();
	}

	/**
	 * Background asynchronous task that takes care of updating the temperature
	 * readout
	 * 
	 */
	private class ProbeUpdateTask extends AsyncTask<Void, Void, Void> {

		/*
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */
		protected Void doInBackground(Void... params) {
			while (running) {
				try {
					global.newProbeID = probeAPI.getProbeID();
					if (global.probeID != global.newProbeID) {
						myHandler.sendEmptyMessage(PROBE_UPDATE);

					}

					Thread.sleep(1000);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return (null);
		}
	}

	LinearLayout layout_active;
	TextView textView_active;
	WifiManager wifiManager;
	IntentFilter ifilter;
	Intent batteryStatus;
	TextView textView_timer;

	ImageView imageView_battery, imageView_wifi, image_view_exit;
	String currentTimeString;
	Intent my_upload_offline;
	DatabaseHandler db;

	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_main_menu);

		// R.layout.activity_main_menu
		global = (globals) getApplication();

		ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		imageView_wifi = (ImageView) findViewById(R.id.imageView_wifi);
		image_view_exit = (ImageView) findViewById(R.id.image_exit);
		textView_battery_percent = (TextView) findViewById(R.id.textView_per);
		// this.registerReceiver(mNetworkReceiver, new
		// IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)); //network
		// info
		textView_timer = (TextView) findViewById(R.id.textView_timer);
		android.provider.Settings.System.putInt(getContentResolver(),
				Settings.System.SCREEN_OFF_TIMEOUT, 120000);

		currentTimeString = new SimpleDateFormat("hh:mm aa").format(new Date());

		// textView is the TextView view that should display it
		textView_timer.setText(currentTimeString);

		wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);

		imageView_battery = (ImageView) findViewById(R.id.imageView_battery);

		handler_battery_update = new Handler();
		handler_battery_update.postDelayed(r, 0);

		probeAPI.initialise();
		// checkProbeInBackground();
		// probeAPI.setContext(MainMenu.this);
		probeAPI.runHaptic(300);
		probeView = (TextView) findViewById(R.id.txt_menu_probe);
		// Initialise application background task to check for interrupts on
		// button GPIO.
		initializeTask();
		probeAPI.check_probe(probeView);

		/*
		 * if (savedInstanceState == null) {
		 * getSupportFragmentManager().beginTransaction() .add(R.id.container,
		 * new PlaceholderFragment()) .commit(); }
		 */

		ConnectionDetector cd = new ConnectionDetector(getApplicationContext());

		Boolean isInternetPresent = cd.isConnectingToInternet(); // true or

		DatabaseHandler db = new DatabaseHandler(getApplicationContext());

		if (isInternetPresent) {
			db = new DatabaseHandler(getApplicationContext());

			Cursor mycursor;
			try {

				mycursor = db.fetch_offline_data();

				if (mycursor.getCount() == 0) {

					return;
				} else {

				}

			} catch (Exception e) {
				e.printStackTrace();

			}

			my_upload_offline = new Intent(getApplicationContext(),
					MyService.class);
			// Log.i("startingService","Here SI teh erviec");
			startService(my_upload_offline);
			registerReceiver(broadcastReceiver, new IntentFilter(
					MyService.BROADCAST_ACTION));

		}

	}

	public void max_brightness() {

		WindowManager.LayoutParams mLayoutParams = MainMenu.this.getWindow()
				.getAttributes();
		mLayoutParams.flags = WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
		// TODO Store original brightness value
		mLayoutParams.screenBrightness = -1f;
		MainMenu.this.getWindow().setAttributes(mLayoutParams);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		Intent intent;
		/*
		 * if (id == R.id.action_settings) { return true; }
		 */
		switch (id) {
		case R.id.home_menu:
			intent = new Intent(getApplicationContext(), MainMenu.class);
			startActivity(intent);
			break;
		case R.id.goods_menu:
			intent = new Intent(getApplicationContext(), goods_user.class);
			startActivity(intent);
			break;
		case R.id.cook_menu:
			intent = new Intent(getApplicationContext(), cook_user.class);
			startActivity(intent);
			break;
		case R.id.chill_menu:
			intent = new Intent(getApplicationContext(), chilling_user.class);
			startActivity(intent);
			break;
		case R.id.reheat_menu:
			intent = new Intent(getApplicationContext(), reheat_user.class);
			startActivity(intent);
			break;
		case R.id.hot_serv_menu:
			intent = new Intent(getApplicationContext(), hot_service_user.class);
			startActivity(intent);
			break;
		case R.id.cold_serv_menu:
			intent = new Intent(getApplicationContext(),
					cold_service_user.class);
			startActivity(intent);
			break;
		case R.id.cal_menu:
			intent = new Intent(getApplicationContext(), calibration_user.class);
			startActivity(intent);
			break;
		default:
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	/*
	 * public static class PlaceholderFragment extends Fragment {
	 * 
	 * public PlaceholderFragment() { }
	 * 
	 * @Override public View onCreateView(LayoutInflater inflater, ViewGroup
	 * container, Bundle savedInstanceState) { View rootView =
	 * inflater.inflate(R.layout.fragment_main_menu, container, false); return
	 * rootView; } }
	 */
	/* Called when the user clicks the Goods In button */

	public void clk_btn_menu_goods(View view) {
		// TODO Do something in response to Goods In button click
		layout_active = (LinearLayout) findViewById(R.id.layout_menu_goods);
		textView_active = (TextView) findViewById(R.id.textView_goods_in);
		textView_active.setBackgroundResource(R.drawable.home_menus_selected);
		layout_active.setBackgroundResource(R.drawable.home_menus_selected);
		textView_active.setTextColor(Color.WHITE);
		Intent intent = new Intent(this, goods_user.class);
		startActivity(intent);

	}

	public void clk_btn_app_exit(View view) {

		image_view_exit.setImageResource(R.drawable.exit_active);
		show_alert("Are You Sure to Exit ?");
	}

	/* Called when the user clicks the Cooking button */
	public void clk_btn_menu_cook(View view) {
		// TODO Do something in response to Cooking button click
		layout_active = (LinearLayout) findViewById(R.id.layout_menu_cooking);
		textView_active = (TextView) findViewById(R.id.textView_cooking);
		textView_active.setBackgroundResource(R.drawable.home_menus_selected);
		layout_active.setBackgroundResource(R.drawable.home_menus_selected);
		textView_active.setTextColor(Color.WHITE);

		Intent intent = new Intent(this, cook_user.class);
		startActivity(intent);
	}

	/* Called when the user clicks the Chilling button */
	public void clk_btn_menu_chill(View view) {
		// TODO Do something in response to Chilling button click

		layout_active = (LinearLayout) findViewById(R.id.layout_menu_chilling);
		textView_active = (TextView) findViewById(R.id.textView_chilling);
		textView_active.setBackgroundResource(R.drawable.home_menus_selected);
		layout_active.setBackgroundResource(R.drawable.home_menus_selected);
		textView_active.setTextColor(Color.WHITE);

		Intent intent = new Intent(this, chilling_user.class);
		startActivity(intent);
	}

	/* Called when the user clicks the Re-Heating button */
	public void clk_btn_menu_reheat(View view) {
		// TODO Do something in response to Re-Heating button click
		layout_active = (LinearLayout) findViewById(R.id.layout_menu_reheating);
		textView_active = (TextView) findViewById(R.id.textView_reheating);
		textView_active.setBackgroundResource(R.drawable.home_menus_selected);
		layout_active.setBackgroundResource(R.drawable.home_menus_selected);
		textView_active.setTextColor(Color.WHITE);

		Intent intent = new Intent(this, reheat_user.class);
		startActivity(intent);
	}

	/* Called when the user clicks the Hot Service button */
	public void clk_btn_menu_hot_service(View view) {
		// TODO Do something in response to Hot Service button click

		layout_active = (LinearLayout) findViewById(R.id.layout_menu_hotserv);
		textView_active = (TextView) findViewById(R.id.textView_hotserv);
		textView_active.setBackgroundResource(R.drawable.home_menus_selected);
		layout_active.setBackgroundResource(R.drawable.home_menus_selected);
		textView_active.setTextColor(Color.WHITE);

		Intent intent = new Intent(this, hot_service_user.class);
		startActivity(intent);
	}

	/* Called when the user clicks the Cold Service button */
	public void clk_btn_menu_cold_service(View view) {
		// TODO Do something in response to Cold Service button click

		layout_active = (LinearLayout) findViewById(R.id.layout_menu_coldserv);
		textView_active = (TextView) findViewById(R.id.textView_coldserv);
		textView_active.setBackgroundResource(R.drawable.home_menus_selected);
		layout_active.setBackgroundResource(R.drawable.home_menus_selected);
		textView_active.setTextColor(Color.WHITE);

		Intent intent = new Intent(this, cold_service_user.class);
		startActivity(intent);
	}

	/* Called when the user clicks the Settings image button */
	public void clk_btn_menu_settings(View view) {
		// TODO Do something in response to Setting image button click
		Intent intent = new Intent(this, calibration_user.class);
		startActivity(intent);
	}

	/*
	 * void checkProbeInBackground() { new Thread(new Runnable() { public void
	 * run() { // DO your work here try { global.newProbeID =
	 * probeAPI.getProbeID(); Thread.sleep(1000); } catch (Exception e) {
	 * e.printStackTrace(); } if (activity_is_not_in_background) {
	 * runOnUiThread(new Runnable() {
	 * 
	 * @Override public void run() { //update UI } }); }
	 * checkProbeInBackground(); } }); }
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onDestroy()
	 */

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.e("Resume outside condition", "enter layout");
		if (layout_active != null) {

			layout_active
					.setBackgroundResource(R.drawable.main_menus_background);
			textView_active
					.setBackgroundResource(R.drawable.home_menus_background);
			textView_active.setTextColor(Color.BLACK);
		}

	}

	private void show_alert(String message) {
		// TODO Auto-generated method stub
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainMenu.this);

		// Setting Dialog Title
		alertDialog.setTitle("Exit!");

		// Setting Dialog Message
		alertDialog.setMessage(message);

		// Setting Icon to Dialog
		alertDialog.setIcon(R.drawable.ic_launcher);

		// Setting Positive "Yes" Button
		alertDialog.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						image_view_exit
								.setImageResource(R.drawable.exit_default);
						dialog.cancel();
						System.exit(0);
						finish();
					}
				});

		// Setting Negative "NO" Button
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// Write your code here to invoke NO event
						image_view_exit
								.setImageResource(R.drawable.exit_default);
						dialog.cancel();
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

	// broadcast receiver==========================================
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// Log.i("On Update UI  Call", " Update ");
			stopService(my_upload_offline);

		}
	};

}
