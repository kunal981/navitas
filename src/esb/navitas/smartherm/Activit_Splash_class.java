package esb.navitas.smartherm;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import JsonParser.JSONParserPost;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.ObjectAnimator;

public class Activit_Splash_class extends Activity {
	ObjectAnimator objectAnimator;

	JSONArray jsonArray, json_Array;
	DatabaseHandler mDatabaseHandler;
	Progress_List mProgress_List;
	Cursor mCursor;
	Progress_ mProgress_;
	Progress_Supplier mProgress_Supplier;
	TextView textView;

	JSONObject json;
	JSONObject jsonObject;

	SharedPreferences mSharedPreferences, mPreferences, mPreferences2,
			mPreferences_all_data;
	public static String My_Shared = "probeid_file";
	public static String My_Shared_PIN = "pin_file";
	public static String My_Shared_Conform = "conform_screen";
	public static String My_Shared_All_Data = "All_Data";

	int private_moade = 0;
	int private_mode = 0;

	int private_mode_ = 0;
	int private_mode_all_data = 0;
	SharedPreferences.Editor editor, mEditor, mEditor2, mEditor3;
	String mString_probeid, mString_pin, mString_deviceid, mString_screen_show,
			mString_encrypt_final_probe_id, mString_encrypt_final_pin,
			mString_status_user, mString_status_supplier, mString_status_menu,
			mString_status_getservices;
	String error_message = "";
	ConnectionDetector cd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_splash);

		mDatabaseHandler = new DatabaseHandler(this);
		mPreferences2 = this.getSharedPreferences(My_Shared_Conform,
				private_mode_);
		mSharedPreferences = this
				.getSharedPreferences(My_Shared, private_moade);
		mPreferences = this.getSharedPreferences(My_Shared_PIN, private_mode);
		mPreferences_all_data = this.getSharedPreferences(My_Shared_All_Data,
				private_mode_all_data);
		editor = mSharedPreferences.edit();
		mEditor = mPreferences.edit();
		mEditor2 = mPreferences2.edit();
		mEditor3 = mPreferences_all_data.edit();
		mString_screen_show = mPreferences2.getString("Screen_Decide", null);
		mString_probeid = mSharedPreferences.getString("PROBE_ID", null);
		// Log.e("id_user:  ", "" + mString_probeid);
		mString_pin = mPreferences.getString("PIN", null);
		// Log.e("mString_pin:  ", "" + mString_pin);
		mString_deviceid = mSharedPreferences.getString("DEVICE_ID", null);

		mString_encrypt_final_probe_id = mPreferences_all_data.getString(
				"PROBEID_ENCRYPT", null);
		mString_encrypt_final_pin = mPreferences_all_data.getString(
				"PIN_ENCRYPT", null);

		textView = (TextView) findViewById(R.id.textView_warming);

		cd = new ConnectionDetector(getApplicationContext());

		// false
		// Log.e("check_inetrnet?_", "" + isInternetPresent);

		show_animation_to_TextView(textView);

	}

	/**
	 * 
	 * @param TextView
	 */
	private void show_animation_to_TextView(final TextView textview) {
		// TODO Auto-generated method stub
		objectAnimator = ObjectAnimator.ofFloat(textview, "alpha", 1f, 1f);
		objectAnimator.setDuration(10000);

		objectAnimator.addListener(new AnimatorListener() {
			public void onAnimationStart(Animator arg0) {
				// TODO Auto-generated method stub

			}

			public void onAnimationRepeat(Animator arg0) {
				// TODO Auto-generated method stub
			}

			public void onAnimationEnd(Animator arg0) {
				// TODO Auto-generated method stub
				Boolean isInternetPresentdone = cd.isConnectingToInternet(); // true
																				// or
				if (isInternetPresentdone == false) {
					if (mString_screen_show != null && mString_probeid != null
							&& mString_pin != null) {
						show_navigate("Are You sure to Proceed Offline?");

					} else
						show_alert("No Internet Connection", false);
					return;
				}
				if (mString_screen_show != null) {

				}

				// Log.e("mString_probeidmString_probeid:", "" +
				// mString_probeid);
				if (mString_probeid != null) {
					// Log.e("mString_probeid:", "" + mString_probeid);
					// Log.e("mString_pin:", "" + mString_pin);
					if (mString_pin != null) {
						// Log.e("mString_screen_show--------", ""
						// + mString_screen_show);
						if (mString_screen_show != null) {
							try {
								mCursor = mDatabaseHandler.fetch_path();
								// Log.i("value in cursor at starting",
								// mCursor.getCount() + "");
							} catch (Exception e) {

								e.printStackTrace();
							}

							// mCursor.moveToFirst();
							if (mCursor.getCount() != 0) {
								mDatabaseHandler.delete_table_data();

							}
							mCursor.close();
							mDatabaseHandler.close();
							Boolean isInternetPresent = cd
									.isConnectingToInternet(); // true or
							// false

							if (isInternetPresent == false) {
								show_alert("No Internet Connection", false);
								return;
							}

							mProgress_List = new Progress_List();
							mProgress_List.execute();
						} else {
							Intent intent_home_screen = new Intent(
									Activit_Splash_class.this,
									Refresh_Conform_Probe_Enable.class);

							startActivity(intent_home_screen);
						}

					} else {
						// Log.d("", "call intent");
						Intent intent_home_screen = new Intent(
								Activit_Splash_class.this, Pin_Screen.class);
						intent_home_screen.putExtra("ProbeId", mString_probeid);
						intent_home_screen.putExtra("DEVICEID",
								mString_deviceid);
						startActivity(intent_home_screen);

						// finish splash activity not going back to this screen
						// after
						finish();
					}

				} else {
					Intent intent_home_screen = new Intent(
							Activit_Splash_class.this, Bussiness_Id_Class.class);

					startActivity(intent_home_screen);

					// finish splash activity not going back to this screen
					// after
					finish();
				}

			}

			public void onAnimationCancel(Animator arg0) {
				// TODO Auto-generated method stub
			}
		});

		objectAnimator.start();

	}

	public class Progress_List extends AsyncTask<String, Void, String> {
		public Progress_List() {
		}

		@Override
		protected String doInBackground(String... params) {

			String URL = "http://test.gardnerjames.eu.com/api/navitas-app/get-users";
			JSONParserPost mJsonParserPost = new JSONParserPost();

			// Log.d("users encrypted=====", "" + mString_encrypt_final_pin);
			// Log.d("users encrypted=====", "" +
			// mString_encrypt_final_probe_id);

			HttpEntity se = null;
			try {

				json = new JSONObject();
				json.put("0", mString_encrypt_final_probe_id);
				json.put("1", mString_encrypt_final_pin);

				se = new StringEntity(json.toString());

				// Log.e("parent====", "" + json);
			} catch (Exception e) {

				e.printStackTrace();
			}

			try {

			} catch (Exception e) {

				e.printStackTrace();
			}

			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(URL, se);

			// //Log.i("mJsonObject string---------", "" +
			// jsonobject.toString());

			try {
				mString_status_user = jsonobject.getString("status");
				// Log.e("mString_status_user===", "" + mString_status_user);

				if (mString_status_user.equals("success")) {

					jsonArray = jsonobject.getJSONArray("result");

					// Log.e("jsonArray---------", "" + jsonArray.length());

					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);

						String mString_id = jsonObject.getString("id");
						String mString_username = jsonObject.getString("name");
						// String mString_email = jsonObject.getString("email");
						String mString_image = jsonObject.getString("avatar");

						// mArrayList_username.add(mString_username);

						mDatabaseHandler.Add_Listing(mString_id, null,
								mString_username, mString_image);

					}

				} else {
					error_message = jsonobject.getString("message");
				}
			} catch (Exception e) {
				mString_status_user = "error";
				error_message = e.getMessage();
			}

			return null;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if (mString_status_user.equals("success")) {

				editor.putString("Screen_Decide", "Menu");
				editor.commit();
				Boolean isInternetPresent = cd.isConnectingToInternet(); // true
																			// or
				// false

				if (isInternetPresent == false) {
					show_alert("No Internet Connection", false);
					return;
				}
				mProgress_ = new Progress_();
				mProgress_.execute();
			}

			else {
				try {

					if (error_message.equals("No data for this id probe."))
						show_alert(error_message, true);
					else
						show_alert(error_message, false);

				} catch (NullPointerException e) {
					// TODO: handle exception
					show_alert(
							"Unable To Connect To Server!\nPlease check your wifi Connection",
							false);
				}
			}

		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();

		}

		@Override
		protected void onProgressUpdate(Void... values) {

			super.onProgressUpdate(values);

		}

	}

	public class Progress_ extends AsyncTask<String, Void, String> {
		public Progress_() {
		}

		@Override
		protected String doInBackground(String... params) {

			String URL = "http://test.gardnerjames.eu.com/api/navitas-app/get-menu";

			JSONParserPost mJsonParserPost = new JSONParserPost();

			 Log.d("menu encrypted   1   =====", "" + mString_encrypt_final_pin);
			 Log.d("menu encrypted  0    =====", "" + mString_encrypt_final_probe_id);

			HttpEntity se = null;
			try {

				json = new JSONObject();
				json.put("0", mString_encrypt_final_probe_id);
				json.put("1", mString_encrypt_final_pin);

				se = new StringEntity(json.toString());

				// Log.e("parent====", "" + json);
			} catch (Exception e) {

				e.printStackTrace();
			}

			try {

			} catch (Exception e) {

				e.printStackTrace();
			}

			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(URL, se);

			 Log.i("mJsonObject string---------", "" + jsonobject.toString());

			try {
				mString_status_menu = jsonobject.getString("status");
				// Log.e("mString_status===", "" + mString_status_menu);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NullPointerException e) {
				mString_status_menu = "Error";
			}

			if (mString_status_menu.equals("success")) {
				try {
					jsonArray = jsonobject.getJSONArray("result");
					try {
						for (int i = 0; i < jsonArray.length(); i++) {
							JSONObject jsonObject = jsonArray.getJSONObject(i);

							String mString_new_group_id = jsonObject
									.getString("id");
							String mString_new_group_name = jsonObject
									.getString("name");
							json_Array = jsonObject.getJSONArray("children");
							// Log.e("json_Array==", "" + json_Array.length());
							for (int j = 0; j < json_Array.length(); j++) {
								JSONObject json_Object = json_Array
										.getJSONObject(j);
								String name_inside = json_Object
										.getString("name");
								String id_inside = json_Object.getString("id");

								// Log.e("name_inside==", "" + name_inside);
								// Log.e("id_inside==", "" + id_inside);
								// Log.e("mString_new_group_id=inside=", ""
								// + mString_new_group_id);
								mDatabaseHandler.Add_Listing_Item(
										mString_new_group_id, name_inside,
										id_inside);

							}
							// Log.d("mString_new_group_id==", ""
							// + mString_new_group_id);
							// Log.d("mString_new_group_name==", ""
							// + mString_new_group_name);
							mDatabaseHandler.Add_Food(null, null, null,
									mString_new_group_name,
									mString_new_group_id);

						}

					} catch (Exception e) {

					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// Log.e("jsonArray---------", "" + jsonArray.length());
			}

			return null;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			try {

				if (mString_status_menu.equals("success")) {
					Boolean isInternetPresent = cd.isConnectingToInternet(); // true
																				// or
					// false

					if (isInternetPresent == false) {
						show_alert("No Internet Connection", false);
						return;
					}
					mProgress_Supplier = new Progress_Supplier();
					mProgress_Supplier.execute();

				} else
					show_alert(
							"Unable To Connect To Server!\nPlease check your wifi Connection",
							false);

			} catch (NullPointerException e) {
				// TODO: handle exception

			}

		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();

		}

		@Override
		protected void onProgressUpdate(Void... values) {

			super.onProgressUpdate(values);

		}

	}

	public class Progress_Supplier extends AsyncTask<String, Void, String> {
		public Progress_Supplier() {
		}

		@Override
		protected String doInBackground(String... params) {

			String URL = "http://test.gardnerjames.eu.com/api/navitas-app/get-suppliers";

			JSONParserPost mJsonParserPost = new JSONParserPost();

			// Log.d("menu encrypted=====", "" + mString_encrypt_final_pin);
			// Log.d("menu encrypted=====", "" +
			// mString_encrypt_final_probe_id);

			HttpEntity se = null;
			try {

				json = new JSONObject();
				json.put("0", mString_encrypt_final_probe_id);
				json.put("1", mString_encrypt_final_pin);

				se = new StringEntity(json.toString());

				// Log.e("parent====", "" + json);
			} catch (Exception e) {

				e.printStackTrace();
			}

			try {

			} catch (Exception e) {

				e.printStackTrace();
			}

			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(URL, se);

			// Log.i("mJsonObject string---------", "" + jsonobject.toString());

			try {
				mString_status_supplier = jsonobject.getString("status");
				// Log.e("mString_status===", "" + mString_status_menu);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NullPointerException e) {
				mString_status_supplier = "Error!";
			}

			if (mString_status_supplier.equals("success")) {

				try {
					jsonArray = jsonobject.getJSONArray("result");
					// Log.e("jsonArray---------", "" + jsonArray.length());
					try {
						for (int i = 0; i < jsonArray.length(); i++) {
							JSONObject jsonObject = jsonArray.getJSONObject(i);

							String mString_email = jsonObject.getString("id");
							String mString_username = jsonObject
									.getString("name");
							String mString_image = jsonObject
									.getString("avatar");
							mDatabaseHandler.Add_Supplier(mString_email,
									mString_username, mString_image);

						}

					} catch (Exception e) {

					}
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}

			return null;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			String[] id_ = { "cooking", "reheating", "hot_service",
					"cold_service", "chilling", "goodsin" };
			String[] name_ = { "Cooking", "Re heating", "Hot Service",
					"Cold Service", "Chilling", "Goods In" };
			for (int i = 0; i < 6; i++) {
				mDatabaseHandler.Add_Service(id_[i], name_[i]);

			}

			try {

				if (mString_status_supplier.equals("success")) {
					Intent intent_home_screen = new Intent(
							Activit_Splash_class.this, MainMenu.class);

					startActivity(intent_home_screen);
					finish();
				} else
					show_alert(
							"Unable To Connect To Server!\nPlease check your wifi Connection",
							false);

			} catch (NullPointerException e) {
				// TODO: handle exception

			}

			// new Progress_Services().execute();

		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();

		}

		@Override
		protected void onProgressUpdate(Void... values) {

			super.onProgressUpdate(values);

		}

	}

	private void show_alert(String message, final boolean value) {
		// TODO Auto-generated method stub
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				Activit_Splash_class.this);

		// Setting Dialog Title
		alertDialog.setTitle("Error!");

		// Setting Dialog Message
		alertDialog.setMessage(message);

		// Setting Icon to Dialog
		alertDialog.setIcon(R.drawable.ic_launcher);

		// Setting Positive "Yes" Button
		alertDialog.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (value) {
							editor.putString("PROBE_ID", null);
							editor.commit();
							mEditor3.putString("PROBEID_ENCRYPT", null);
							mEditor3.putString("PIN_ENCRYPT", null);
							mEditor3.commit();
						}

						System.exit(0);
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

	private void show_navigate(String message) {
		// TODO Auto-generated method stub
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				Activit_Splash_class.this);

		// Setting Dialog Title
		alertDialog.setTitle("Internet Connection!");

		// Setting Dialog Message
		alertDialog.setMessage(message);

		// Setting Icon to Dialog
		alertDialog.setIcon(R.drawable.ic_launcher);

		// Setting Positive "Yes" Button
		alertDialog.setPositiveButton("YES",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						Intent intent_home_screen = new Intent(
								Activit_Splash_class.this, MainMenu.class);

						startActivity(intent_home_screen);
						finish();

					}
				});
		alertDialog.setNegativeButton("NO",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						System.exit(0);

					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

}
