package esb.navitas.smartherm;

import java.util.ArrayList;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {
	// broadcast receiver object=======

	Cursor cursor;
	DatabaseHandler db_handler;
	ArrayList<Data_off> arraylist_offline_data;
	Common_Method common_method;
	public static final String BROADCAST_ACTION = "esb.navitas.smartherm";
	@Override
	public IBinder onBind(Intent intent) {

		return null;
	}

	Handler handler;

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate() {
		Log.e("cursor.getCount()dffsdfd ", "############ONSTARTTTT");
		super.onCreate();
	}

	@Override
	public void onStart(Intent intent, int startId) {
		Log.e("cursor.getCount()dffsdfd ", "############ONSTARTTTT");
		
		db_handler = new DatabaseHandler(getApplicationContext());
	
		handler = new Handler();
		arraylist_offline_data=new ArrayList<MyService.Data_off>();
		arraylist_offline_data.clear();
		try {
			cursor = db_handler.fetch_offline_data();

		} catch (Exception e) {

			e.printStackTrace();
		}

		cursor.moveToFirst();
		Log.e("cursor.getCount()dffsdfd ", ""+cursor.getCount());
		if (cursor.getCount() != 0) {
			do {

				Data_off data = new Data_off();
				data.coulmn_id = cursor.getString(0).trim();
				data.probe_id = cursor.getString(1).trim();
				data.pin_id = cursor.getString(2).trim();
				data.service_id = cursor.getString(3).trim();
				data.food_id = cursor.getString(4).trim();
				data.user_id = cursor.getString(5).trim();
				data.temparture = cursor.getString(6).trim();
				data.status = cursor.getString(7).trim();
				arraylist_offline_data.add(data);
				common_method = new Common_Method(getApplicationContext());
				common_method.upload_offline(cursor.getString(0).trim(), cursor
						.getString(1).trim(), cursor.getString(2).trim(),
						cursor.getString(3).trim(), cursor.getString(5).trim(),
						cursor.getString(4).trim(), cursor.getString(6).trim(),
						cursor.getString(7).trim());

			} while (cursor.moveToNext());

		}
		cursor.close();
	Intent	intent2 = new Intent(BROADCAST_ACTION);
		
		 sendBroadcast(intent2);

		super.onStart(intent, startId);
	}

	public static class Data_off {
		String probe_id;
		String coulmn_id;
		String pin_id;
		String service_id;
		String user_id;
		String temparture;
		String status;
		String food_id;

	}

	@Override
	public void onDestroy() {

		super.onDestroy();
	}

}
