package esb.navitas.smartherm;

import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import JsonParser.JSONParserPost;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import esb.navitas.smartherm.Panel.OnPanelListener;
//import android.graphics.PorterDuff;
//import android.support.v7.app.ActionBarActivity;
//import android.widget.ListView;

//import java.util.ArrayList;

/**
 * Created by jburger on 06/01/14.
 */
public class goods_product_range extends Activity implements OnPanelListener,
		OnClickListener { /* ActionBarActivity */
	// globals global = (globals) getApplication();
	// boolean
	// rawMeat=false,fish=false,cookedMeat=false,dairy=false,bakery=false,vegetables=false,frozen=false,other=false;
	JSONArray jsonArray, json_Array;
	private Panel left_panel;
	Panel panel;
	JSONObject json;
	SharedPreferences mPreferences_all_data;
	ArrayList<String> mArrayList_ids_, mArrayList_name_dynamic;
	public static String My_Shared_All_Data = "All_Data";

	ProgressDialog mProgressDialog;

	int private_mode_all_data = 0;
	SharedPreferences.Editor mEditor;

	TextView textView_home, textView_goods_in, textView_chilling,
			textView_reheating, textView_hotserv, textView_coldserv,
			textview_calibration, textView_cooking;
	TextView textview_active, textView_supplier_name;
	ImageView imageView_supplier_pic;
	String supp_name, mString_supplier, mString_encrypt_final_probe_id,
			mString_encrypt_final_pin, mString_supp_id, mString_user_id,
			mString_status_;
	ArrayList<String> mArrayList;
	ArrayList<String> mArrayList_new;
	ArrayList<String> mArray_List_new;

	ArrayList<Boolean> mArrayList_store;

	GridView mGridView;
	ViewHolder holder;
	View gridView;
	Progress_List mProgress_List;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.goods_product_range);
		mGridView = (GridView) findViewById(R.id.gridview);
		mProgressDialog = new ProgressDialog(goods_product_range.this);
		mArrayList = new ArrayList<String>();
		mArray_List_new = new ArrayList<String>();
		mArrayList_ids_ = new ArrayList<String>();
		mArrayList_name_dynamic = new ArrayList<String>();
		mPreferences_all_data = this.getSharedPreferences(My_Shared_All_Data,
				private_mode_all_data);

		mEditor = mPreferences_all_data.edit();
		mString_encrypt_final_probe_id = mPreferences_all_data.getString(
				"PROBEID_ENCRYPT", null);
		mString_encrypt_final_pin = mPreferences_all_data.getString(
				"PIN_ENCRYPT", null);

		if (mArrayList != null) {
			mArrayList.clear();
			mArrayList_ids_.clear();
			mArrayList_name_dynamic.clear();
		}

		mArrayList_new = new ArrayList<String>();

		if (mArrayList_new != null) {
			mArrayList_new.clear();
		}
		if (mArray_List_new != null) {
			mArray_List_new.clear();
		}
		if (mArrayList_name_dynamic != null) {
			mArrayList_name_dynamic.clear();
		}
	
		globals global = (globals) getApplication();
		global.rawMeat = false;
		global.fish = false;
		global.cookedMeat = false;
		global.dairy = false;
		global.bakery = false;
		global.vegetables = false;
		global.frozen = false;
		global.other = false;
		textView_supplier_name = (TextView) findViewById(R.id.textView_supp_name);
		imageView_supplier_pic = (ImageView) findViewById(R.id.imageView_supp_image);
		supp_name = getIntent().getStringExtra("supp_name");
		mString_supp_id = getIntent().getStringExtra("supp_name_id");
		mString_user_id = getIntent().getStringExtra("user_id");

		mString_supplier = getIntent().getStringExtra("supplier");
		textView_supplier_name.setText(supp_name);

		textview_calibration = (TextView) findViewById(R.id.textView_callibration);
		textView_chilling = (TextView) findViewById(R.id.textView_chilling);
		textView_coldserv = (TextView) findViewById(R.id.textView_cold);
		textView_goods_in = (TextView) findViewById(R.id.textView_goods_in);
		textView_home = (TextView) findViewById(R.id.textView_home);
		textView_hotserv = (TextView) findViewById(R.id.textView_hotservice);
		textView_reheating = (TextView) findViewById(R.id.textView_reheating);
		textView_cooking = (TextView) findViewById(R.id.textView_cooking);

		textview_calibration.setOnClickListener(this);
		textView_chilling.setOnClickListener(this);
		textView_coldserv.setOnClickListener(this);
		textView_goods_in.setOnClickListener(this);
		textView_home.setOnClickListener(this);
		textView_hotserv.setOnClickListener(this);
		textView_reheating.setOnClickListener(this);
		textView_cooking.setOnClickListener(this);

		left_panel = panel = (Panel) findViewById(R.id.leftPanel1);
		panel.setOnPanelListener(this);

		findViewById(R.id.smoothButton1).setOnClickListener(
				new OnClickListener() {
					public void onClick(View v) {
						left_panel.setOpen(!left_panel.isOpen(), true);
					}
				});
		
		
		ConnectionDetector	cd = new ConnectionDetector(getApplicationContext());

		Boolean isInternetPresent = cd.isConnectingToInternet(); // true or
		if (isInternetPresent) {
		mProgress_List = new Progress_List();
		mProgress_List.execute();
		}
		else
		{
			show_alert("You can not use GoodsIn without internet access");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.user_selected_menu, menu);
		globals global = (globals) getApplication();
		if (global.goodsUser != null) {
			MenuItem txtUser = menu.findItem(R.id.user_display);
			txtUser.setTitle(global.goodsUser);
		}
		return true;
	}

	public void clk_btn_back_view(View view) {
		// TODO Do something in response to Submit button click and return to
		// main menu
		// globals global = (globals) getApplication();
		finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		Intent intent;
		/*
		 * if (id == R.id.action_settings) { return true; }
		 */
		switch (id) {
		case R.id.user_display:
			intent = new Intent(getApplicationContext(), goods_user.class);
			startActivity(intent);
			break;
		case R.id.home_menu:
			intent = new Intent(getApplicationContext(), MainMenu.class);
			startActivity(intent);
			break;
		case R.id.goods_menu:
			intent = new Intent(getApplicationContext(), goods_user.class);
			startActivity(intent);
			break;
		case R.id.cook_menu:
			intent = new Intent(getApplicationContext(), cook_user.class);
			startActivity(intent);
			break;
		case R.id.chill_menu:
			intent = new Intent(getApplicationContext(), chilling_user.class);
			startActivity(intent);
			break;
		case R.id.reheat_menu:
			intent = new Intent(getApplicationContext(), reheat_user.class);
			startActivity(intent);
			break;
		case R.id.hot_serv_menu:
			intent = new Intent(getApplicationContext(), hot_service_user.class);
			startActivity(intent);
			break;
		case R.id.cold_serv_menu:
			intent = new Intent(getApplicationContext(),
					cold_service_user.class);
			startActivity(intent);
			break;
		case R.id.cal_menu:
			intent = new Intent(getApplicationContext(), calibration_user.class);
			startActivity(intent);
			break;
		default:
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	// /* Called when the user clicks the Raw Meats button */
	// public void clk_btn_goods_product_raw_meat(View view) {
	// // TODO Do something in response to Raw Meats button click
	// LinearLayout button = (LinearLayout)
	// findViewById(R.id.btn_goods_product_raw_meat);
	// TextView textView_rawmeat = (TextView)
	// findViewById(R.id.textView_rawmeat);
	// globals global = (globals) getApplication();
	// // boolean rawMeat = global.rawMeat;
	// if (!global.rawMeat) {
	// // Highlight button
	// mArrayList.set(0, "0");
	// global.rawMeat = true;
	// //
	// button.setBackgroundDrawable(getResources().getDrawable(R.drawable.apptheme_btn_default_pressed_holo_light));
	// button.setBackgroundResource(R.drawable.button_pressed);
	// textView_rawmeat.setBackgroundColor(Color.TRANSPARENT);
	// textView_rawmeat.setTextColor(Color.WHITE);
	// } else {
	// // Deselect button
	// global.rawMeat = false;
	// mArrayList.set(0, "1");
	//
	// //
	// button.setBackgroundDrawable(getResources().getDrawable(R.drawable.apptheme_btn_default_normal_holo_light));
	// button.setBackgroundResource(R.drawable.main_menus_background);
	// textView_rawmeat
	// .setBackgroundResource(R.drawable.home_menus_background);
	// textView_rawmeat.setTextColor(Color.BLACK);
	// }
	// }
	//
	// /* Called when the user clicks the Fish button */
	// public void clk_btn_goods_product_fish(View view) {
	// // TODO Do something in response to Fish button click
	// LinearLayout button = (LinearLayout)
	// findViewById(R.id.btn_goods_product_fish);
	// TextView textView_fish = (TextView) findViewById(R.id.textView_fish);
	// globals global = (globals) getApplication();
	// if (!global.fish) {
	// global.fish = true;
	// mArrayList.set(1, "1");
	//
	// button.setBackgroundResource(R.drawable.button_pressed);
	// textView_fish.setBackgroundColor(Color.TRANSPARENT);
	// textView_fish.setTextColor(Color.WHITE);
	// } else {
	// // Deselect button
	// global.fish = false;
	// mArrayList.set(1, "0");
	// //
	// button.setBackgroundDrawable(getResources().getDrawable(R.drawable.apptheme_btn_default_normal_holo_light));
	// button.setBackgroundResource(R.drawable.main_menus_background);
	// textView_fish
	// .setBackgroundResource(R.drawable.home_menus_background);
	// textView_fish.setTextColor(Color.BLACK);
	// }
	// }
	//
	// /* Called when the user clicks the Cooked Meat button */
	// public void clk_btn_goods_product_cooked_meat(View view) {
	// // TODO Do something in response to Cooked Meat button click
	// LinearLayout button = (LinearLayout)
	// findViewById(R.id.btn_goods_product_cooked_meat);
	// TextView textView_cookedmeat = (TextView)
	// findViewById(R.id.textView_cooked_meat);
	// globals global = (globals) getApplication();
	// if (!global.cookedMeat) {
	// global.cookedMeat = true;
	// mArrayList.set(2, "1");
	//
	// button.setBackgroundResource(R.drawable.button_pressed);
	// textView_cookedmeat.setBackgroundColor(Color.TRANSPARENT);
	// textView_cookedmeat.setTextColor(Color.WHITE);
	// } else {
	// // Deselect button
	// global.cookedMeat = false;
	// mArrayList.set(2, "0");
	// //
	// button.setBackgroundDrawable(getResources().getDrawable(R.drawable.apptheme_btn_default_normal_holo_light));
	// button.setBackgroundResource(R.drawable.main_menus_background);
	// textView_cookedmeat
	// .setBackgroundResource(R.drawable.home_menus_background);
	// textView_cookedmeat.setTextColor(Color.BLACK);
	// }
	// }
	//
	// /* Called when the user clicks the Dairy button */
	// public void clk_btn_goods_product_dairy(View view) {
	// // TODO Do something in response to Dairy button click
	// LinearLayout button = (LinearLayout)
	// findViewById(R.id.btn_goods_product_dairy);
	// TextView textView_dairy = (TextView) findViewById(R.id.textView_dairy);
	// globals global = (globals) getApplication();
	// if (!global.dairy) {
	// global.dairy = true;
	// mArrayList.set(3, "1");
	//
	// button.setBackgroundResource(R.drawable.button_pressed);
	// textView_dairy.setBackgroundColor(Color.TRANSPARENT);
	// textView_dairy.setTextColor(Color.WHITE);
	// } else {
	// // Deselect button
	// global.dairy = false;
	// mArrayList.set(3, "0");
	// //
	// button.setBackgroundDrawable(getResources().getDrawable(R.drawable.apptheme_btn_default_normal_holo_light));
	// button.setBackgroundResource(R.drawable.main_menus_background);
	// textView_dairy
	// .setBackgroundResource(R.drawable.home_menus_background);
	// textView_dairy.setTextColor(Color.BLACK);
	//
	// }
	// }
	//
	// /* Called when the user clicks the Bakery button */
	// public void clk_btn_goods_product_bakery(View view) {
	// // TODO Do something in response to Bakery button click
	// LinearLayout button = (LinearLayout)
	// findViewById(R.id.btn_goods_product_bakery);
	// TextView textView_bakery = (TextView) findViewById(R.id.textView_bakery);
	// globals global = (globals) getApplication();
	// if (!global.bakery) {
	// global.bakery = true;
	// mArrayList.set(4, "1");
	//
	// button.setBackgroundResource(R.drawable.button_pressed);
	// textView_bakery.setBackgroundColor(Color.TRANSPARENT);
	// textView_bakery.setTextColor(Color.WHITE);
	// } else {
	// // Deselect button
	// global.bakery = false;
	// mArrayList.set(4, "0");
	// //
	// button.setBackgroundDrawable(getResources().getDrawable(R.drawable.apptheme_btn_default_normal_holo_light));
	// button.setBackgroundResource(R.drawable.main_menus_background);
	// textView_bakery
	// .setBackgroundResource(R.drawable.home_menus_background);
	// textView_bakery.setTextColor(Color.BLACK);
	// }
	// }
	//
	// /* Called when the user clicks the Vegetables button */
	// public void clk_btn_goods_product_vegetables(View view) {
	// // TODO Do something in response to Vegetables button click
	// LinearLayout button = (LinearLayout)
	// findViewById(R.id.btn_goods_product_vegetables);
	// TextView textView_vegetables = (TextView)
	// findViewById(R.id.textView_vegetables);
	// globals global = (globals) getApplication();
	// if (!global.vegetables) {
	// global.vegetables = true;
	// mArrayList.set(5, "1");
	//
	// button.setBackgroundResource(R.drawable.button_pressed);
	// textView_vegetables.setBackgroundColor(Color.TRANSPARENT);
	// textView_vegetables.setTextColor(Color.WHITE);
	// } else {
	// // Deselect button
	// global.vegetables = false;
	// mArrayList.set(5, "0");
	// //
	// button.setBackgroundDrawable(getResources().getDrawable(R.drawable.apptheme_btn_default_normal_holo_light));
	// button.setBackgroundResource(R.drawable.main_menus_background);
	// textView_vegetables
	// .setBackgroundResource(R.drawable.home_menus_background);
	// textView_vegetables.setTextColor(Color.BLACK);
	// }
	// }
	//
	// /* Called when the user clicks the Frozen button */
	// public void clk_btn_goods_product_frozen(View view) {
	// // TODO Do something in response to Frozen button click
	// LinearLayout button = (LinearLayout)
	// findViewById(R.id.btn_goods_product_frozen);
	// TextView textView_frozen = (TextView) findViewById(R.id.textView_frozen);
	// globals global = (globals) getApplication();
	// if (!global.frozen) {
	// global.frozen = true;
	// mArrayList.set(6, "1");
	//
	// button.setBackgroundResource(R.drawable.button_pressed);
	// textView_frozen.setBackgroundColor(Color.TRANSPARENT);
	// textView_frozen.setTextColor(Color.WHITE);
	// } else {
	// // Deselect button
	// global.frozen = false;
	// mArrayList.set(6, "0");
	// //
	// button.setBackgroundDrawable(getResources().getDrawable(R.drawable.apptheme_btn_default_normal_holo_light));
	// button.setBackgroundResource(R.drawable.main_menus_background);
	// textView_frozen
	// .setBackgroundResource(R.drawable.home_menus_background);
	// textView_frozen.setTextColor(Color.BLACK);
	// }
	// }
	//
	// /* Called when the user clicks the Other button */
	// public void clk_btn_goods_product_other(View view) {
	// // TODO Do something in response to Other button click
	// LinearLayout button = (LinearLayout)
	// findViewById(R.id.btn_goods_product_other);
	// TextView textView_other = (TextView) findViewById(R.id.textView_other);
	// globals global = (globals) getApplication();
	// if (!global.other) {
	// global.other = true;
	// mArrayList.set(7, "1");
	//
	// button.setBackgroundResource(R.drawable.button_pressed);
	// textView_other.setBackgroundColor(Color.TRANSPARENT);
	// textView_other.setTextColor(Color.WHITE);
	// } else {
	// // Deselect button
	// global.other = false;
	// mArrayList.set(7, "0");
	// //
	// button.setBackgroundDrawable(getResources().getDrawable(R.drawable.apptheme_btn_default_normal_holo_light));
	// button.setBackgroundResource(R.drawable.main_menus_background);
	// textView_other
	// .setBackgroundResource(R.drawable.home_menus_background);
	// textView_other.setTextColor(Color.BLACK);
	// }
	// }

	/* Called when the user clicks the Submit button */
	public void clk_btn_goods_product_submit(View view) {
		// TODO Do something in response to Submit button click

		if (mString_status_.equals("fail")) {
			Toast.makeText(getApplicationContext(),
					"Please add product to suppiler section on navitas site",
					Toast.LENGTH_LONG).show();
			return;
		}
		mArray_List_new.clear();
		globals global = (globals) getApplication();
		//Log.i("mArrayList.size()===== ", "" + mArrayList_store.size());

		for (int i = 0; i < mArrayList_store.size(); i++) {
			//Log.i("eentet", "" + mArrayList_store.get(i));
			if (mArrayList_store.get(i)) {

				mArray_List_new.add(mArrayList_ids_.get(i));
			}
		}


		if (mArray_List_new.size() == 0) {
			Toast.makeText(getApplicationContext(), "Select The Food Range",
					Toast.LENGTH_LONG).show();
			return;
		}

		Intent intent = new Intent(getApplicationContext(), goods_invoice.class);
		intent.putExtra("supp_name", supp_name);
		intent.putExtra("supplier", mString_supplier);
		intent.putExtra("user_id", mString_user_id);
		intent.putExtra("supp_name_id", mString_supp_id);

		intent.putStringArrayListExtra("ProductRange", mArray_List_new);
		startActivityForResult(intent, 0);

		// mArray_List_new.add("1");
		// mArray_List_new.add("2");
		// mArray_List_new.add("3");

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 0 && resultCode == RESULT_OK) {
			setResult(RESULT_OK);
			finish();
		}
	}

	public void onPanelClosed(Panel panel) {
		String panelName = getResources().getResourceEntryName(panel.getId());
		//Log.d("Test", "Panel [" + panelName + "] closed");
		findViewById(R.id.trans_region).setVisibility(View.GONE);

	}

	public void onPanelOpened(Panel panel) {
		String panelName = getResources().getResourceEntryName(panel.getId());
		//Log.d("Test", "Panel [" + panelName + "] opened");
		findViewById(R.id.trans_region).setVisibility(View.VISIBLE);
		;
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent;
		switch (v.getId()) {

		case R.id.textView_home:
			left_panel.setOpen(!left_panel.isOpen(), true);
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_home.setBackgroundResource(R.drawable.button_background);
			intent = new Intent(this, MainMenu.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);

			finish();
			break;
		case R.id.textView_goods_in:

			left_panel.setOpen(!left_panel.isOpen(), true);
			break;

		case R.id.textView_cooking:
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_cooking
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, cook_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		case R.id.textView_chilling:

			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_chilling
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, chilling_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();

			break;

		case R.id.textView_reheating:
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_reheating
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, reheat_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		case R.id.textView_hotservice:
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_hotserv
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, hot_service_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		case R.id.textView_cold:
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_coldserv
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, cold_service_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		case R.id.textView_callibration:
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textview_calibration
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, calibration_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			setResult(RESULT_OK);
			startActivity(intent);

			finish();
			break;

		default:
			break;
		}
	}

	public class Progress_List extends AsyncTask<String, Void, String> {
		public Progress_List() {
		}

		@Override
		protected String doInBackground(String... params) {

			//Log.d("mString_supp_id=====", "" + mString_supp_id);

			String URL = "http://test.gardnerjames.eu.com/api/navitas-app/"
					+ mString_supp_id + "/get-products";
			//Log.d("URL=====", "" + URL);
			JSONParserPost mJsonParserPost = new JSONParserPost();

			//Log.d("users encrypted=====", "" + mString_encrypt_final_pin);
			//Log.d("users encrypted=====", "" + mString_encrypt_final_probe_id);

			HttpEntity se = null;
			try {

				json = new JSONObject();
				json.put("0", mString_encrypt_final_probe_id);
				json.put("1", mString_encrypt_final_pin);

				se = new StringEntity(json.toString());

				//Log.e("parent====", "" + json);
			} catch (Exception e) {

				e.printStackTrace();
			}
			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(URL, se);

			//Log.i("mJsonObject string---------", "" + jsonobject.toString());

			try {
				mString_status_ = jsonobject.getString("status");
				//Log.e("mString_status_user===", "" + mString_status_);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (mString_status_.equals("success")) {
				try {
					jsonArray = jsonobject.getJSONArray("result");
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				//Log.e("jsonArray---------", "" + jsonArray.length());
				mArrayList_store = new ArrayList<Boolean>();
				try {
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);

						String mString_id = jsonObject.getString("id");
						String mString_name = jsonObject.getString("name");
						mArrayList_store.add(false);

						mArrayList_ids_.add(mString_id);
						mArrayList_name_dynamic.add(mString_name);

					}

				} catch (Exception e) {

				}
			}

			return null;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			mProgressDialog.cancel();
			mProgressDialog.dismiss();

			if (mString_status_.equals("success")) {
				ImageAdapter mAdapter = new ImageAdapter(
						getApplicationContext(), mArrayList_name_dynamic);
				mGridView.setAdapter(mAdapter);
			} else {
		show_alert("No Product Range For Supplier");
			}

		}

		@Override
		protected void onPreExecute() {

			mProgressDialog.show();
			mProgressDialog.setCancelable(false);

			super.onPreExecute();

		}

		@Override
		protected void onProgressUpdate(Void... values) {

			super.onProgressUpdate(values);

		}

	}

	// base adapter set

	public class ImageAdapter extends BaseAdapter {
		private Context mContext;
		private ArrayList<String> mArrayList;
		LayoutInflater inflater;

		ArrayList<TextView> mArrayList_textview;
		ArrayList<LinearLayout> mArrayList_linear;
		ArrayList<Boolean> mArrayList_boolean;

		public ImageAdapter(Context c, ArrayList<String> mArrayList_text) {
			mContext = c;
			mArrayList = mArrayList_text;
			mArrayList_linear = new ArrayList<LinearLayout>();
			mArrayList_textview = new ArrayList<TextView>();
			mArrayList_boolean = new ArrayList<Boolean>();

			for (int i = 0; i < mArrayList_text.size(); i++) {
				mArrayList_boolean.add(i, false);
			}

			inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		public int getCount() {
			return mArrayList.size();
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		// create a new ImageView for each item referenced by the Adapter
		public View getView(final int position, View convertView,
				ViewGroup parent) {

			gridView = convertView;
			if (convertView == null) {

				holder = new ViewHolder();

				gridView = inflater
						.inflate(R.layout.custom_product_range, null);
				holder.mLinearLayout = (LinearLayout) gridView
						.findViewById(R.id.btn_goods_product_cooked_meat);
				holder.imageView = (TextView) gridView
						.findViewById(R.id.textView_common);
				mArrayList_textview.add(holder.imageView);
				mArrayList_linear.add(holder.mLinearLayout);

			} else {

				holder = (ViewHolder) gridView.getTag();
			}
			holder.mLinearLayout.setId(position);
			holder.imageView.setId(position);
			holder.imageView.setText(mArrayList.get(position));
			gridView.setTag(holder);

			gridView.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
				
					if (mArrayList_store.get(position)) {
						mArrayList_store.set(position, false);

						TextView text_ = mArrayList_textview.get(position);
						LinearLayout linear_ = mArrayList_linear.get(position);
						linear_.setBackgroundDrawable(getResources()
								.getDrawable(R.drawable.main_menus_background));

						text_.setBackgroundResource(R.drawable.home_menus_background);
						text_.setTextColor(Color.BLACK);
					} else {
						// String mString_position =
						// mArrayList_ids_.get(position);

						TextView text_ = mArrayList_textview.get(position);
						LinearLayout linear_ = mArrayList_linear.get(position);
						linear_.setBackgroundResource(R.drawable.button_pressed);

						text_.setBackgroundColor(Color.TRANSPARENT);
						text_.setTextColor(Color.WHITE);

						mArrayList_store.set(position, true);
					}

				}
			});

			return gridView;
		}

	}

	public class ViewHolder {
		public TextView imageView;

		public LinearLayout mLinearLayout;

	}
	
	private void show_alert(String message) {
		// TODO Auto-generated method stub
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				goods_product_range.this);

		// Setting Dialog Title
		alertDialog.setTitle("Error!");

		// Setting Dialog Message
		alertDialog.setMessage(message);

		// Setting Icon to Dialog
		alertDialog.setIcon(R.drawable.ic_launcher);

		// Setting Positive "Yes" Button
		alertDialog.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						finish();
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}


}
