package esb.navitas.smartherm;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import JsonParser.JSONParserPost;
import android.util.Log;

public class Good_In_Api_Call {
	JSONObject json;
	String mString_status="";
	String error_message="";

	// constructor
	public Good_In_Api_Call() {

	}

	public String json_method_call(String probeid, String pin, String suppid,
			String userid, String tempvalue, String mString_invoiceno,
			String mString_package_accept, String mString_datacode,
			ArrayList<String> mArrayList_product_range) {

		try {

			String URL = "http://test.gardnerjames.eu.com/api/navitas-app/goods-in";
			JSONArray jsonArray = new JSONArray();
			JSONParserPost mJsonParserPost = new JSONParserPost();

			for (int i = 0; i < mArrayList_product_range.size(); i++) {
				jsonArray.put(mArrayList_product_range.get(i));
			}

			Log.e("probeid=====", "" + probeid);
			Log.e("pin=====", "" + pin);
			Log.e("suppid=====", "" + suppid);
			Log.e("userid=====", "" + userid);
			Log.e("suppid=====", "" + suppid);
			Log.e("mArrayList_product_range=====", ""
					+ mArrayList_product_range);
			Log.e("mString_package_accept=====", "" + mString_package_accept);
			Log.e("mString_datacode=====", "" + mString_datacode);
			Log.e("jsonArray=====", "" + jsonArray);

			Log.e("tempvalue=====", "" + tempvalue);

			HttpEntity se = null;
	

				json = new JSONObject();
				json.put("0", probeid);
				json.put("1", pin);
				json.put("s_id", suppid);
				json.put("u_id", userid);
				json.put("p_val", mString_package_accept);
				json.put("t_val", tempvalue);
				json.put("d_val", mString_datacode);
				json.put("i_val", mString_invoiceno);
				json.put("p_arr", jsonArray);

				try {
					se = new StringEntity(json.toString());
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				// Log.e("jsonObject====", ""+jsonObject);

		
			Log.e("parent====Check Value ", "" + json);
				JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(URL, se);
				Log.e("jsonobject====", "" + jsonobject);
				if(jsonobject==null)
				{error_message="Error converting result org.json.JSONException: Value <!DOCTYPE of type java.lang.String cannot be converted to JSONObject";}
				else
				{
		
				mString_status = jsonobject.getString("status");
				Log.e("mString_status===", "" + mString_status);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				error_message="Error converting result org.json.JSONException: Value <!DOCTYPE of type java.lang.String cannot be converted to JSONObject";
				e.printStackTrace();
			}
		

		if (mString_status.equals("success")) 
		return mString_status;
		else
	    return error_message;
			
	}

}
