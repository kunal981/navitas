package esb.navitas.smartherm;

import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import JsonParser.JSONARRAY;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import esb.navitas.smartherm.Panel.OnPanelListener;
//import android.support.v7.app.ActionBarActivity;
//import android.widget.TextView;
import esb.navitas.smartherm.goods_user.Progress_List;

/**
 * Created by jburger on 06/01/14.
 */
public class goods_suppliers extends Activity implements OnPanelListener,
		OnClickListener { /* ActionBarActivity */
	// private Menu menu;
	private boolean running = false;
	private Panel left_panel;
	Panel panel;
	private CustomFastScrollView fastScrollView;

	Cursor mCursor;
	DatabaseHandler mDatabaseHandler;
	JSONArray jsonArray;
	ProgressDialog pDialog;
	Progress_List mProgress_List;
	ArrayList<String> mArrayList;
	ArrayList<String> mArrayList_supplier_id;

	TextView textView_home, textView_goods_in, textView_chilling,
			textView_reheating, textView_hotserv, textView_coldserv,
			textview_calibration, textView_cooking;
	TextView textview_active, textView_supplier_name;
	ImageView image_arrow, imageView_supplier_pic;

	ListView listView;
	String supp_name, mString_supp_id;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.goods_suppliers);
		mDatabaseHandler = new DatabaseHandler(this);
		final String[] values = getResources()
				.getStringArray(R.array.suppliers);

		pDialog = new ProgressDialog(this);
		mArrayList = new ArrayList<String>();
		mArrayList_supplier_id = new ArrayList<String>();
		if (mArrayList != null) {
			mArrayList.clear();
			mArrayList_supplier_id.clear();
		}
		listView = (ListView) findViewById(android.R.id.list);
		fastScrollView = (CustomFastScrollView) findViewById(R.id.fast_scroll_view);
		textView_supplier_name = (TextView) findViewById(R.id.textView_supp_name);
		imageView_supplier_pic = (ImageView) findViewById(R.id.imageView_supp_image);
		/*
		 * String[] values = new String[]{"Adam", "Bob", "Carl", "Dave", "Ed",
		 * "Fran", "George", "Harry", "Ian", "Jo", "Katy", "Laura", "Mike",
		 * "Nat", "Rachael"};
		 */

		supp_name = getIntent().getStringExtra("supp_name");
		mString_supp_id = getIntent().getStringExtra("user_id");

		textView_supplier_name.setText(supp_name);
		// //final ArrayList<String> list = new ArrayList<String>();
		// for (int i = 0; i < values.length; ++i) {
		// list.add(values[i]);
		// }
		// Collections.sort(list);

		textview_calibration = (TextView) findViewById(R.id.textView_callibration);
		textView_chilling = (TextView) findViewById(R.id.textView_chilling);
		textView_coldserv = (TextView) findViewById(R.id.textView_cold);
		textView_goods_in = (TextView) findViewById(R.id.textView_goods_in);
		textView_home = (TextView) findViewById(R.id.textView_home);
		textView_hotserv = (TextView) findViewById(R.id.textView_hotservice);
		textView_reheating = (TextView) findViewById(R.id.textView_reheating);
		textView_cooking = (TextView) findViewById(R.id.textView_cooking);

		textview_calibration.setOnClickListener(this);
		textView_chilling.setOnClickListener(this);
		textView_coldserv.setOnClickListener(this);
		textView_goods_in.setOnClickListener(this);
		textView_home.setOnClickListener(this);
		textView_hotserv.setOnClickListener(this);
		textView_reheating.setOnClickListener(this);
		textView_cooking.setOnClickListener(this);
		// mProgress_List=new Progress_List();

		// mProgress_List.execute();

		try {
			mCursor = mDatabaseHandler.fetch_supplier();
			//Log.i("value in cursor at starting", mCursor.getCount() + "");
		} catch (Exception e) {

			e.printStackTrace();
		}

		// mCursor.moveToFirst();
		if (mCursor.getCount() != 0) {
			do {

				mArrayList.add(mCursor.getString(0).trim());
				mArrayList_supplier_id.add(mCursor.getString(1).trim());

			} while (mCursor.moveToNext());

		}
		mCursor.close();
		mDatabaseHandler.close();


		StableArrayAdapter adapter = new StableArrayAdapter(
				getApplicationContext(), mArrayList);
		listView.setAdapter(adapter);

		listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		// ArrayAdapter<String> adapter = new CountryAdapter(this,
		// android.R.layout.simple_list_item_2, list);
		// listView.setAdapter(adapter);
		/*
		 * listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
		 * { //@Override public void onItemClick(AdapterView<?> parent, View
		 * view, int position, long id) { globals global = (globals)
		 * getApplication(); global.goodsSupplier = values[position]; Intent
		 * intent = new Intent(getApplicationContext(),
		 * goods_product_range.class); startActivity(intent); } });
		 */

		left_panel = panel = (Panel) findViewById(R.id.leftPanel1);
		panel.setOnPanelListener(this);

		findViewById(R.id.smoothButton1).setOnClickListener(
				new OnClickListener() {
					public void onClick(View v) {
						left_panel.setOpen(!left_panel.isOpen(), true);
					}
				});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 0 && resultCode == RESULT_OK) {
			setResult(RESULT_OK);
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.user_selected_menu, menu);
		globals global = (globals) getApplication();
		if (global.goodsUser != null) {
			MenuItem txtUser = menu.findItem(R.id.user_display);
			txtUser.setTitle(global.goodsUser);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		Intent intent;
		/*
		 * if (id == R.id.action_settings) { return true; }
		 */
		switch (id) {
		case R.id.user_display:
			intent = new Intent(getApplicationContext(), goods_user.class);
			startActivity(intent);
			break;
		case R.id.home_menu:
			intent = new Intent(getApplicationContext(), MainMenu.class);
			startActivity(intent);
			break;
		case R.id.goods_menu:
			intent = new Intent(getApplicationContext(), goods_user.class);
			startActivity(intent);
			break;
		case R.id.cook_menu:
			intent = new Intent(getApplicationContext(), cook_user.class);
			startActivity(intent);
			break;
		case R.id.chill_menu:
			intent = new Intent(getApplicationContext(), chilling_user.class);
			startActivity(intent);
			break;
		case R.id.reheat_menu:
			intent = new Intent(getApplicationContext(), reheat_user.class);
			startActivity(intent);
			break;
		case R.id.hot_serv_menu:
			intent = new Intent(getApplicationContext(), hot_service_user.class);
			startActivity(intent);
			break;
		case R.id.cold_serv_menu:
			intent = new Intent(getApplicationContext(),
					cold_service_user.class);
			startActivity(intent);
			break;
		case R.id.cal_menu:
			intent = new Intent(getApplicationContext(), calibration_user.class);
			startActivity(intent);
			break;
		default:
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/*
	 * private class StableArrayAdapter extends ArrayAdapter<String> {
	 * 
	 * HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();
	 * 
	 * public StableArrayAdapter(Context context, int textViewResourceId,
	 * List<String> objects) { super(context, textViewResourceId, objects); for
	 * (int i = 0; i < objects.size(); ++i) { mIdMap.put(objects.get(i), i); } }
	 * 
	 * @Override public long getItemId(int position) { String item =
	 * getItem(position); return mIdMap.get(item); }
	 * 
	 * @Override public boolean hasStableIds() { return true; } }
	 */

	private class StableArrayAdapter extends BaseAdapter {
		ArrayList<String> arrayList_supplier;
		Context context;
		LayoutInflater inflater;

		public StableArrayAdapter(final Context context,
				final ArrayList<String> arrayList_supplier) {
			this.arrayList_supplier = arrayList_supplier;
			this.context = context;
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		}

		public int getCount() {
			// TODO Auto-generated method stub
			return arrayList_supplier.size();
		}

		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return arrayList_supplier.get(arg0);
		}

		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {
			final ViewHolder holder;
			View vi = convertView;
			if (convertView == null) {
				vi = inflater.inflate(R.layout.custom_supplier_list, null);
				holder = new ViewHolder();
				holder.word_supplier = (TextView) vi
						.findViewById(R.id.textView1);

				holder.imageView_arrow = (ImageView) vi
						.findViewById(R.id.imageView2);
				vi.setTag(holder);
			} else {
				holder = (ViewHolder) vi.getTag();
			}
			holder.word_supplier.setId(position);
			holder.imageView_arrow.setId(position);

			holder.word_supplier.setText(arrayList_supplier.get(position));
			vi.setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (listView.isEnabled()) {
						textview_active = holder.word_supplier;
						holder.word_supplier.setTextColor(Color
								.parseColor("#FB8739"));
						holder.imageView_arrow
								.setImageResource(R.drawable.arrow_orange);
						image_arrow = holder.imageView_arrow;
						globals global = (globals) getApplication();
						global.goodsSupplier = arrayList_supplier.get(position);
						Intent intent = new Intent(getApplicationContext(),
								goods_product_range.class);
						intent.putExtra("supp_name", supp_name);
						intent.putExtra("user_id", mString_supp_id);
						intent.putExtra("supplier",
								arrayList_supplier.get(position));
						intent.putExtra("supp_name_id",
								mArrayList_supplier_id.get(position));
						startActivityForResult(intent, 0);

					}

				}
			});

			return vi;
		}

		public class ViewHolder {
			public TextView word_supplier;
			public ImageView imageView_arrow;

		}

	}

	public void onPanelClosed(Panel panel) {
		String panelName = getResources().getResourceEntryName(panel.getId());
		//Log.d("Test", "Panel [" + panelName + "] closed");
		findViewById(R.id.trans_region).setVisibility(View.GONE);

		listView.setEnabled(true);

	}

	public void onPanelOpened(Panel panel) {
		String panelName = getResources().getResourceEntryName(panel.getId());
		//Log.d("Test", "Panel [" + panelName + "] opened");
		findViewById(R.id.trans_region).setVisibility(View.VISIBLE);

		listView.setEnabled(false);
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent;
		switch (v.getId()) {

		case R.id.textView_home:
			left_panel.setOpen(!left_panel.isOpen(), true);
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_home.setBackgroundResource(R.drawable.button_background);
			intent = new Intent(this, MainMenu.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);

			finish();
			break;
		case R.id.textView_goods_in:

			left_panel.setOpen(!left_panel.isOpen(), true);
			break;

		case R.id.textView_cooking:
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_cooking
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, cook_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		case R.id.textView_chilling:

			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_chilling
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, chilling_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();

			break;

		case R.id.textView_reheating:
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_reheating
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, reheat_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		case R.id.textView_hotservice:
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_hotserv
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, hot_service_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		case R.id.textView_cold:
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_coldserv
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, cold_service_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		case R.id.textView_callibration:
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textview_calibration
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, calibration_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			setResult(RESULT_OK);
			startActivity(intent);

			finish();
			break;

		default:
			break;
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (textview_active != null) {
			textview_active.setTextColor(Color.BLACK);
			image_arrow.setImageResource(R.drawable.arrow_black);

		}
	}

	public class Progress_List extends AsyncTask<String, Void, String> {
		public Progress_List() {
		}

		@Override
		protected String doInBackground(String... params) {

			String URL = "http://brstdev.com/caczcall/calendar/navitasapp/supplier.php";

			JSONARRAY jParser = new JSONARRAY();
			// getting JSON string from URL

			String response = jParser.getJSONFromUrl(URL);
			//Log.e("response---------", "" + response);

			try {
				JSONObject mJsonObject = new JSONObject(response);

		

				jsonArray = mJsonObject.getJSONArray("result");
				//Log.e("jsonArray---------", "" + jsonArray.length());

			} catch (JSONException e) {

				e.printStackTrace();
			}
			catch(NullPointerException e)
			{
				
			}
			try {
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jsonObject = jsonArray.getJSONObject(i);

					String mString_username = jsonObject.getString("name");

					mArrayList.add(mString_username);

				}

			} catch (Exception e) {

			}

			return null;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.cancel();
			pDialog.dismiss();
		
			StableArrayAdapter adapter = new StableArrayAdapter(
					getApplicationContext(), mArrayList);
			listView.setAdapter(adapter);

			listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		}

		@Override
		protected void onPreExecute() {

			pDialog.show();
			pDialog.setCancelable(false);

			super.onPreExecute();

		}

		@Override
		protected void onProgressUpdate(Void... values) {

			super.onProgressUpdate(values);

		}

	}

}
