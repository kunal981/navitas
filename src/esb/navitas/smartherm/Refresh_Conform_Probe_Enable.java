package esb.navitas.smartherm;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import JsonParser.JSONParserPost;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

@SuppressLint("SimpleDateFormat")
public class Refresh_Conform_Probe_Enable extends Activity {
	Button mButton_refresh;
	EditText mEditText;
	ProgressDialog pDialog;
	JSONArray jsonArray, json_Array;
	DatabaseHandler mDatabaseHandler;
	Progress_List mProgress_List;
	Cursor mCursor;
	Progress_ mProgress_;
	Progress_Supplier mProgress_Supplier;
	String mString_deciceid;

	String androidId, mString;
	int private_mode = 0;
	JSONObject json;
	JSONObject jsonObject;
	String encrypted_pin, encrypted_probeid;
	String mString_status, mString_probeid, mString_again, mString_status_menu,
			mString_status_user, mString_status_supplier,
			mString_status_services;

	SharedPreferences mSharedPreferences, mPreferences, mPreferences_probe;
	public static String My_Shared_All_Data = "All_Data";
	public static String My_Shared_Conform = "conform_screen";
	public static String My_Shared = "probeid_file";

	int private_moade = 0;
	SharedPreferences.Editor editor, mEditor, probe_editor;
	Bundle bundle;
	ConnectionDetector cd;
	String error_message = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.refresh_probe);

		pDialog = new ProgressDialog(this);

		mDatabaseHandler = new DatabaseHandler(this);
		mButton_refresh = (Button) findViewById(R.id.refresh_button);

		mSharedPreferences = this.getSharedPreferences(My_Shared_All_Data,
				private_moade);
		mPreferences = this.getSharedPreferences(My_Shared_Conform,
				private_mode);
		mPreferences_probe = this.getSharedPreferences(My_Shared, private_mode);
		probe_editor = mPreferences_probe.edit();
		editor = mSharedPreferences.edit();
		mEditor = mPreferences.edit();
		cd = new ConnectionDetector(getApplicationContext());
		mButton_refresh.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				System.gc();

				Boolean isInternetPresent = cd.isConnectingToInternet(); // true
				// or
				// false

				if (isInternetPresent == false) {
					show_alert("No Internet Connection", false);
					return;
				}

				encrypted_probeid = null;
				encrypted_pin = null;
				encrypted_probeid = mSharedPreferences.getString(
						"PROBEID_ENCRYPT", null);
				// Log.i("id_user:  ", "" + encrypted_probeid);
				encrypted_pin = mSharedPreferences.getString("PIN_ENCRYPT",
						null);
				// Log.i("mString_pin:  ", "" + encrypted_pin);

				new Progress_List().execute();

			}
		});

	}

	// post end message=====

	public class Progress_List extends AsyncTask<String, Void, String> {
		public Progress_List() {
		}

		@Override
		protected String doInBackground(String... params) {

			String URL = "http://test.gardnerjames.eu.com/api/navitas-app/get-users";
			JSONParserPost mJsonParserPost = new JSONParserPost();

			Log.d("users encrypted=====", "" + encrypted_pin);
			Log.d("users encrypted=====", "" + encrypted_probeid);

			HttpEntity se = null;
			try {

				json = new JSONObject();
				json.put("0", encrypted_probeid);
				json.put("1", encrypted_pin);

				se = new StringEntity(json.toString());

				// Log.i("parent====", "" + json);
			} catch (Exception e) {

				e.printStackTrace();
			}

			try {

			} catch (Exception e) {

				e.printStackTrace();
			}

			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(URL, se);

			

			try {
				mString_status_user = jsonobject.getString("status");
				error_message = jsonobject.getString("message");
				// Log.i("mString_status_user===", "" + mString_status_user);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NullPointerException e) {
				// TODO: handle exception
				mString_status_user = "Error!";
			}

			if (mString_status_user.equals("success")) {
				try {
					jsonArray = jsonobject.getJSONArray("result");
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				// Log.i("jsonArray---------", "" + jsonArray.length());

				try {
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);

						String mString_id = jsonObject.getString("id");
						String mString_username = jsonObject.getString("name");
						// String mString_email = jsonObject.getString("email");
						String mString_image = jsonObject.getString("avatar");

						// mArrayList_username.add(mString_username);
						// Log.i("mString_username:", "" + mString_username);
						// Log.i("mString_id:", "" + mString_id);
						// Log.i("mString_image:", "" + mString_image);

						mDatabaseHandler.Add_Listing(mString_id, null,
								mString_username, mString_image);

					}

				} catch (Exception e) {

				}

			}

			return null;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if (mString_status_user.equals("success")) {

				mEditor.putString("Screen_Decide", "Menu");
				mEditor.commit();

				mProgress_ = new Progress_();
				mProgress_.execute();
			} else if (mString_status_user.equals("Error!")) {
		
				pDialog.cancel();
				pDialog.dismiss();
				Toast.makeText(
						getApplicationContext(),
						"Unable To Connect To Server!\nPlease check your wifi Connection",
						Toast.LENGTH_LONG).show();
			} else {
				pDialog.cancel();
				pDialog.dismiss();
				if (error_message.equals("No data for this id probe.")) {
					show_alert(error_message, true);
				}

			}

		}

		@Override
		protected void onPreExecute() {

			pDialog.show();
			pDialog.setCancelable(false);

			super.onPreExecute();

		}

		@Override
		protected void onProgressUpdate(Void... values) {

			super.onProgressUpdate(values);

		}

	}

	public class Progress_ extends AsyncTask<String, Void, String> {
		public Progress_() {
		}

		@Override
		protected String doInBackground(String... params) {

			String URL = "http://test.gardnerjames.eu.com/api/navitas-app/get-menu";

			JSONParserPost mJsonParserPost = new JSONParserPost();

			Log.d("menu encrypted=====", "" + encrypted_pin);
			Log.d("menu encrypted=====", "" + encrypted_probeid);

			HttpEntity se = null;
			try {

				json = new JSONObject();
				json.put("0", encrypted_probeid);
				json.put("1", encrypted_pin);

				se = new StringEntity(json.toString());

				// Log.i("parent====", "" + json);
			} catch (Exception e) {

				e.printStackTrace();
			}

			try {

			} catch (Exception e) {

				e.printStackTrace();
			}

			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(URL, se);

			Log.i("mJsonObject string---------", "" + jsonobject.toString());

			try {
				mString_status_menu = jsonobject.getString("status");
				// Log.i("mString_status===", "" + mString_status_menu);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NullPointerException e) {
				mString_status_menu = "Error!";
			}

			if (mString_status_menu.equals("success")) {
				try {
					jsonArray = jsonobject.getJSONArray("result");
					try {
						for (int i = 0; i < jsonArray.length(); i++) {
							JSONObject jsonObject = jsonArray.getJSONObject(i);

							String mString_new_group_id = jsonObject
									.getString("id");
							String mString_new_group_name = jsonObject
									.getString("name");
							json_Array = jsonObject.getJSONArray("children");
							// Log.i("json_Array==", "" + json_Array.length());
							for (int j = 0; j < json_Array.length(); j++) {
								JSONObject json_Object = json_Array
										.getJSONObject(j);
								String name_inside = json_Object
										.getString("name");
								String id_inside = json_Object.getString("id");

								// Log.i("name_inside==", "" + name_inside);
								// Log.i("id_inside==", "" + id_inside);
								mDatabaseHandler.Add_Listing_Item(
										mString_new_group_id, name_inside,
										id_inside);

							}

							mDatabaseHandler.Add_Food(null, null, null,
									mString_new_group_name,
									mString_new_group_id);

						}

					} catch (Exception e) {

					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// Log.i("jsonArray---------", "" + jsonArray.length());
			}

			return null;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if (mString_status_menu.equals("Error!")) {
				pDialog.cancel();
				pDialog.dismiss();
				Toast.makeText(
						getApplicationContext(),
						"Unable To Connect To Server!\nPlease check your wifi Connection",
						Toast.LENGTH_LONG).show();
			} else {

				mProgress_Supplier = new Progress_Supplier();
				mProgress_Supplier.execute();
			}
		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();

		}

		@Override
		protected void onProgressUpdate(Void... values) {

			super.onProgressUpdate(values);

		}

	}

	public class Progress_Supplier extends AsyncTask<String, Void, String> {
		public Progress_Supplier() {
		}

		@Override
		protected String doInBackground(String... params) {

			String URL = "http://test.gardnerjames.eu.com/api/navitas-app/get-suppliers";

			JSONParserPost mJsonParserPost = new JSONParserPost();

			Log.d("Suppliers encrypted=====", "" + encrypted_pin);
			Log.d("Suppliers encrypted=====", "" + encrypted_probeid);

			HttpEntity se = null;
			try {

				json = new JSONObject();
				json.put("0", encrypted_probeid);
				json.put("1", encrypted_pin);

				se = new StringEntity(json.toString());

				// Log.i("parent====", "" + json);
			} catch (Exception e) {

				e.printStackTrace();
			}

			try {

			} catch (Exception e) {

				e.printStackTrace();
			}

			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(URL, se);

		

			try {
				mString_status_supplier = jsonobject.getString("status");
				// Log.i("mString_status===", "" + mString_status_menu);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (NullPointerException e) {
				// TODO Auto-generated catch block
				mString_status_supplier="Error!";
			}
			

			if (mString_status_supplier.equals("success")) {

				try {
					jsonArray = jsonobject.getJSONArray("result");
					// Log.i("jsonArray---------", "" + jsonArray.length());
					try {
						for (int i = 0; i < jsonArray.length(); i++) {
							JSONObject jsonObject = jsonArray.getJSONObject(i);

							String mString_email = jsonObject.getString("id");
							String mString_username = jsonObject
									.getString("name");
							String mString_image = jsonObject
									.getString("avatar");
							mDatabaseHandler.Add_Supplier(mString_email,
									mString_username, mString_image);

						}

					} catch (Exception e) {

					}
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}

			return null;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			pDialog.dismiss();
			pDialog.cancel();
			if (mString_status_supplier.equals("Error!")) {

				Toast.makeText(
						getApplicationContext(),
						"Unable To Connect To Server!\nPlease check your wifi Connection",
						Toast.LENGTH_LONG).show();
			} else {

				Intent intent_home_screen = new Intent(
						Refresh_Conform_Probe_Enable.this, MainMenu.class);

				startActivity(intent_home_screen);
				finish();
			}
			

		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();

		}

		@Override
		protected void onProgressUpdate(Void... values) {

			super.onProgressUpdate(values);

		}

	}

	// / services data get from services==========================
	public class Progress_Services extends AsyncTask<String, Void, String> {
		public Progress_Services() {
		}

		@Override
		protected String doInBackground(String... params) {

			String URL = "http://test.gardnerjames.eu.com/api/navitas-app/get-services";

			JSONParserPost mJsonParserPost = new JSONParserPost();

			Log.d("Suppliers encrypted=====", "" + encrypted_pin);
			Log.d("Suppliers encrypted=====", "" + encrypted_probeid);

			HttpEntity se = null;
			try {

				json = new JSONObject();
				json.put("0", encrypted_probeid);
				json.put("1", encrypted_pin);

				se = new StringEntity(json.toString());

				// Log.i("parent====", "" + json);
			} catch (Exception e) {

				e.printStackTrace();
			}

			try {

			} catch (Exception e) {

				e.printStackTrace();
			}

			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(URL, se);
			if (jsonobject == null) {
				mString_status_services = "Error converting result org.json.JSONException: Value <!DOCTYPE of type java.lang.String cannot be converted to JSONObject";

			} else {

				try {
					mString_status_services = jsonobject.getString("status");
					// Log.i("mString_status===", "" + mString_status_services);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch(NullPointerException e)
				{
					mString_status_services="Error!";
				}
			}

			if (mString_status_services.equals("success")) {

				try {
					jsonArray = jsonobject.getJSONArray("result");
					// Log.i("jsonArray---------", "" + jsonArray.length());
					try {
						for (int i = 0; i < jsonArray.length(); i++) {
							JSONObject jsonObject = jsonArray.getJSONObject(i);

							String mString_email = jsonObject.getString("id");
							String mString_username = jsonObject
									.getString("name");

							mDatabaseHandler.Add_Service(mString_email,
									mString_username);

						}

					} catch (Exception e) {

					}
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}

			return null;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		

		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();

		}

		@Override
		protected void onProgressUpdate(Void... values) {

			super.onProgressUpdate(values);

		}

	}

	private void show_alert(String message, final boolean value) {
		// TODO Auto-generated method stub
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				Refresh_Conform_Probe_Enable.this);

		// Setting Dialog Title
		alertDialog.setTitle("Error!");

		// Setting Dialog Message
		alertDialog.setMessage(message);

		// Setting Icon to Dialog
		alertDialog.setIcon(R.drawable.ic_launcher);

		// Setting Positive "Yes" Button
		alertDialog.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (value) {
							probe_editor.putString("PROBE_ID", null);
							probe_editor.commit();
						}
						System.exit(0);
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

}
