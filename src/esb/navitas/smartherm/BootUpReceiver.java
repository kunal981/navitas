package esb.navitas.smartherm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

//import android.app.AlarmManager;
//import android.app.PendingIntent;

//import android.util.Log;

/**
 * Created by jburger on 09/01/14.
 */
public class BootUpReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		Intent i = new Intent(context, Activit_Splash_class.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(i);
	}
	/*
	 * private int interval = 10000;
	 * 
	 * @Override public void onReceive(Context context, Intent intent) {
	 * Log.i("Autostart Message", "Received"); AlarmManager am = (AlarmManager)
	 * context.getSystemService(Context.ALARM_SERVICE); PendingIntent pi =
	 * PendingIntent.getService(context, 0, new Intent(context, MainMenu.class),
	 * PendingIntent.FLAG_UPDATE_CURRENT);
	 * am.setInexactRepeating(AlarmManager.RTC_WAKEUP,
	 * System.currentTimeMillis() + interval, interval, pi); }
	 */
}
