package esb.navitas.smartherm;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import JsonParser.JSONParserPost;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Bussiness_Id_Class extends Activity {
	Button mButton;
	ProgressDialog pDialog;
	JSONArray jsonArray;
	DatabaseHandler mDatabaseHandler;

	Cursor mCursor;

	String androidId, mString;

	JSONObject json;
	JSONObject jsonObject;

	MCrypt mCrypt;
	String encrypted = null;
	String encrypted_deviceid = null;
	String[] myStringArray;
	String mString_deciceid;
	public static String mString_probeid;
	String mString_status;
	SharedPreferences mSharedPreferences;
	public static String My_Shared = "probeid_file";

	int private_moade = 0;
	SharedPreferences.Editor editor;
	EditText mEditText;
	String mString_message="";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bussiness_id);
		mEditText = (EditText) findViewById(R.id.edit_bussiness);
		mButton = (Button) findViewById(R.id.hide_button);
		pDialog = new ProgressDialog(this);
		mString_probeid = null;

		mDatabaseHandler = new DatabaseHandler(this);
		androidId = Settings.Secure.getString(getContentResolver(),
				Settings.Secure.ANDROID_ID);

		mSharedPreferences = this
				.getSharedPreferences(My_Shared, private_moade);
		editor = mSharedPreferences.edit();

		mButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				System.gc();
				if (mEditText.getText().toString().length() != 0) {
					new Progress_List_Post().execute();
				} else {
					Toast.makeText(getApplicationContext(),
							"Please enter Company Identifier",
							Toast.LENGTH_LONG).show();
				}
				

			}
		});

	}

	// post message=======

	public static Timestamp convertToTimestamp(String stringToFormat) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:00:00");
		try {
			Date date = dateFormat.parse(stringToFormat);
			Timestamp tstamp = new Timestamp(date.getTime());
			return tstamp;
		} catch (ParseException e) {
			return null;
		}
	}

	public class Progress_List_Post extends AsyncTask<String, Void, String> {
		public Progress_List_Post() {
		}

		@Override
		protected String doInBackground(String... params) {

			// http://mrwconnected.info/berkshares/encode.php
			// http://test.gardnerjames.eu.com/api/navitas-app/create
			String URL = "http://test.gardnerjames.eu.com/api/navitas-app/create";

			JSONParserPost mJsonParserPost = new JSONParserPost();
			mCrypt = new MCrypt();
			// ArrayList<NameValuePair> nameValuePairs = new
			// ArrayList<NameValuePair>();
			// Log.e("androidId+ts=====", "" + androidId + ts);
			// Log.e("ts=====", "" + ts);
			// Log.e("androidId=====", "" + androidId);
			// mString="co1navitas";
			mString_deciceid = androidId;
			Log.d("mString_deciceid=====", "" + mString_deciceid);

			// mRijndaelCrypt=new RijndaelCrypt("amit123456789000");

			try {
				encrypted = MCrypt.bytesToHex(mCrypt.encrypt(mEditText
						.getText().toString()));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				encrypted_deviceid = MCrypt.bytesToHex(mCrypt
						.encrypt(mString_deciceid));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Log.i("encrypted=====", "" + encrypted);
			Log.i("encrypted_deviceid=====", "" + encrypted_deviceid);

			// /* Decrypt */
			//
			// try {
			// String decrypted = new String(mCrypt.decrypt(encrypted));
			// } catch (Exception e1) {
			// // TODO Auto-generated catch block
			// e1.printStackTrace();
			// }
			HttpEntity se = null;
			try {

				//
				// mString="co1navitas";
				// String mString2="n$V!T$S$Pa.";
				// mString_deciceid=androidId;

				json = new JSONObject();
				json.put("0", encrypted_deviceid);
				json.put("1", encrypted);

				se = new StringEntity(json.toString());

				// Log.e("jsonObject====", ""+jsonObject);
				Log.i("parent====", "" + json);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				// nameValuePairs.add(new
				// BasicNameValuePair("",parent.toString()));
				// nameValuePairs.add(new BasicNameValuePair("",encrypted));

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// nameValuePairs.add(new BasicNameValuePair("device_id",
			// androidId+ts));

			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(URL, se);
			Log.i("jsonobject=====", "" + jsonobject);
			Log.i("se=====", "" + se);
		
			try {
				mString_status = jsonobject.getString("status");
				Log.i("mString_status===", "" + mString_status);
		
			
				mString_message = jsonobject.getString("message");
				Log.i("mString_message===", "" + mString_message);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (NullPointerException e) {
				// TODO Auto-generated catch block
				if(mString_message.equals(""))
				mString_status="Error!";
			}
			if (mString_status.equals("success")) {
				try {
					mString_probeid = jsonobject.getString("id");
					Log.i("testing=======", "" + mString_probeid);
					editor.putString("PROBE_ID", mString_probeid);
					editor.putString("DEVICE_ID", encrypted_deviceid);
					editor.commit();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			return null;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.cancel();
			pDialog.dismiss();

			if (mString_status.equals("success")) {
				Intent intent_home_screen = new Intent(Bussiness_Id_Class.this,
						Pin_Screen.class);
				intent_home_screen.putExtra("ProbeId", mString_probeid);
				intent_home_screen.putExtra("DEVICEID", encrypted_deviceid);
				startActivity(intent_home_screen);
				finish();

			} else if(mString_status.equals("Error!")){
				mEditText.setText("");
				Toast.makeText(getApplicationContext(),
						"Unable To Connect To Server!\nPlease check your wifi Connection", Toast.LENGTH_LONG).show();
			}
			else
			{
				mEditText.setText("");
				Toast.makeText(getApplicationContext(),
						mString_message, Toast.LENGTH_LONG).show();	
			}
		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			pDialog.show();
		}

		@Override
		protected void onProgressUpdate(Void... values) {

			super.onProgressUpdate(values);

		}

	}

}
