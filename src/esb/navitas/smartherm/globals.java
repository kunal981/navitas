package esb.navitas.smartherm;

import android.app.Application;
import android.util.Log;

/**
 * Created by jburger on 07/01/14.
 */
public class globals extends Application {
	/* User name globals */
	public String goodsUser = null, cookUser = null, chillUser = null,
			reheatUser = null, hotServiceUser = null, coldServiceUser = null,
			calibrationUser = null;
	public int goodsUserPosition = 0, cookUserPosition = 0,
			chillUserPosition = 0, reheatUserPosition = 0,
			hotServiceUserPosition = 0, coldServiceUserPosition = 0,
			calibrationUserPosition = 0;
	/* Goods In globals */
	public boolean rawMeat = false, fish = false, cookedMeat = false,
			dairy = false, bakery = false, vegetables = false, frozen = false,
			other = false, packaging = false, dateCode = false,
			questionPick = false;
	public String goodsSupplier = null, goodsInvoiceNumber = null;
	/* Cooking */
	public int cookFoodGroup = 0, cookFoodItem = 0;
	/* Re-Heating */
	public int reheatFoodGroup = 0, reheatFoodItem = 0;
	/* Hot Service */
	public int hotServiceFoodGroup = 0, hotServiceFoodItem = 0;
	/* Cold Service */
	public int coldServiceFoodGroup = 0, coldServiceFoodItem = 0;
	/* Chilling */
	public int chillBeginFoodGroup = 0, chillBeginFoodItem = 0,
			chillFinishFoodGroup = 0, chillFinishFoodItem = 0;
	// public String chillFinishFoodItemName = null;
	/* Probe globals */
	public String probeID = null, newProbeID = null;// Is type of probe
													// needed???
	public final static int PROBE_UPDATE = 5;

	public globals() {
		super();
		Log.i("Globals", "globals constructor()");
	}
}
