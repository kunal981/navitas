package esb.navitas.smartherm;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import JsonParser.JSONARRAY;
import JsonParser.JSONParserPost;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Common_Method {
	JSONObject json;
	String mString_status;
	String error_message;
	DatabaseHandler db;
	Context context;

	// constructor
	public Common_Method(Context context) {
		db = new DatabaseHandler(context);

	}

	public void save_data_offline(String probeid, String pin, String suppid,
			String userid, String foodid, String tempvalue,
			String status_chilling) {
		db.Add_OFFLINE_DATA(probeid, pin, suppid, userid, foodid, tempvalue,
				status_chilling);
		db.close();

	}

	public String json_method_call(String probeid, String pin, String suppid,
			String userid, String foodid, String tempvalue,
			String status_chilling) {

		try {

			String URL = "http://test.gardnerjames.eu.com/api/navitas-app/service-temperature";

			JSONParserPost mJsonParserPost = new JSONParserPost();

			//Log.e("probeid=====", "" + probeid);
			//Log.e("pin=====", "" + pin);
			//Log.e("suppid=====", "" + suppid);
			//Log.e("userid=====", "" + userid);
			//Log.e("foodid=====", "" + foodid);

			//Log.e("tempvalue=====", "" + tempvalue);

			HttpEntity se = null;

			json = new JSONObject();
			json.put("0", probeid);
			json.put("1", pin);
			json.put("a_id", suppid);
			json.put("u_id", userid);
			json.put("f_id", foodid);
			json.put("t_val", tempvalue);
			json.put("status", status_chilling);

			se = new StringEntity(json.toString());

			// //Log.e("jsonObject====", ""+jsonObject);
			//Log.e("parent====", "" + json);

			//Log.e("parent====Check Value ", "" + json);
			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(URL, se);
			//Log.e("jsonobject=====", "" + jsonobject);

			// //Log.e("se=====", "" + se);

			mString_status = jsonobject.getString("status");
			error_message = jsonobject.getString("message");
			//Log.e("error_message===", "" + error_message);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			error_message = e.getMessage();
		} catch (Exception e) {
			error_message = e.getMessage();
		}
		if (mString_status.equals("success"))
			return mString_status;
		else
			return error_message;
	}

	public String upload_offline(String column_id, String probeid, String pin,
			String suppid, String userid, String foodid, String tempvalue,
			String status_chilling) {

	Progress_List progress_list = new Progress_List();
	progress_list.execute(column_id, probeid, pin, suppid, userid, foodid,
			tempvalue, status_chilling);
	

		return null;
	}

	public class Progress_List extends AsyncTask<String, Void, String> {

		String mString_status;
		String error_message;
		String column_key;

		String my_user_id;

		String food_item_id;
		String chill_staus;
		String aid;

		public Progress_List() {

		}

		@Override
		protected String doInBackground(String... params) {
			try {
				String URL = "http://test.gardnerjames.eu.com/api/navitas-app/service-temperature";

				JSONParserPost mJsonParserPost = new JSONParserPost();

				HttpEntity se = null;

				column_key = params[0];
				my_user_id = params[4];
				food_item_id = params[5];
				chill_staus = params[7];
				aid = params[3];
				json = new JSONObject();
				json.put("0", params[1]);
				json.put("1", params[2]);
				json.put("a_id", params[3]);
				json.put("u_id", params[4]);

				json.put("f_id", params[5]);
				json.put("t_val", params[6]);
				json.put("status", params[7]);

				se = new StringEntity(json.toString());

				// //Log.e("jsonObject====", ""+jsonObject);

				JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(URL, se);
				

				mString_status = jsonobject.getString("status");
				error_message = jsonobject.getString("message");
				Log.e("error_message===ISDDDD ", "" +  params[0]+"  "+ params[4] +"  "+params[5]+"  "+params[7]);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				error_message = e.getMessage();
			} catch (Exception e) {
				error_message = e.getMessage();
			}
		
			if (mString_status.equals("success")) {
				// db.REMOVE_OFFLINE_ITEM(column_id);
				return mString_status;

			} else
				return error_message;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (result.equals("success")) {
				db.REMOVE_OFFLINE_ITEM(column_key);
	
				if (aid == "chilling" && chill_staus == "1")
				{
					 SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa dd/MM/yyyy");
			           String currentDateandTime = sdf.format(new Date());
					db.Add_CHILL_ITEM(my_user_id, food_item_id,currentDateandTime);
				}
				else if (aid == "chilling" && chill_staus == "2") {
					db.REMOVE_CHILL_ITEM(food_item_id, my_user_id);
				}
				
				Log.e("Afetre Successsss===ISDDDD ", "food_item_id  " +  food_item_id+"  my_user_id  "+my_user_id +"  chill_staus  "+chill_staus);
			}
			db.close();

		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();

		}

		@Override
		protected void onProgressUpdate(Void... values) {

			super.onProgressUpdate(values);

		}

	}
}
