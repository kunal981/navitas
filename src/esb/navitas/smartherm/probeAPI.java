/**********************************************************************************/
/**	Low level interface to i.Mx53 board for Navitas SmartProbe					  */
/**		    																	  */
/**	@file probeAPI.java (source file)											  */
/**	@date 4th March 2014														  */
/**	@author J Burger (Design Engineer)											  */
/**																				  */
/**	@brief Provide functions to hardware interface on i.Mx53					  */
/**																				  */
/**	@version																	  */
/**	Rev:	0.1	04/03/2014 � Draft Version										  */
/** 		0.11 10/03/2014 - Changed getTemperature() function					  */
/**																				  */
/**	@attention																	  */
/**	(C) Copyright 2014, Datalink Electronics Ltd								  */
/**																				  */
/**********************************************************************************/
package esb.navitas.smartherm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import android.content.Context;
import android.gpio.GPIO;
import android.gpio.GPIOException;
import android.spi.SPI;
import android.util.Log;
//import android.gpioException;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

public class probeAPI {
	private static SPI mSPI;
	private static GPIO convertGPIO;
	private static GPIO hapticGPIO;
	/* DISABLE IF TRIGGER INTERRUPT FUNCTION WANTED */
	private static GPIO triggerGPIO;
	/* DISABLE IF TRIGGER INTERRUPT FUNCTION WANTED */
	private static final double vRef = 2.5;
	private static final double ADCmax = 65536;
	private static final double r1 = 10000.0; // 10kohm
	private static final double kelvin = 273.15;
	private static final double SHa = 0.000826887;
	private static final double SHb = 0.000208820;
	private static final double SHc = 0.000000081;
	private static final int noSamples = 100;// every ~ 0.1 seconds return an
												// averaged temperature sample
	private static double R2coef = 0.0;

	// averaged temperature sample
	// private static MainMenu con;

	/**
	 * @brief Initialise the hardware interface and variables.
	 * @param None
	 * @retval None
	 */
	public static boolean initialise() {
		try {
			initialiseSPI();
			convertGPIO = new GPIO(155, GPIO.MODE_OUTPUT);
			hapticGPIO = new GPIO(116, GPIO.MODE_OUTPUT);
			/* DISABLE IF TRIGGER INTERRUPT FUNCTION WANTED */
			triggerGPIO = new GPIO(96, GPIO.MODE_INPUT);
			/* DISABLE IF TRIGGER INTERRUPT FUNCTION WANTED */
			convertGPIO.setState(false);
			hapticGPIO.setState(false);
			R2coef = vRef / ADCmax;
			writeFile(new File("/sys/class/backlight/WLED-5/brightness"), "95");
			return true;
		} catch (GPIOException e) {
			// e.printStackTrace();
			return false;
		}
	}

	// public static void setContext(MainMenu value) {
	// con=value;

	// }

	/**
	 * @brief Run haptic device for a defined period.
	 * @param Period
	 *            in ms.
	 * @retval None
	 */
	public static void runHaptic(int period) {
		try {
			hapticGPIO.setState(true);
			Thread.sleep(period);
			hapticGPIO.setState(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @brief Get temperature reading from the ADC. Takes ~0.1 seconds to return
	 *        an averaged temperature reading
	 * @param None
	 * @retval Temperature to 1 decimal point or -100 for error.
	 */
	public static double getTemperature() {
		if (isProbeAttached()) {
			double l = 0, k = 0;
			for (int i = 0; i < noSamples; i++) {
				k = (double) probeAPI.readADCData();
				if (k == -100)
					return -100;
				else
					l += k / noSamples;
			}
			double vOut = l * R2coef;
			double r2 = r1 / ((vRef / vOut) - 1);
			return steinhartTempCalculator(r2);
		}
		return -100;
	}

	/* DISABLE IF TRIGGER INTERRUPT FUNCTION WANTED */
	/**
	 * @brief Get trigger switch state.
	 * @param None
	 * @retval true if trigger is pressed. False when not pressed or error.
	 */
	public static boolean getTriggerState() {
		try {
			return !triggerGPIO.getState();
		} catch (GPIOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/* DISABLE IF TRIGGER INTERRUPT FUNCTION WANTED */

	/**
	 * @brief Check if a 1-wire device is attached.
	 * @param None
	 * @retval True if attached, false if no device or error.
	 */
	public static boolean isProbeAttached() {
		int slaveCount = 0;

		try {
			slaveCount = Integer.parseInt(readFile(new File(
					"/sys/devices/w1 bus master/w1_master_slave_count")));
			//Log.i("slaveCount================", "" + slaveCount);
		} catch (Exception e) {
			//e.printStackTrace();
			return false;
		}

		if (slaveCount == 1)
			return true;
		else
			return false;
	}

	/**
	 * @brief Retrieve unique ID of the ID chip in probe.
	 * @param None
	 * @retval 12 character unique ID.
	 */
	public static String getProbeID() {
		String id = "";
		//Log.i("isProbeAttached()==========", "" + isProbeAttached());
		if (isProbeAttached()) {
			try {
				id = readFile(new File(
						"/sys/devices/w1 bus master/w1_master_slaves"));
				//Log.i("id===", "" + id);
			} catch (Exception e) {
				e.printStackTrace();
				//Log.i("id=========", "" + id);
				return null;
			}

			if (id.startsWith("2d-")) {
				String[] idn = id.split("-");
				return idn[1];
			}
		}

		return null;
	}

	/**
	 * @brief Read from EEPROM of the ID chip.
	 * @param None
	 * @retval Contents of the EEPROM.
	 */
	public static String readProbeMemory() {
		String id = getProbeID();
		//Log.i("id=========", "" + id);
		String data;

		if (id != null) {
			try {
				data = readFile(new File("/sys/devices/w1 bus master/2d-" + id
						+ "/eeprom"));
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}

			return data;
		}

		return null;
	}

	/**
	 * @brief Write to EEPROM of the ID chip.
	 * @param Up
	 *            to 64 character string.
	 * @retval None
	 */
	public static boolean writeProbeMemory(String data) {
		/* Scratchpad size 64bytes */
		if (data.length() < 65) {
			String id = getProbeID();

			if (id != null) {
				try {
					writeFile(new File("/sys/devices/w1 bus master/2d-" + id
							+ "/eeprom"), data);
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
				return true;
			}
			return false;
		}
		return false;
	}

	/* Helper functions for probeAPI */
	private static void initialiseSPI() {
		mSPI = new SPI(1, 0);
		try {
			mSPI.open(0, 8, 500000);
			mSPI.setMode(0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static long readADCData() {
		try {
			convertGPIO.setState(true);
			Thread.sleep(0, 5000);
			convertGPIO.setState(false);
			Thread.sleep(0, 5000);

			byte[] rx_data = mSPI.read(2);
			long a = rx_data[0] & 0x000000FF;
			long b = rx_data[1] & 0x000000FF;
			long t = (a * 256) + b;
			return t;// String.valueOf(t)
		} catch (Exception e) {
			e.printStackTrace();
			return -100;// null
		}
	}

	private static double steinhartTempCalculator(double res) {
		double logR = Math.log(res);
		double t = (1 / (SHa + SHb * logR + SHc * (Math.pow(logR, 3))))
				- kelvin;
		return Math.round(t * 10) / 10.0d;
	}

	private static String readFile(File file) {
		if (!file.exists())
			return null;

		BufferedReader reader;

		try {
			reader = new BufferedReader(new FileReader(file), 8);
			String value = reader.readLine();
			reader.close();
			return value.trim();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	private static void writeFile(File file, String s) {
		BufferedWriter writer;

		try {
			writer = new BufferedWriter(new FileWriter(file));
			writer.write(s);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/* Helper functions for probeAPI */

	/*
	 * Function to alter bar at the bottom of screens according to the probe
	 * attached
	 */
	public static void check_probe(View view) {
		TextView probeView = (TextView) view;
		Context context = view.getContext();
		if (probeAPI.isProbeAttached()) {
			try {
				String s = probeAPI.readProbeMemory();
				char[] probe = s.toCharArray();
				switch (probe[1]) {
				case 'H':
					probeView.setText(R.string.halal);
					probeView.setBackgroundColor(context.getResources()
							.getColor(R.color.yellow));
					probeView.setTextColor(context.getResources().getColor(
							R.color.black));
					// WindowManager.LayoutParams mLayoutParams
					// =con.getWindow().getAttributes();
					// mLayoutParams.flags =
					// WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
					// TODO Store original brightness value
					// mLayoutParams.screenBrightness = -1f;
					// con.getWindow().setAttributes(mLayoutParams);

					break;
				case 'E':
					probeView.setText(R.string.eat_probe);
					probeView.setBackgroundColor(context.getResources()
							.getColor(R.color.green));
					probeView.setTextColor(context.getResources().getColor(
							R.color.white));
					// WindowManager.LayoutParams mLayout_Params
					// =con.getWindow().getAttributes();
					// mLayout_Params.flags =
					// WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
					// TODO Store original brightness value
					// mLayout_Params.screenBrightness = -1f;
					// con.getWindow().setAttributes(mLayout_Params);
					break;
				case 'M':
					probeView.setText(R.string.raw_meat_probe);
					probeView.setBackgroundColor(context.getResources()
							.getColor(R.color.red));
					probeView.setTextColor(context.getResources().getColor(
							R.color.white));
					// WindowManager.LayoutParams _mLayoutParams
					// =con.getWindow().getAttributes();
					// _mLayoutParams.flags =
					// WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
					// TODO Store original brightness value
					// _mLayoutParams.screenBrightness = -1f;
					// con.getWindow().setAttributes(_mLayoutParams);
					break;
				default:
					probeView.setText(R.string.not_rec_probe);
					probeView.setBackgroundColor(context.getResources()
							.getColor(R.color.black));
					probeView.setTextColor(context.getResources().getColor(
							R.color.white));
					// WindowManager.LayoutParams m_LayoutParams
					// =con.getWindow().getAttributes();
					// m_LayoutParams.flags =
					// WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
					// TODO Store original brightness value
					// m_LayoutParams.screenBrightness = -1f;
					// con.getWindow().setAttributes(m_LayoutParams);

					break;
				}
			} catch (Exception e) {
				e.printStackTrace();
				probeView.setText(R.string.not_rec_probe);
				probeView.setBackgroundColor(context.getResources().getColor(
						R.color.black));
				probeView.setTextColor(context.getResources().getColor(
						R.color.white));
				// WindowManager.LayoutParams _mLayoutParams_
				// =con.getWindow().getAttributes();
				// _mLayoutParams_.flags =
				// WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
				// TODO Store original brightness value
				// _mLayoutParams_.screenBrightness = -1f;
				// con.getWindow().setAttributes(_mLayoutParams_);
			}
		} else {
			probeView.setText(R.string.no_probe);
			probeView.setBackgroundColor(context.getResources().getColor(
					R.color.gray));
			probeView.setTextColor(context.getResources().getColor(
					R.color.white));
		}
	}
	/*
	 * Function to alter bar at the bottom of screens according to the probe
	 * attached
	 */
}

/***************** (C) Copyright 2014, Datalink Electronics Ltd ********************/
/********************************** END OF FILE ************************************/
