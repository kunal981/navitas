package esb.navitas.smartherm;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.gpio.GPIO;
import android.gpio.GPIOException;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import esb.navitas.smartherm.Panel.OnPanelListener;

//import android.support.v7.app.ActionBarActivity;

/**
 * Created by jburger on 07/01/14.
 */
/* public class chilling_begin_temp extends ActionBarActivity */
public class chilling_begin_temp extends Activity implements OnPanelListener,
		OnClickListener {

	private Panel left_panel;
	Panel panel;
	ProgressDialog mDialog;
	TextView textView;
	String androidId, mString_probeid, mString_encrypt_final_probe_id,
			mString_encrypt_final_pin, mString_success;

	SharedPreferences mSharedPreferences, mPreferences_all_data;
	public static String My_Shared = "probeid_file";
	public static String My_Shared_All_Data = "All_Data";

	int private_moade = 0;
	int private_mode_all_data = 0;
	SharedPreferences.Editor editor, mEditor;
	Cursor mCursor;
	DatabaseHandler mDatabaseHandler;
	TextView textView_home, textView_goods_in, textView_chilling,
			textView_reheating, textView_hotserv, textView_coldserv,
			textview_calibration, textView_cooking;
	TextView textview_active;
	TextView textView_supplier_name;
	ImageView imageView_supplier_pic;
	String supp_name, mString_supp_name_id;
	Progress_List mProgress_List;

	// GPIO numbers
	private final static int GPIO_BUTTON_1_MX53 = 96;
	// Variables
	private GPIO pushButtonGPIO;
	TextView tempView;
	// Action enumeration
	private final static int PUSH_BUTTON_PRESSED = 0;
	private final static int PUSH_BUTTON_RELEASED = 1;
	private final static int TEMP_VIEW_UPDATE = 2;

	private PushButtonTask pushButtonTask;
	private TempUpdateTask tempUpdateTask;

	private boolean running = false;
	private boolean runningTemp = false;
	String mString_foodgroupid, mString_fooditemid, mString_serviceid;
	
	private String temperature = "0";

	// Handler to take care of UI actions called from other threads
	private Handler myHandler = new Handler() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.Handler#handleMessage(android.os.Message)
		 */
		public void handleMessage(Message msg) {
			switch (msg.what) {
			// Check which kind of message was received to perform required
			// actions.
			case (PUSH_BUTTON_PRESSED):
				// performPushButtonGPIOAction(false);
				new backgroundHaptic().execute();
				if (runningTemp == false)
					startTempUpdateTask();
				else
					stopTempUpdateTask();
				break;
			case (PUSH_BUTTON_RELEASED):
				// performPushButtonGPIOAction(true);
				break;
			case (TEMP_VIEW_UPDATE):
				updateTextView();
				break;
			}
		}
	};

	/**
	 * Initializes all the GPIOs that will be used in the application.
	 */
	private void initializeGPIOs() {
		try {
			pushButtonGPIO = new GPIO(GPIO_BUTTON_1_MX53,
					GPIO.MODE_INTERRUPT_EDGE_BOTH);
		} catch (GPIOException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * Initializes background asynchronous task that take care of listening for
	 * interrupt events on board button and perform required actions on LED GPIO
	 * and graphics.
	 */
	private void initializeTask() {
		// Set global running variable to true.
		running = true;
		// Declare task
		pushButtonTask = new PushButtonTask();
		// Start task.
		pushButtonTask.execute();
	}

	private void startTempUpdateTask() {
		runningTemp = true;
		// Declare task
		tempUpdateTask = new TempUpdateTask();
		// Start task
		tempUpdateTask.execute();
	}

	/**
	 * Stop asynchronous background task that were taking care of checking GPIO
	 * button interrupt events.
	 */

	private void stopTask() {
		// Set global running variable to false.
		try {
		running = false;
		// Stop waiting for interrupt on GPIO.
		pushButtonGPIO.stopWaitingForInterrupt();
		// Give time to propagate stop request.
	
			Thread.sleep(1000);
		
		// Cancel tasks.
		pushButtonTask.cancel(true);
		pushButtonTask = null;
		} catch (InterruptedException e) {
		}
		catch(NullPointerException e)
		{}
	}

	/**
	 * Stop asynchronous background task that were taking care of Temperature
	 * Readout events.
	 */
	private void stopTempUpdateTask() {
		// Set global running variable to false.
		runningTemp = false;
		// Give time to propagate stop request.
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		// Cancel tasks.
		tempUpdateTask.cancel(true);
		tempUpdateTask = null;
	}

	private class backgroundHaptic extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			probeAPI.runHaptic(300);
			return (null);
		}
	}

	private void updateTextView() {
		tempView.setText(temperature + "\u2103");
		// \u2103
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onDestroy()
	 */
	protected void onPause() {
		// Stop background task
		try
		{
		stopTask();
		if (runningTemp == true)
			stopTempUpdateTask();
		pushButtonGPIO = null;
		running = false;
		runningTemp = false;
		
	} catch(NullPointerException e)
	{}
		super.onPause();
	}

	/**
	 * Background asynchronous task that takes care of listening for interrupt
	 * events on the board push button (User Button 1) and perform required
	 * actions.
	 * 
	 */
	private class PushButtonTask extends AsyncTask<Void, Void, Void> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */
		protected Void doInBackground(Void... params) {
			while (running) {
				try {
					boolean state = pushButtonGPIO.waitForInterrupt(0, 100);
					if (running) {
						// Send event to handler to perform required actions.
						if (!state)
							myHandler.sendEmptyMessage(PUSH_BUTTON_PRESSED);
						else
							myHandler.sendEmptyMessage(PUSH_BUTTON_RELEASED);
					}
				} catch (GPIOException e) {
					e.printStackTrace();
				}
			}
			return (null);
		}
	}

	/**
	 * Background asynchronous task that takes care of updating the temperature
	 * readout
	 * 
	 */
	private class TempUpdateTask extends AsyncTask<Void, Void, Void> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */
		protected Void doInBackground(Void... params) {
			while (runningTemp) {
				try {
					double val = 0;

					val = probeAPI.getTemperature();

					temperature = String.valueOf(val);
					myHandler.sendEmptyMessage(TEMP_VIEW_UPDATE);
					// Thread.sleep(1000);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return (null);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chilling_begin_temp);
	
		// Initialize application GPIOs.
		mDialog = new ProgressDialog(this);
		androidId = Settings.Secure.getString(getContentResolver(),
				Settings.Secure.ANDROID_ID);
		mDatabaseHandler = new DatabaseHandler(this);
		// mString_probeid=probeAPI.getProbeID();

		mSharedPreferences = this
				.getSharedPreferences(My_Shared, private_moade);

		editor = mSharedPreferences.edit();

		mPreferences_all_data = this.getSharedPreferences(My_Shared_All_Data,
				private_mode_all_data);
		mEditor = mPreferences_all_data.edit();

		mString_probeid = mSharedPreferences.getString("PROBE_ID", null);
		//Log.e("id_user:  ", "" + mString_probeid);
		textView_supplier_name = (TextView) findViewById(R.id.textView_supp_name);
		imageView_supplier_pic = (ImageView) findViewById(R.id.imageView_supp_image);
		supp_name = getIntent().getStringExtra("supp_name");
		// user id
		mString_supp_name_id = getIntent().getStringExtra("supp_name_id");
		textView_supplier_name.setText(supp_name);
		mString_foodgroupid = getIntent().getStringExtra("foodgroupid");
		mString_fooditemid = getIntent().getStringExtra("fooditemid");
		textview_calibration = (TextView) findViewById(R.id.textView_callibration);
		textView_chilling = (TextView) findViewById(R.id.textView_chilling);
		textView_coldserv = (TextView) findViewById(R.id.textView_cold);
		textView_goods_in = (TextView) findViewById(R.id.textView_goods_in);
		textView_home = (TextView) findViewById(R.id.textView_home);
		textView_hotserv = (TextView) findViewById(R.id.textView_hotservice);
		textView_reheating = (TextView) findViewById(R.id.textView_reheating);
		textView_cooking = (TextView) findViewById(R.id.textView_cooking);

		textview_calibration.setOnClickListener(this);
		textView_chilling.setOnClickListener(this);
		textView_coldserv.setOnClickListener(this);
		textView_goods_in.setOnClickListener(this);
		textView_home.setOnClickListener(this);
		textView_hotserv.setOnClickListener(this);
		textView_reheating.setOnClickListener(this);
		textView_cooking.setOnClickListener(this);
		left_panel = panel = (Panel) findViewById(R.id.leftPanel1);
		panel.setOnPanelListener(this);

		findViewById(R.id.smoothButton1).setOnClickListener(
				new OnClickListener() {
					public void onClick(View v) {
						left_panel.setOpen(!left_panel.isOpen(), true);
					}
				});

		mString_encrypt_final_probe_id = mPreferences_all_data.getString(
				"PROBEID_ENCRYPT", null);
		mString_encrypt_final_pin = mPreferences_all_data.getString(
				"PIN_ENCRYPT", null);

		mString_serviceid = "chilling";

		initializeGPIOs();
		// Initialize application background task to check for interrupts on
		// button GPIO.
		initializeTask();
		tempView = (TextView) findViewById(R.id.chilling_begin_temp_textView_temp);
		startTempUpdateTask();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.user_selected_menu, menu);
		globals global = (globals) getApplication();
		if (global.chillUser != null) {
			MenuItem txtUser = menu.findItem(R.id.user_display);
			txtUser.setTitle(global.chillUser);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		Intent intent;
		/*
		 * if (id == R.id.action_settings) { return true; }
		 */
		switch (id) {
		case R.id.user_display:
			intent = new Intent(getApplicationContext(), chilling_user.class);
			startActivity(intent);
			break;
		case R.id.home_menu:
			intent = new Intent(getApplicationContext(), MainMenu.class);
			startActivity(intent);
			break;
		case R.id.goods_menu:
			intent = new Intent(getApplicationContext(), goods_user.class);
			startActivity(intent);
			break;
		case R.id.cook_menu:
			intent = new Intent(getApplicationContext(), cook_user.class);
			startActivity(intent);
			break;
		case R.id.chill_menu:
			intent = new Intent(getApplicationContext(), chilling_user.class);
			startActivity(intent);
			break;
		case R.id.reheat_menu:
			intent = new Intent(getApplicationContext(), reheat_user.class);
			startActivity(intent);
			break;
		case R.id.hot_serv_menu:
			intent = new Intent(getApplicationContext(), hot_service_user.class);
			startActivity(intent);
			break;
		case R.id.cold_serv_menu:
			intent = new Intent(getApplicationContext(),
					cold_service_user.class);
			startActivity(intent);
			break;
		case R.id.cal_menu:
			intent = new Intent(getApplicationContext(), calibration_user.class);
			startActivity(intent);
			break;
		default:
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/* Called when the user clicks the Submit button */
	public void clk_btn_chilling_begin_temp_submit(View view) {
		// TODO Do something in response to Submit button click and return to
		// main menu
		textView = (TextView) findViewById(R.id.chilling_begin_temp_textView_temp);
		//Log.i("Submit Temp = ", textView.getText().toString());
		//Log.i("Supllier = ", supp_name);
		if(temperature.equals("-100.0"))
		{
			show_probe("Please Check Probe Connection. Wrong Temperature Value");
			return;
		}
	ConnectionDetector	cd = new ConnectionDetector(getApplicationContext());

		Boolean isInternetPresent = cd.isConnectingToInternet(); // true or
		if (isInternetPresent == false) {

				show_navigate("Want to save Temperature Offline?");
			return;
		}
		mProgress_List = new Progress_List();
		mProgress_List.execute();

	}

	public void clk_btn_back_view(View view) {
		// TODO Do something in response to Submit button click and return to
		// main menu
		// globals global = (globals) getApplication();
		finish();
	}

	/**
	 * 
	 * Click Event On SideBar Menus.....
	 */

	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent;
		switch (v.getId()) {

		case R.id.textView_home:
			left_panel.setOpen(!left_panel.isOpen(), true);
			textView_chilling.setBackgroundResource(R.drawable.list_background);
			textView_chilling.setTextColor(Color.WHITE);
			textView_home.setBackgroundResource(R.drawable.button_background);
			intent = new Intent(this, MainMenu.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;
		case R.id.textView_goods_in:

			textView_chilling.setBackgroundResource(R.drawable.list_background);
			textView_chilling.setTextColor(Color.WHITE);
			textView_goods_in
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, goods_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		case R.id.textView_cooking:

			textView_chilling.setBackgroundResource(R.drawable.list_background);
			textView_chilling.setTextColor(Color.WHITE);
			textView_cooking
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, cook_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();

			break;

		case R.id.textView_chilling:

			left_panel.setOpen(!left_panel.isOpen(), true);

			break;

		case R.id.textView_reheating:
			textView_chilling.setBackgroundResource(R.drawable.list_background);
			textView_chilling.setTextColor(Color.WHITE);
			textView_reheating
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, reheat_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		case R.id.textView_hotservice:
			textView_chilling.setBackgroundResource(R.drawable.list_background);
			textView_chilling.setTextColor(Color.WHITE);
			textView_hotserv
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, hot_service_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		case R.id.textView_cold:
			textView_chilling.setBackgroundResource(R.drawable.list_background);
			textView_chilling.setTextColor(Color.WHITE);
			textView_coldserv
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, cold_service_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		case R.id.textView_callibration:
			textView_chilling.setBackgroundResource(R.drawable.list_background);
			textView_chilling.setTextColor(Color.WHITE);
			textview_calibration
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, calibration_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		default:
			break;
		}
	}

	/**
	 * 
	 * Panel Handling Intefrace
	 */

	public void onPanelClosed(Panel panel) {
		String panelName = getResources().getResourceEntryName(panel.getId());
		//Log.d("Test", "Panel [" + panelName + "] closed");
		findViewById(R.id.trans_region).setVisibility(View.GONE);

	}

	public void onPanelOpened(Panel panel) {
		String panelName = getResources().getResourceEntryName(panel.getId());
		//Log.d("Test", "Panel [" + panelName + "] opened");
		findViewById(R.id.trans_region).setVisibility(View.VISIBLE);

	}

	public class Progress_List extends AsyncTask<String, Void, String> {
		public Progress_List() {
		}

		@Override
		protected String doInBackground(String... params) {
			Common_Method mCommon_Method = new Common_Method(getApplicationContext());
			mString_success = mCommon_Method.json_method_call(
					mString_encrypt_final_probe_id, mString_encrypt_final_pin,
					mString_serviceid, mString_supp_name_id,
					mString_fooditemid, temperature, "1");

			return null;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			mDialog.cancel();
			mDialog.dismiss();
			if (mString_success.equals("success")) {
				
			    SimpleDateFormat sdf = new SimpleDateFormat("hh:mmaa dd/MM/yyyy");
		           String currentDateandTime = sdf.format(new Date());
		           Log.e("SDtring Date Andf Time", "_____"+currentDateandTime);
				mDatabaseHandler.Add_CHILL_ITEM(mString_supp_name_id,mString_fooditemid,currentDateandTime);

				show_alert("Data Successfully Sent ", true);

			} else {
				show_alert(mString_success, false);
			}

		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			mDialog.show();
			mDialog.setCancelable(false);
		}

		@Override
		protected void onProgressUpdate(Void... values) {

			super.onProgressUpdate(values);

		}

	}

	private void show_alert(String message, final boolean value) {
		// TODO Auto-generated method stub
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				chilling_begin_temp.this);

		if(value)
			alertDialog.setTitle("Success!");
			else
			alertDialog.setTitle("Error!");
		
		// Setting Dialog Message
		alertDialog.setMessage(message);

		// Setting Icon to Dialog
		alertDialog.setIcon(R.drawable.ic_launcher);

		// Setting Positive "Yes" Button
		alertDialog.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (value) {
							Intent intent = new Intent(getApplicationContext(),
									MainMenu.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(intent);
							//new backgroundHaptic().execute();
						} else {
							finish();
						}
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}
	

	private void show_navigate(String message) {
		// TODO Auto-generated method stub
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				chilling_begin_temp.this);

		// Setting Dialog Title
		alertDialog.setTitle("Save Offline!");

		// Setting Dialog Message
		alertDialog.setMessage(message);

		// Setting Icon to Dialog
		alertDialog.setIcon(R.drawable.ic_launcher);

		// Setting Positive "Yes" Button
		alertDialog.setPositiveButton("YES",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {


						Common_Method mCommon_Method = new Common_Method(getApplicationContext());
					           mCommon_Method.save_data_offline(
								mString_encrypt_final_probe_id, mString_encrypt_final_pin,
								mString_serviceid, mString_supp_name_id,
								mString_fooditemid, temperature, "1");
					           
					           SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa dd/MM/yyyy");
					           String currentDateandTime = sdf.format(new Date());
					           Log.e("SDtring Date Andf Time", "_____"+currentDateandTime);
					           mDatabaseHandler.Add_CHILL_ITEM(mString_supp_name_id, mString_fooditemid,currentDateandTime);
					           Intent intent = new Intent(getApplicationContext(),
										MainMenu.class);
								intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								startActivity(intent);
					}
				});
	

		// Showing Alert Message
		alertDialog.show();
	}
	
	
	
	private void show_probe(String message) {
		// TODO Auto-generated method stub
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				chilling_begin_temp.this);


			alertDialog.setTitle("Error!");

		// Setting Dialog Message
		alertDialog.setMessage(message);

		// Setting Icon to Dialog
		alertDialog.setIcon(R.drawable.ic_launcher);

		// Setting Positive "Yes" Button
		alertDialog.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}
	


}
