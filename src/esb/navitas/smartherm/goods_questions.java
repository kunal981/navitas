package esb.navitas.smartherm;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import esb.navitas.smartherm.Panel.OnPanelListener;

//import android.support.v7.app.ActionBarActivity;

/**
 * Created by jburger on 07/01/14.
 */
public class goods_questions extends Activity implements OnPanelListener,
		OnClickListener { /* ActionBarActivity */

	private Panel left_panel;
	Panel panel;

	TextView textView_home, textView_goods_in, textView_chilling,
			textView_reheating, textView_hotserv, textView_coldserv,
			textview_calibration, textView_cooking;
	TextView textview_active, textView_supplier_name;
	ImageView imageView_supplier_pic;
	String supp_name, mString_invoice, mString_supplier, mString_user_id,
			mString_supp_id;
	String mString_accept = "NO";
	String mString_company_policy = "NO";
	ArrayList<String> mArrayList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.goods_questions);
		globals global = (globals) getApplication();
		global.packaging = false;
		global.dateCode = false;
		global.questionPick = false;
		mArrayList = new ArrayList<String>();
		if (mArrayList != null) {
			mArrayList.clear();
		}
		mArrayList = getIntent().getStringArrayListExtra("ProductRange");

		//Log.e("mArrayList=[=============", "" + mArrayList);
		textView_supplier_name = (TextView) findViewById(R.id.textView_supp_name);
		imageView_supplier_pic = (ImageView) findViewById(R.id.imageView_supp_image);
		supp_name = getIntent().getStringExtra("supp_name");
		mString_supplier = getIntent().getStringExtra("supplier");

		mString_user_id = getIntent().getStringExtra("user_id");

		mString_supp_id = getIntent().getStringExtra("supp_name_id");
		textView_supplier_name.setText(supp_name);
		mString_invoice = getIntent().getStringExtra("InvoiceNumber");

		textview_calibration = (TextView) findViewById(R.id.textView_callibration);
		textView_chilling = (TextView) findViewById(R.id.textView_chilling);
		textView_coldserv = (TextView) findViewById(R.id.textView_cold);
		textView_goods_in = (TextView) findViewById(R.id.textView_goods_in);
		textView_home = (TextView) findViewById(R.id.textView_home);
		textView_hotserv = (TextView) findViewById(R.id.textView_hotservice);
		textView_reheating = (TextView) findViewById(R.id.textView_reheating);
		textView_cooking = (TextView) findViewById(R.id.textView_cooking);

		textview_calibration.setOnClickListener(this);
		textView_chilling.setOnClickListener(this);
		textView_coldserv.setOnClickListener(this);
		textView_goods_in.setOnClickListener(this);
		textView_home.setOnClickListener(this);
		textView_hotserv.setOnClickListener(this);
		textView_reheating.setOnClickListener(this);
		textView_cooking.setOnClickListener(this);

		left_panel = panel = (Panel) findViewById(R.id.leftPanel1);
		panel.setOnPanelListener(this);

		findViewById(R.id.smoothButton1).setOnClickListener(
				new OnClickListener() {
					public void onClick(View v) {
						left_panel.setOpen(!left_panel.isOpen(), true);
					}
				});

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 0 && resultCode == RESULT_OK) {
			setResult(RESULT_OK);
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.user_selected_menu, menu);
		globals global = (globals) getApplication();
		if (global.goodsUser != null) {
			MenuItem txtUser = menu.findItem(R.id.user_display);
			txtUser.setTitle(global.goodsUser);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		Intent intent;
		/*
		 * if (id == R.id.action_settings) { return true; }
		 */
		switch (id) {
		case R.id.user_display:
			intent = new Intent(getApplicationContext(), goods_user.class);
			startActivity(intent);
			break;
		case R.id.home_menu:
			intent = new Intent(getApplicationContext(), MainMenu.class);
			startActivity(intent);
			break;
		case R.id.goods_menu:
			intent = new Intent(getApplicationContext(), goods_user.class);
			startActivity(intent);
			break;
		case R.id.cook_menu:
			intent = new Intent(getApplicationContext(), cook_user.class);
			startActivity(intent);
			break;
		case R.id.chill_menu:
			intent = new Intent(getApplicationContext(), chilling_user.class);
			startActivity(intent);
			break;
		case R.id.reheat_menu:
			intent = new Intent(getApplicationContext(), reheat_user.class);
			startActivity(intent);
			break;
		case R.id.hot_serv_menu:
			intent = new Intent(getApplicationContext(), hot_service_user.class);
			startActivity(intent);
			break;
		case R.id.cold_serv_menu:
			intent = new Intent(getApplicationContext(),
					cold_service_user.class);
			startActivity(intent);
			break;
		case R.id.cal_menu:
			intent = new Intent(getApplicationContext(), calibration_user.class);
			startActivity(intent);
			break;
		default:
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/* Called when the user clicks the Packaging No button */
	public void clk_btn_goods_questions_packaging_no(View view) {
		// TODO Do something in response to Packaging No button click
		LinearLayout button = (LinearLayout) findViewById(R.id.btn_goods_questions_packaging_no);
		LinearLayout button2 = (LinearLayout) findViewById(R.id.btn_goods_questions_packaging_yes);
		TextView textView_no = (TextView) findViewById(R.id.pack_no);
		TextView textView_yes = (TextView) findViewById(R.id.pack_yes);
		globals global = (globals) getApplication();
		global.packaging = false;
		global.questionPick = true;
		mString_accept = "NO";
		// button.setBackgroundDrawable(getResources().getDrawable(R.drawable.apptheme_btn_default_normal_holo_light));
		button.setBackgroundResource(R.drawable.button_pressed);
		button2.setBackgroundResource(R.drawable.main_menus_background);

		textView_no.setBackgroundColor(Color.TRANSPARENT);
		textView_yes.setBackgroundResource(R.drawable.home_menus_background);
		textView_no.setTextColor(Color.WHITE);
		textView_yes.setTextColor(Color.BLACK);
	}

	/* Called when the user clicks the Packaging Yes button */
	public void clk_btn_goods_questions_packaging_yes(View view) {
		// TODO Do something in response to Packaging Yes button click
		LinearLayout button = (LinearLayout) findViewById(R.id.btn_goods_questions_packaging_no);
		LinearLayout button2 = (LinearLayout) findViewById(R.id.btn_goods_questions_packaging_yes);
		TextView textView_no = (TextView) findViewById(R.id.pack_no);
		TextView textView_yes = (TextView) findViewById(R.id.pack_yes);
		globals global = (globals) getApplication();
		global.packaging = true;
		global.questionPick = true;
		mString_accept = "Yes";
		button2.setBackgroundResource(R.drawable.button_pressed);
		button.setBackgroundResource(R.drawable.main_menus_background);
		textView_yes.setBackgroundColor(Color.TRANSPARENT);
		textView_no.setBackgroundResource(R.drawable.home_menus_background);
		textView_yes.setTextColor(Color.WHITE);
		textView_no.setTextColor(Color.BLACK);
	}

	/* Called when the user clicks the Accordance No button */
	public void clk_btn_goods_questions_accordance_no(View view) {
		// TODO Do something in response to Accordance No button click
		LinearLayout button = (LinearLayout) findViewById(R.id.btn_goods_questions_accordance_no);
		LinearLayout button2 = (LinearLayout) findViewById(R.id.btn_goods_questions_accordance_yes);

		TextView textView_no = (TextView) findViewById(R.id.acc_no);
		TextView textView_yes = (TextView) findViewById(R.id.acc_yes);
		globals global = (globals) getApplication();
		global.dateCode = false;
		mString_company_policy = "NO";
		global.questionPick = true;
		button.setBackgroundResource(R.drawable.button_pressed);
		button2.setBackgroundResource(R.drawable.main_menus_background);
		textView_no.setBackgroundColor(Color.TRANSPARENT);
		textView_yes.setBackgroundResource(R.drawable.home_menus_background);
		textView_no.setTextColor(Color.WHITE);
		textView_yes.setTextColor(Color.BLACK);
	}

	/* Called when the user clicks the Accordance Yes button */
	public void clk_btn_goods_questions_accordance_yes(View view) {
		// TODO Do something in response to Accordance Yes button click
		LinearLayout button = (LinearLayout) findViewById(R.id.btn_goods_questions_accordance_no);
		LinearLayout button2 = (LinearLayout) findViewById(R.id.btn_goods_questions_accordance_yes);
		TextView textView_no = (TextView) findViewById(R.id.acc_no);
		TextView textView_yes = (TextView) findViewById(R.id.acc_yes);
		globals global = (globals) getApplication();
		global.dateCode = true;
		global.questionPick = true;
		mString_company_policy = "YES";
		button2.setBackgroundResource(R.drawable.button_pressed);
		button.setBackgroundResource(R.drawable.main_menus_background);
		textView_yes.setBackgroundColor(Color.TRANSPARENT);
		textView_no.setBackgroundResource(R.drawable.home_menus_background);
		textView_yes.setTextColor(Color.WHITE);
		textView_no.setTextColor(Color.BLACK);
	}

	/* Called when the user clicks the Submit button */
	public void clk_btn_goods_questions_submit(View view) {
		// TODO Do something in response to Submit button click
		globals global = (globals) getApplication();
		//Log.i("Packaging = ", String.valueOf(global.packaging));
		//Log.i("Date Code OK = ", String.valueOf(global.dateCode));
		if (global.questionPick) {
			Intent intent = new Intent(getApplicationContext(),
					goods_temp.class);
			intent.putExtra("supp_name", supp_name);
			intent.putExtra("InvoiceNumber", mString_invoice);
			intent.putStringArrayListExtra("ProductRange", mArrayList);
			intent.putExtra("accept", mString_accept);
			intent.putExtra("company", mString_company_policy);
			intent.putExtra("supplier", mString_supplier);

			intent.putExtra("user_id", mString_user_id);
			intent.putExtra("supp_name_id", mString_supp_id);

			startActivityForResult(intent, 0);
		} else {
			Toast.makeText(getApplicationContext(),
					getString(R.string.select_questions), Toast.LENGTH_SHORT)
					.show();
		}
	}

	public void clk_btn_back_view(View view) {
		// TODO Do something in response to Submit button click and return to
		// main menu
		// globals global = (globals) getApplication();
		finish();
	}

	public void onPanelClosed(Panel panel) {
		String panelName = getResources().getResourceEntryName(panel.getId());
		//Log.d("Test", "Panel [" + panelName + "] closed");
		findViewById(R.id.trans_region).setVisibility(View.GONE);

	}

	public void onPanelOpened(Panel panel) {
		String panelName = getResources().getResourceEntryName(panel.getId());
		//Log.d("Test", "Panel [" + panelName + "] opened");
		findViewById(R.id.trans_region).setVisibility(View.VISIBLE);

	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent;
		switch (v.getId()) {

		case R.id.textView_home:
			left_panel.setOpen(!left_panel.isOpen(), true);
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_home.setBackgroundResource(R.drawable.button_background);
			intent = new Intent(this, MainMenu.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);

			finish();
			break;
		case R.id.textView_goods_in:

			left_panel.setOpen(!left_panel.isOpen(), true);
			break;

		case R.id.textView_cooking:
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_cooking
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, cook_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		case R.id.textView_chilling:

			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_chilling
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, chilling_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();

			break;

		case R.id.textView_reheating:
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_reheating
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, reheat_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		case R.id.textView_hotservice:
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_hotserv
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, hot_service_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		case R.id.textView_cold:
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_coldserv
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, cold_service_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		case R.id.textView_callibration:
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textview_calibration
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, calibration_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			setResult(RESULT_OK);
			startActivity(intent);

			finish();
			break;

		default:
			break;
		}
	}

}
