package esb.navitas.smartherm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import JsonParser.JSONARRAY;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import esb.navitas.smartherm.Activit_Splash_class.Progress_List;
import esb.navitas.smartherm.Panel.OnPanelListener;

//import android.support.v7.app.ActionBarActivity;

/**
 * Created by jburger on 06/01/14.
 */

public class goods_user extends Activity implements OnPanelListener,
		OnClickListener { /* ActionBarActivity */
	TextView probeView;
	globals global;
	private CustomFastScrollView fastScrollView;
	private final static int PROBE_UPDATE = 3;
	ListView listView;
	JSONArray jsonArray;
	Cursor mCursor;
	DatabaseHandler mDatabaseHandler;
	ProgressDialog pDialog;
	Progress_List mProgress_List;
	ArrayList<String> mArrayList;
	ArrayList<String> mArrayList_ids;
	ArrayList<String> lis;
	// private ProbeUpdateTask probeUpdateTask;

	private boolean running = false;
	private Panel left_panel;
	Panel panel;

	TextView textView_home, textView_goods_in, textView_chilling,
			textView_reheating, textView_hotserv, textView_coldserv,
			textview_calibration, textView_cooking;
	TextView textview_active;

	// Handler to take care of UI actions called from other threads
	private Handler myHandler = new Handler() {

		/*
		 * @see android.os.Handler#handleMessage(android.os.Message)
		 */
		public void handleMessage(Message msg) {
			switch (msg.what) {
			// Check which kind of message was received to perform required
			// actions.
			case (PROBE_UPDATE):
				probeAPI.check_probe(probeView);
				global.probeID = global.newProbeID;
				break;
			}
		}
	};

	/**
	 * Initialises background asynchronous task that take care of listening for
	 * interrupt events on board button and perform required actions on LED GPIO
	 * and graphics.
	 */
	private void initializeTask() {
		// Set global running variable to true.
		// running = true;
		// Declare task
		// probeUpdateTask = new ProbeUpdateTask();
		// Start task.
		// probeUpdateTask.execute();
	}

	/**
	 * Stop asynchronous background task that were taking care of checking GPIO
	 * button interrupt events.
	 */
	/*
	 * private void stopTask() { // Set global running variable to false.
	 * running = false; // Give time to propagate stop request. try {
	 * Thread.sleep(1000); } catch (InterruptedException e) { } // Cancel tasks.
	 * probeUpdateTask.cancel(true); probeUpdateTask = null; }
	 */
	@Override
	protected void onDestroy() {
		// Stop background task
		// stopTask();
		super.onDestroy();
	}

	/**
	 * Background asynchronous task that takes care of updating the temperature
	 * readout
	 * 
	 */
	// private class ProbeUpdateTask extends AsyncTask<Void, Void, Void> {
	//
	// /*
	// * @see android.os.AsyncTask#doInBackground(Params[])
	// */
	// protected Void doInBackground(Void... params) {
	// while (running) {
	// try {
	// global.newProbeID = probeAPI.getProbeID();
	// if (global.probeID != global.newProbeID) {
	// myHandler.sendEmptyMessage(PROBE_UPDATE);
	// }
	// Thread.sleep(1000);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// return (null);
	// }
	// }

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		global = (globals) getApplication();// globals
		String[] values = getResources().getStringArray(R.array.user_names);
		setContentView(R.layout.goods_user);
		mDatabaseHandler = new DatabaseHandler(this);

		pDialog = new ProgressDialog(this);
		mArrayList = new ArrayList<String>();
		lis = new ArrayList<String>();
		mArrayList_ids = new ArrayList<String>();
		if (mArrayList != null) {
			mArrayList.clear();
			mArrayList_ids.clear();
			// lis.clear();
		}
		// probeView = (TextView) findViewById(R.id.txt_goods_user_probe);
		// probeAPI.check_probe(probeView);
		global.probeID = null;

		textview_calibration = (TextView) findViewById(R.id.textView_callibration);
		textView_chilling = (TextView) findViewById(R.id.textView_chilling);
		textView_coldserv = (TextView) findViewById(R.id.textView_cold);
		textView_goods_in = (TextView) findViewById(R.id.textView_goods_in);
		textView_home = (TextView) findViewById(R.id.textView_home);
		textView_hotserv = (TextView) findViewById(R.id.textView_hotservice);
		textView_reheating = (TextView) findViewById(R.id.textView_reheating);
		textView_cooking = (TextView) findViewById(R.id.textView_cooking);

		textview_calibration.setOnClickListener(this);
		textView_chilling.setOnClickListener(this);
		textView_coldserv.setOnClickListener(this);
		textView_goods_in.setOnClickListener(this);
		textView_home.setOnClickListener(this);
		textView_hotserv.setOnClickListener(this);
		textView_reheating.setOnClickListener(this);
		textView_cooking.setOnClickListener(this);

		textview_active = textView_goods_in;

		// Initialise application background task to check for interrupts on
		// button GPIO.
		// initializeTask();
		listView = (ListView) findViewById(android.R.id.list);
		fastScrollView = (CustomFastScrollView) findViewById(R.id.fast_scroll_view);
		// final ArrayList<String> list = new ArrayList<String>();
		// for (int i = 0; i < values.length; ++i) {
		// list.add(values[i]);
		// }
		// mProgress_List=new Progress_List();

		// mProgress_List.execute();

		try {
			mCursor = mDatabaseHandler.fetch_path();
			//Log.i("value in cursor at starting", mCursor.getCount() + "");
		} catch (Exception e) {

			e.printStackTrace();
		}

		// mCursor.moveToFirst();
		if (mCursor.getCount() != 0) {
			do {
				mArrayList_ids.add(mCursor.getString(0).trim());
				mArrayList.add(mCursor.getString(1).trim());

			} while (mCursor.moveToNext());

		}
		mCursor.close();
		mDatabaseHandler.close();
		ArrayAdapter<String> adapter = new CountryAdapter(
				getApplicationContext(), android.R.layout.simple_list_item_2,
				mArrayList);
		listView.setAdapter(adapter);
		listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		// final StableArrayAdapter adapter = new StableArrayAdapter(this,
		// android.R.layout.simple_list_item_1, list);
		// listView.setAdapter(adapter);
		if (global.goodsUserPosition != 0) {
			listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			listView.setItemChecked(global.goodsUserPosition, true);
			listView.setSelection(global.goodsUserPosition - 2);
		}
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			// @Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// globals global = (globals) getApplication();

				String[] values = getResources().getStringArray(
						R.array.user_names);
				global.goodsUser = values[position];
				global.goodsUserPosition = position;
				//Log.i("User Position", String.valueOf(position));
				Intent intent = new Intent(getApplicationContext(),
						goods_suppliers.class);
				intent.putExtra("supp_name", mArrayList.get(position));
				intent.putExtra("user_id", mArrayList_ids.get(position));

				startActivityForResult(intent, 0);
			}
		});

		left_panel = panel = (Panel) findViewById(R.id.leftPanel1);
		panel.setOnPanelListener(this);

		findViewById(R.id.smoothButton1).setOnClickListener(
				new OnClickListener() {
					public void onClick(View v) {
						left_panel.setOpen(!left_panel.isOpen(), true);
					}
				});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

		listView.setVisibility(View.GONE);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		listView.setVisibility(View.VISIBLE);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		Intent intent;
		/*
		 * if (id == R.id.action_settings) { return true; }
		 */
		switch (id) {
		case R.id.home_menu:
			intent = new Intent(getApplicationContext(), MainMenu.class);
			startActivity(intent);
			break;
		case R.id.goods_menu:
			intent = new Intent(getApplicationContext(), goods_user.class);
			startActivity(intent);
			break;
		case R.id.cook_menu:
			intent = new Intent(getApplicationContext(), cook_user.class);
			startActivity(intent);
			break;
		case R.id.chill_menu:
			intent = new Intent(getApplicationContext(), chilling_user.class);
			startActivity(intent);
			break;
		case R.id.reheat_menu:
			intent = new Intent(getApplicationContext(), reheat_user.class);
			startActivity(intent);
			break;
		case R.id.hot_serv_menu:
			intent = new Intent(getApplicationContext(), hot_service_user.class);
			startActivity(intent);
			break;
		case R.id.cold_serv_menu:
			intent = new Intent(getApplicationContext(),
					cold_service_user.class);
			startActivity(intent);
			break;
		case R.id.cal_menu:
			intent = new Intent(getApplicationContext(), calibration_user.class);
			startActivity(intent);
			break;
		default:
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private class StableArrayAdapter extends ArrayAdapter<String> {

		HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

		public StableArrayAdapter(Context context, int textViewResourceId,
				List<String> objects) {
			super(context, textViewResourceId, objects);

			for (int i = 0; i < objects.size(); ++i) {
				mIdMap.put(objects.get(i), i);

			}
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			return super.getView(position, convertView, parent);

		}

		@Override
		public long getItemId(int position) {
			String item = getItem(position);
			return mIdMap.get(item);
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 0 && resultCode == RESULT_OK) {
			finish();
		}
	}

	public void onPanelClosed(Panel panel) {
		String panelName = getResources().getResourceEntryName(panel.getId());
		//Log.d("Test", "Panel [" + panelName + "] closed");
		findViewById(R.id.trans_region).setVisibility(View.GONE);

		listView.setEnabled(true);

	}

	public void onPanelOpened(Panel panel) {
		String panelName = getResources().getResourceEntryName(panel.getId());
		//Log.d("Test", "Panel [" + panelName + "] opened");
		findViewById(R.id.trans_region).setVisibility(View.VISIBLE);

		listView.setEnabled(false);
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent;
		switch (v.getId()) {

		case R.id.textView_home:
			left_panel.setOpen(!left_panel.isOpen(), true);
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_home.setBackgroundResource(R.drawable.button_background);
			intent = new Intent(this, MainMenu.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);

			finish();
			break;
		case R.id.textView_goods_in:

			left_panel.setOpen(!left_panel.isOpen(), true);
			break;

		case R.id.textView_cooking:
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_cooking
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, cook_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
			break;

		case R.id.textView_chilling:

			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_chilling
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, chilling_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();

			break;

		case R.id.textView_reheating:
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_reheating
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, reheat_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
			break;

		case R.id.textView_hotservice:
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_hotserv
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, hot_service_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
			break;

		case R.id.textView_cold:
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textView_coldserv
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, cold_service_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
			break;

		case R.id.textView_callibration:
			textView_goods_in.setBackgroundResource(R.drawable.list_background);
			textView_goods_in.setTextColor(Color.WHITE);
			textview_calibration
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, calibration_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			startActivity(intent);

			finish();
			break;

		default:
			break;
		}
	}

	public class Progress_List extends AsyncTask<String, Void, String> {
		public Progress_List() {
		}

		@Override
		protected String doInBackground(String... params) {

			String URL = "http://brstdev.com/caczcall/calendar/navitasapp/user.php";

			JSONARRAY jParser = new JSONARRAY();
			// getting JSON string from URL

			String response = jParser.getJSONFromUrl(URL);
			//Log.e("response---------", "" + response);

			try {
				JSONObject mJsonObject = new JSONObject(response);


				jsonArray = mJsonObject.getJSONArray("result");
				//Log.e("jsonArray---------", "" + jsonArray.length());

			} catch (JSONException e) {

				e.printStackTrace();
			}
			try {
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jsonObject = jsonArray.getJSONObject(i);

					String mString_username = jsonObject.getString("username");

					mArrayList.add(mString_username);

				}

			} catch (Exception e) {

			}

			return null;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.cancel();
			pDialog.dismiss();
			ArrayAdapter<String> adapter = new CountryAdapter(
					getApplicationContext(),
					android.R.layout.simple_list_item_2, mArrayList);
			listView.setAdapter(adapter);
			listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		}

		@Override
		protected void onPreExecute() {

			pDialog.show();
			pDialog.setCancelable(false);

			super.onPreExecute();

		}

		@Override
		protected void onProgressUpdate(Void... values) {

			super.onProgressUpdate(values);

		}

	}

}
