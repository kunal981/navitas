package esb.navitas.smartherm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import JsonParser.JSONARRAY;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import esb.navitas.smartherm.Panel.OnPanelListener;
//import android.support.v7.app.ActionBarActivity;
import esb.navitas.smartherm.chilling_finish_food_group.Progress_List;

/**
 * Created by jburger on 07/01/14.
 */
/* public class chilling_finish_food_item extends Activity { /*ActionBarActivity */
public class chilling_finish_food_item extends Activity implements
		OnPanelListener, OnClickListener {
	ListView listView;

	JSONArray jsonArray;
	ProgressDialog pDialog;
	Progress_List mProgress_List;
	ArrayList<String> mArrayList;
	ArrayList<String> mArrayList_foodgroupid;
	ArrayList<String> mArrayList_fooditemid;
	ArrayList<String> mArrayList_begin_chilling;

	private Panel left_panel;
	Panel panel;
	Cursor mCursor;
	DatabaseHandler mDatabaseHandler;

	TextView textView_home, textView_goods_in, textView_chilling,
			textView_reheating, textView_hotserv, textView_coldserv,
			textview_calibration, textView_cooking;

	TextView textview_active;
	TextView textView_supplier_name;
	ImageView imageView_supplier_pic;
	String supp_name, mString_foodname, mString_id, mString_supp_name_id;
	private CustomFastScrollView fastScrollView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chilling_finish_food_item);
		mDatabaseHandler = new DatabaseHandler(this);
		textView_supplier_name = (TextView) findViewById(R.id.textView_supp_name);
		imageView_supplier_pic = (ImageView) findViewById(R.id.imageView_supp_image);
		supp_name = getIntent().getStringExtra("supp_name");
		mString_supp_name_id = getIntent().getStringExtra("supp_name_id");
		mString_foodname = getIntent().getStringExtra("food_name");
		mString_id = getIntent().getStringExtra("food_group_id");
		textView_supplier_name.setText(supp_name);
		globals global = (globals) getApplication();
		listView = (ListView) findViewById(android.R.id.list);
		fastScrollView = (CustomFastScrollView) findViewById(R.id.fast_scroll_view);
		String[] values;
		switch (global.chillFinishFoodGroup) {
		case 0:
			values = getResources().getStringArray(R.array.fruits);
			break;
		case 1:
			values = getResources().getStringArray(R.array.meat);
			break;
		case 2:
			values = getResources().getStringArray(R.array.ready_meals);
			break;
		case 3:
			values = getResources().getStringArray(R.array.vegetables);
			break;
		default:
			values = getResources().getStringArray(R.array.suppliers);
		}
		textview_calibration = (TextView) findViewById(R.id.textView_callibration);
		textView_chilling = (TextView) findViewById(R.id.textView_chilling);
		textView_coldserv = (TextView) findViewById(R.id.textView_cold);
		textView_goods_in = (TextView) findViewById(R.id.textView_goods_in);
		textView_home = (TextView) findViewById(R.id.textView_home);
		textView_hotserv = (TextView) findViewById(R.id.textView_hotservice);
		textView_reheating = (TextView) findViewById(R.id.textView_reheating);
		textView_cooking = (TextView) findViewById(R.id.textView_cooking);

		textview_calibration.setOnClickListener(this);
		textView_chilling.setOnClickListener(this);
		textView_coldserv.setOnClickListener(this);
		textView_goods_in.setOnClickListener(this);
		textView_home.setOnClickListener(this);
		textView_hotserv.setOnClickListener(this);
		textView_reheating.setOnClickListener(this);
		textView_cooking.setOnClickListener(this);
		left_panel = panel = (Panel) findViewById(R.id.leftPanel1);
		panel.setOnPanelListener(this);

		findViewById(R.id.smoothButton1).setOnClickListener(
				new OnClickListener() {
					public void onClick(View v) {
						left_panel.setOpen(!left_panel.isOpen(), true);
					}
				});

		pDialog = new ProgressDialog(this);
		mArrayList = new ArrayList<String>();
		mArrayList_foodgroupid = new ArrayList<String>();
		mArrayList_fooditemid = new ArrayList<String>();
		mArrayList_begin_chilling=new ArrayList<String>();
		if (mArrayList != null) {
			mArrayList.clear();
			mArrayList_foodgroupid.clear();
			mArrayList_fooditemid.clear();
			mArrayList_begin_chilling.clear();
		}

		// mProgress_List=new Progress_List();

		// mProgress_List.execute();
		try {
			//Log.d("mString_id==", "" + mString_id);
			mCursor = mDatabaseHandler.fetch_menu_item_list(mString_id);
			//Log.i("value in cursor at starting==============",
				//	mCursor.getCount() + "");
		} catch (Exception e) {

			e.printStackTrace();
		}

		// mCursor.moveToFirst();
		if (mCursor.getCount() != 0) {
			do {

				Cursor chill_mCursor = mDatabaseHandler.fetch_chill_list(mCursor.getString(2).trim(),mString_supp_name_id);
		
				if(chill_mCursor.getCount()==0)
				{
					continue;
				}
				
		
				mArrayList_foodgroupid.add(mCursor.getString(0).trim());
				mArrayList.add(mCursor.getString(1).trim());
				mArrayList_fooditemid.add(mCursor.getString(2).trim());
				mArrayList_begin_chilling.add(chill_mCursor.getString(2).trim());
				
			} while (mCursor.moveToNext());

		}
		mCursor.close();
		mDatabaseHandler.close();
		ArrayAdapter<String> adapter = new CountryAdapter(
				getApplicationContext(), android.R.layout.simple_list_item_2,
				mArrayList);
		listView.setAdapter(adapter);
		listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			// @Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				globals global = (globals) getApplication();
				global.chillFinishFoodItem = position;
				Intent intent = new Intent(getApplicationContext(),
						chilling_finish_temp.class);
				intent.putExtra("supp_name", supp_name);
				intent.putExtra("supp_name_id", mString_supp_name_id);
				intent.putExtra("foodgroupid",
						mArrayList_foodgroupid.get(position));
				intent.putExtra("fooditemid",
						mArrayList_fooditemid.get(position));
				
				
				intent.putExtra("food_item_name",
						mArrayList.get(position));
				intent.putExtra("foood_begin_time",
						mArrayList_begin_chilling.get(position));
				
				
				
				
				startActivityForResult(intent, 0);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.user_selected_menu, menu);
		globals global = (globals) getApplication();
		if (global.chillUser != null) {
			MenuItem txtUser = menu.findItem(R.id.user_display);
			txtUser.setTitle(global.chillUser);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		Intent intent;
		/*
		 * if (id == R.id.action_settings) { return true; }
		 */
		switch (id) {
		case R.id.user_display:
			intent = new Intent(getApplicationContext(), chilling_user.class);
			startActivity(intent);
			break;
		case R.id.home_menu:
			intent = new Intent(getApplicationContext(), MainMenu.class);
			startActivity(intent);
			break;
		case R.id.goods_menu:
			intent = new Intent(getApplicationContext(), goods_user.class);
			startActivity(intent);
			break;
		case R.id.cook_menu:
			intent = new Intent(getApplicationContext(), cook_user.class);
			startActivity(intent);
			break;
		case R.id.chill_menu:
			intent = new Intent(getApplicationContext(), chilling_user.class);
			startActivity(intent);
			break;
		case R.id.reheat_menu:
			intent = new Intent(getApplicationContext(), reheat_user.class);
			startActivity(intent);
			break;
		case R.id.hot_serv_menu:
			intent = new Intent(getApplicationContext(), hot_service_user.class);
			startActivity(intent);
			break;
		case R.id.cold_serv_menu:
			intent = new Intent(getApplicationContext(),
					cold_service_user.class);
			startActivity(intent);
			break;
		case R.id.cal_menu:
			intent = new Intent(getApplicationContext(), calibration_user.class);
			startActivity(intent);
			break;
		default:
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private class StableArrayAdapter extends ArrayAdapter<String> {

		HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

		public StableArrayAdapter(Context context, int textViewResourceId,
				List<String> objects) {
			super(context, textViewResourceId, objects);
			for (int i = 0; i < objects.size(); ++i) {
				mIdMap.put(objects.get(i), i);
			}
		}

		@Override
		public long getItemId(int position) {
			String item = getItem(position);
			return mIdMap.get(item);
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}
	}

	/**
	 * Catch Activity Result
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK && requestCode == 0) {
			setResult(RESULT_OK);
			finish();
		}

	}

	/**
	 * 
	 * Click Event On SideBar Menus.....
	 */

	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent;
		switch (v.getId()) {

		case R.id.textView_home:
			left_panel.setOpen(!left_panel.isOpen(), true);
			textView_chilling.setBackgroundResource(R.drawable.list_background);
			textView_chilling.setTextColor(Color.WHITE);
			textView_home.setBackgroundResource(R.drawable.button_background);
			intent = new Intent(this, MainMenu.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;
		case R.id.textView_goods_in:

			textView_chilling.setBackgroundResource(R.drawable.list_background);
			textView_chilling.setTextColor(Color.WHITE);
			textView_goods_in
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, goods_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		case R.id.textView_cooking:

			textView_chilling.setBackgroundResource(R.drawable.list_background);
			textView_chilling.setTextColor(Color.WHITE);
			textView_cooking
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, cook_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();

			break;

		case R.id.textView_chilling:

			left_panel.setOpen(!left_panel.isOpen(), true);

			break;

		case R.id.textView_reheating:
			textView_chilling.setBackgroundResource(R.drawable.list_background);
			textView_chilling.setTextColor(Color.WHITE);
			textView_reheating
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, reheat_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		case R.id.textView_hotservice:
			textView_chilling.setBackgroundResource(R.drawable.list_background);
			textView_chilling.setTextColor(Color.WHITE);
			textView_hotserv
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, hot_service_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		case R.id.textView_cold:
			textView_chilling.setBackgroundResource(R.drawable.list_background);
			textView_chilling.setTextColor(Color.WHITE);
			textView_coldserv
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, cold_service_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		case R.id.textView_callibration:
			textView_chilling.setBackgroundResource(R.drawable.list_background);
			textView_chilling.setTextColor(Color.WHITE);
			textview_calibration
					.setBackgroundResource(R.drawable.button_background);
			left_panel.setOpen(!left_panel.isOpen(), true);
			intent = new Intent(this, calibration_user.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
			break;

		default:
			break;
		}
	}

	/**
	 * 
	 * Panel Handling Intefrace
	 */

	public void onPanelClosed(Panel panel) {
		String panelName = getResources().getResourceEntryName(panel.getId());
		//Log.d("Test", "Panel [" + panelName + "] closed");
		findViewById(R.id.trans_region).setVisibility(View.GONE);

		listView.setEnabled(true);

	}

	public void onPanelOpened(Panel panel) {
		String panelName = getResources().getResourceEntryName(panel.getId());
		//Log.d("Test", "Panel [" + panelName + "] opened");
		findViewById(R.id.trans_region).setVisibility(View.VISIBLE);

		listView.setEnabled(false);
	}

	public class Progress_List extends AsyncTask<String, Void, String> {
		public Progress_List() {
		}

		@Override
		protected String doInBackground(String... params) {

			String URL = "http://brstdev.com/caczcall/calendar/navitasapp/menu.php?foodgroup="
					+ mString_foodname;

			JSONARRAY jParser = new JSONARRAY();
			// getting JSON string from URL

			String response = jParser.getJSONFromUrl(URL);
			//Log.e("response---------", "" + response);

			try {
				JSONObject mJsonObject = new JSONObject(response);

				//Log.i("mJsonObject string---------",
				//		"" + mJsonObject.toString());

				jsonArray = mJsonObject.getJSONArray("result");
				//Log.e("jsonArray---------", "" + jsonArray.length());

			} catch (JSONException e) {

				e.printStackTrace();
			}
			try {
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jsonObject = jsonArray.getJSONObject(i);

					String mString_username = jsonObject.getString("fooditem");

					mArrayList.add(mString_username);

				}

			} catch (Exception e) {

			}

			return null;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.cancel();
			pDialog.dismiss();
			ArrayAdapter<String> adapter = new CountryAdapter(
					getApplicationContext(),
					android.R.layout.simple_list_item_2, mArrayList);
			listView.setAdapter(adapter);
			listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		}

		@Override
		protected void onPreExecute() {

			pDialog.show();
			pDialog.setCancelable(false);

			super.onPreExecute();

		}

		@Override
		protected void onProgressUpdate(Void... values) {

			super.onProgressUpdate(values);

		}

	}

}
