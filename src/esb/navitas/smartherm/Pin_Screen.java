package esb.navitas.smartherm;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import JsonParser.JSONARRAY;
import JsonParser.JSONParserPost;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

@SuppressLint("SimpleDateFormat")
public class Pin_Screen extends Activity {
	Button mButton_pin;
	EditText mEditText;
	ProgressDialog pDialog;
	JSONArray jsonArray, json_Array;
	DatabaseHandler mDatabaseHandler;
	// Progress_List mProgress_List;
	Cursor mCursor;
	// Progress_ mProgress_;
	// Progress_Supplier mProgress_Supplier;
	String mString_deciceid;
	MCrypt mCrypt;
	Again_Crypt mAgain_Crypt;
	String androidId, mString;

	JSONObject json;
	JSONObject jsonObject;
	String encrypted_pin, encrypted_probeid, mString_device;
	String mString_status, mString_probeid, mString_again, mString_status_menu,
			mString_status_user, mString_status_supplier;

	SharedPreferences mSharedPreferences, mPreferences;
	public static String My_Shared_PIN = "pin_file";

	public static String My_Shared_All_Data = "All_Data";

	int private_moade = 0;

	int private_mode = 0;
	SharedPreferences.Editor editor, mEditor;
	Bundle bundle;
	ConnectionDetector cd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pinscreen);

		pDialog = new ProgressDialog(this);
		bundle = getIntent().getExtras();
		encrypted_probeid = bundle.getString("ProbeId");
		mString_device = bundle.getString("DEVICEID");
		Log.i("Main Activity encrypted_probeid==", "" + encrypted_probeid);
		Log.i("Main Activity encrypted_deviceid==", "" + mString_device);
		cd = new ConnectionDetector(getApplicationContext());
		mCrypt = new MCrypt();
		mDatabaseHandler = new DatabaseHandler(this);
		mButton_pin = (Button) findViewById(R.id.pin_button);
		if (encrypted_probeid != null) {

			try {
				mString_again = new String(mCrypt.decrypt(encrypted_probeid));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {

				
				// SimpleDateFormat sdfDateTime = new SimpleDateFormat(
				// "yyyy-MM-dd HH:00:00",Locale.UK);
				// String newtime = sdfDateTime.format(new Date(System
				// .currentTimeMillis()));
				// String mString_timetime = convertToTimestamp(newtime);
				// //Log.i("mString_timetime static=====", "" + mString_timetime);
				// try {
				//
				// String digestInput = mString_again;
				//
				// Log.i("digestInput", "" + digestInput);
				//
				// String mString_key_pass = md5(digestInput);
				//
				// //Log.i("mString_key_pass=", "" + mString_key_pass);
				//
				// Bussiness_Id_Class.mString_probeid = mString_key_pass;
				//
				// Log.i("Main Activity Bussiness_Id_Class.mString_probeid===",
				// "" + Bussiness_Id_Class.mString_probeid);
				//
				// // digestString now contains the md5 hashed password
				// } catch (Exception e) {
				// // do some type of logging here
				// }

				// mString_again=Bussiness_Id_Class.mString_probeid;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {

			Bussiness_Id_Class.mString_probeid = bundle.getString("ProbeId");
			try {
				mString_again = new String(mCrypt.decrypt(encrypted_probeid));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			try {
				// Bussiness_Id_Class.mString_probeid = data(mString_again);
				

				// SimpleDateFormat sdfDateTime = new SimpleDateFormat(
				// "yyyy-MM-dd HH:00:00",Locale.UK);
				// String newtime = sdfDateTime.format(new Date(System
				// .currentTimeMillis()));
				// String mString_timetime = convertToTimestamp(newtime);
				// //Log.i("mString_timetime=====", "" + mString_timetime);
				// String digestInput = mString_again;
				//
				// String mString_key_pass = md5(digestInput);
				// //Log.i("mString_key_pass=", "" + mString_key_pass);
				// Bussiness_Id_Class.mString_probeid = mString_key_pass;

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		mSharedPreferences = this.getSharedPreferences(My_Shared_PIN,
				private_moade);
		mPreferences = this.getSharedPreferences(My_Shared_All_Data,
				private_mode);
		editor = mSharedPreferences.edit();
		mEditor = mPreferences.edit();

		mEditText = (EditText) findViewById(R.id.edit_pin);

		mButton_pin.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				System.gc();
				if (mEditText.getText().toString().length() != 0) {
					Boolean isInternetPresent = cd.isConnectingToInternet(); // true
																				// or
					// false

					if (isInternetPresent == false) {
						show_alert("No Internet Connection", false);
						return;
					}
					new Progress_List_Post().execute();

				} else {
					Toast.makeText(getApplicationContext(), "Please enter Pin",
							Toast.LENGTH_LONG).show();
				}

			}
		});

	}

	public String data(String s) throws UnsupportedEncodingException {
		s = "hashme";
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy.MM.dd HH:00:00");
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+1:00")); // Londyn?

		String fullDateUK = dateFormat.format(new Date());

		// String result = s.concat(fullDateUK);
		//Log.i("result====", "" + s);

		byte[] hash;
		Log.d("ssss==", "" + s);
		try {
			hash = MessageDigest.getInstance("MD5").digest(s.getBytes("UTF-8"));
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("Huh, MD5 should be supported?", e);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("Huh, UTF-8 should be supported?", e);
		}

		StringBuilder hex = new StringBuilder(hash.length * 2);

		for (byte b : hash) {
			int i = (b & 0xFF);
			if (i < 0x10)
				hex.append('0');
			hex.append(Integer.toHexString(i));
		}
		//Log.i("hex.toString()====", "" + hex.toString());
		return hex.toString();

	}

	public static String md5(String string) {
		byte[] hash;

		try {
			hash = MessageDigest.getInstance("MD5").digest(
					string.getBytes("UTF-8"));
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("Huh, MD5 should be supported?", e);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("Huh, UTF-8 should be supported?", e);
		}

		StringBuilder hex = new StringBuilder(hash.length * 2);

		for (byte b : hash) {
			int i = (b & 0xFF);
			if (i < 0x10)
				hex.append('0');
			hex.append(Integer.toHexString(i));
		}
		//Log.i("another method=========", "" + hex.toString());
		return hex.toString();
	}

	public static String convertToTimestamp(String stringToFormat)
			throws ParseException {

		stringToFormat = "2014-06-25 09:00:00";
		Log.i("stringToFormat==", "" + stringToFormat);

		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:00:00", Locale.UK);
		Log.i("dateFormat==", "" + dateFormat);
		Date date = dateFormat.parse(stringToFormat);

		Log.i("date==", "" + date);
		Long mLong = date.getTime() / 1000;
		//Log.i("mLong=====", "" + mLong);
		// Timestamp mTimestamp=new Timestamp(mLong);
		return mLong.toString();

	}

	// post message=======

	public class Progress_List_Post extends AsyncTask<String, Void, String> {
		public Progress_List_Post() {
		}

		@Override
		protected String doInBackground(String... params) {

			// Long tsLong = System.currentTimeMillis() / 1000;
			// String ts = tsLong.toString();
			// mAgain_Crypt = new Again_Crypt();
			// http://test.gardnerjames.eu.com/api/navitas-app/auth
			// http://test.gardnerjames.eu.com/api/navitas-app/pin-confirm

			String URL = "http://test.gardnerjames.eu.com/api/navitas-app/pin-confirm";

			JSONParserPost mJsonParserPost = new JSONParserPost();

			try {
				encrypted_pin = MCrypt.bytesToHex(mCrypt.encrypt(mEditText
						.getText().toString()));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			//Log.i("pin-confirm encrypted=====", "" + encrypted_pin);
			//Log.i("pin-confirm encrypted_probeid=====", "" + encrypted_probeid);

			HttpEntity se = null;
			try {

				json = new JSONObject();
				json.put("0", encrypted_probeid);
				json.put("1", encrypted_pin);

				se = new StringEntity(json.toString());

				//Log.i("parent====", "" + json);
			} catch (Exception e) {

				e.printStackTrace();
			}

			try {

			} catch (Exception e) {

				e.printStackTrace();
			}

			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(URL, se);
			//Log.i("jsonobject=====", "" + jsonobject);

			try {
				mString_status = jsonobject.getString("status");
				//Log.i("mString_status===", "" + mString_status);
		
				String mString_message = jsonobject.getString("message");
				//Log.i("mString_message===", "" + mString_message);
			}catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (NullPointerException e) {
				// TODO Auto-generated catch block
				mString_status="Error!";
			}
			if (mString_status.equals("success")) {
				try {
					// mString_probeid = jsonobject.getString("id");
					editor.putString("PIN", mEditText.getText().toString());
					editor.commit();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			return null;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.cancel();
			pDialog.dismiss();

			if (mString_status.equals("success")) {

				editor.putString("PIN", mEditText.getText().toString());
				editor.commit();
				mEditor.putString("PROBEID_ENCRYPT", encrypted_probeid);
				mEditor.putString("PIN_ENCRYPT", encrypted_pin);
				mEditor.commit();
				Intent intent_home_screen = new Intent(Pin_Screen.this,
						Refresh_Conform_Probe_Enable.class);

				startActivity(intent_home_screen);
				finish();

			}else if(mString_status.equals("Error!"))
			{
				mEditText.setText("");
				Toast.makeText(getApplicationContext(), "Unable To Connect To Server!\nPlease check your wifi Connection",
						Toast.LENGTH_LONG).show();
			}
			else {
				mEditText.setText("");
				Toast.makeText(getApplicationContext(), "Wrong Pin Inserted",
						Toast.LENGTH_LONG).show();
			}
		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			pDialog.show();
		}

		@Override
		protected void onProgressUpdate(Void... values) {

			super.onProgressUpdate(values);

		}

	}

	// // post end message=====
	//
	// public class Progress_List extends AsyncTask<String, Void, String> {
	// public Progress_List() {
	// }
	//
	// @Override
	// protected String doInBackground(String... params) {
	//
	// String URL = "http://test.gardnerjames.eu.com/api/navitas-app/get-users";
	// JSONParserPost mJsonParserPost = new JSONParserPost();
	//
	//
	// Log.d("users encrypted=====", "" + encrypted_pin);
	// Log.d("users encrypted=====", "" + encrypted_probeid);
	//
	// HttpEntity se = null;
	// try {
	//
	// json = new JSONObject();
	// json.put("0", encrypted_probeid);
	// json.put("1", encrypted_pin);
	//
	// se = new StringEntity(json.toString());
	//
	// //Log.i("parent====", "" + json);
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	//
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(URL, se);
	//
	//
	//
	//
	// Log.i("mJsonObject string---------", "" + jsonobject.toString());
	//
	// try {
	// mString_status_user = jsonobject.getString("status");
	// //Log.i("mString_status_user===", "" + mString_status_user);
	// } catch (JSONException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// if (mString_status_user.equals("success")) {
	// try {
	// jsonArray = jsonobject.getJSONArray("result");
	// } catch (JSONException e1) {
	// // TODO Auto-generated catch block
	// e1.printStackTrace();
	// }
	// //Log.i("jsonArray---------", "" + jsonArray.length());
	//
	// try {
	// for (int i = 0; i < jsonArray.length(); i++) {
	// JSONObject jsonObject = jsonArray.getJSONObject(i);
	//
	// String mString_id = jsonObject.getString("id");
	// String mString_username = jsonObject.getString("name");
	// // String mString_email = jsonObject.getString("email");
	// String mString_image = jsonObject.getString("avatar");
	//
	// // mArrayList_username.add(mString_username);
	// //Log.i("mString_username:", "" + mString_username);
	//
	// mDatabaseHandler.Add_Listing(mString_id, null,
	// mString_username, mString_image);
	//
	// }
	//
	// } catch (Exception e) {
	//
	// }
	//
	// }
	//
	// return null;
	// }
	//
	// @Override
	// protected void onCancelled() {
	//
	// super.onCancelled();
	// }
	//
	// @Override
	// protected void onPostExecute(String result) {
	// // TODO Auto-generated method stub
	// super.onPostExecute(result);
	// // pDialog.cancel();
	// // pDialog.dismiss();
	// if (mString_status_user.equals("success")){
	// mProgress_ = new Progress_();
	// mProgress_.execute();
	// }
	// else{
	// AlertDialog.Builder alert = new
	// AlertDialog.Builder(getApplicationContext());
	// alert.setTitle("Do you want to Continous please enable the probe in website?");
	// // alert.setMessage("Message");
	//
	// alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	// public void onClick(DialogInterface dialog, int whichButton) {
	// dialog.cancel();
	// new Progress_List().execute();
	// }
	// });
	//
	// alert.setNegativeButton("Cancel",
	// new DialogInterface.OnClickListener() {
	// public void onClick(DialogInterface dialog, int whichButton) {
	// dialog.cancel();
	// }
	// });
	//
	// alert.show();
	// }
	//
	//
	// }
	//
	// @Override
	// protected void onPreExecute() {
	//
	// // pDialog.show();
	// // pDialog.setCancelable(false);
	//
	// super.onPreExecute();
	//
	// }
	//
	// @Override
	// protected void onProgressUpdate(Void... values) {
	//
	// super.onProgressUpdate(values);
	//
	// }
	//
	// }
	//
	// public class Progress_ extends AsyncTask<String, Void, String> {
	// public Progress_() {
	// }
	//
	// @Override
	// protected String doInBackground(String... params) {
	//
	// String URL = "http://brstdev.com/caczcall/calendar/navitasapp/menu.php";
	//
	// JSONParserPost mJsonParserPost = new JSONParserPost();
	//
	//
	// Log.d("menu encrypted=====", "" + encrypted_pin);
	// Log.d("menu encrypted=====", "" + encrypted_probeid);
	//
	// HttpEntity se = null;
	// try {
	//
	// json = new JSONObject();
	// json.put("0", encrypted_probeid);
	// json.put("1", encrypted_pin);
	//
	// se = new StringEntity(json.toString());
	//
	// //Log.i("parent====", "" + json);
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	//
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(URL, se);
	//
	//
	//
	//
	// Log.i("mJsonObject string---------", "" + jsonobject.toString());
	//
	// try {
	// jsonArray = jsonobject.getJSONArray("result");
	// } catch (JSONException e1) {
	// // TODO Auto-generated catch block
	// e1.printStackTrace();
	// }
	// //Log.i("jsonArray---------", ""+jsonArray.length());
	//
	//
	// try {
	// for (int i = 0; i < jsonArray.length(); i++) {
	// JSONObject jsonObject = jsonArray.getJSONObject(i);
	//
	// String mString_localmanagerid = jsonObject.getString("locmanagerid");
	// String mString_item = jsonObject.getString("fooditem");
	// String mString_username = jsonObject.getString("foodgroup");
	// String mString_groupid = jsonObject.getString("foodgroupid");
	// String mString_itemid= jsonObject.getString("fooditemid");
	//
	//
	// mDatabaseHandler.Add_Food(mString_localmanagerid, mString_item,
	// mString_itemid,mString_username,mString_groupid);
	//
	//
	//
	//
	// }
	//
	//
	//
	// } catch (Exception e) {
	//
	// }
	//
	//
	//
	// // try {
	// // mString_status_menu = jsonobject.getString("status");
	// // //Log.i("mString_status===", "" + mString_status_menu);
	// // } catch (JSONException e) {
	// // // TODO Auto-generated catch block
	// // e.printStackTrace();
	// // }
	// // try {
	// // String mString_message_menu = jsonobject.getString("message");
	// // //Log.i("mString_message===", "" + mString_message_menu);
	// // } catch (JSONException e) {
	// // // TODO Auto-generated catch block
	// // e.printStackTrace();
	// // }
	// // if (mString_status_menu.equals("success")) {
	// // try {
	// // jsonArray = jsonobject.getJSONArray("result");
	// // try {
	// // for (int i = 0; i < jsonArray.length(); i++) {
	// // JSONObject jsonObject = jsonArray.getJSONObject(i);
	// //
	// // String mString_new_group_id = jsonObject
	// // .getString("id");
	// // String mString_new_group_name = jsonObject
	// // .getString("name");
	// // json_Array = jsonObject.getJSONArray("children");
	// // //Log.i("json_Array==", "" + json_Array.length());
	// // for (int j = 0; j < json_Array.length(); j++) {
	// // JSONObject json_Object = json_Array
	// // .getJSONObject(j);
	// // String name_inside = json_Object
	// // .getString("name");
	// // String id_inside = json_Object.getString("id");
	// //
	// // //Log.i("name_inside==", "" + name_inside);
	// // //Log.i("id_inside==", "" + id_inside);
	// // mDatabaseHandler.Add_Listing_Item(
	// // mString_new_group_id, name_inside,
	// // id_inside);
	// //
	// // }
	// //
	// // mDatabaseHandler.Add_Food(null, null, null,
	// // mString_new_group_name,
	// // mString_new_group_id);
	// //
	// // }
	// //
	// // } catch (Exception e) {
	// //
	// // }
	// // } catch (JSONException e) {
	// // // TODO Auto-generated catch block
	// // e.printStackTrace();
	// // }
	// // //Log.i("jsonArray---------", "" + jsonArray.length());
	// // }
	//
	// return null;
	// }
	//
	// @Override
	// protected void onCancelled() {
	//
	// super.onCancelled();
	// }
	//
	// @Override
	// protected void onPostExecute(String result) {
	// // TODO Auto-generated method stub
	// super.onPostExecute(result);
	//
	// mProgress_Supplier = new Progress_Supplier();
	// mProgress_Supplier.execute();
	//
	// }
	//
	// @Override
	// protected void onPreExecute() {
	//
	// super.onPreExecute();
	//
	// }
	//
	// @Override
	// protected void onProgressUpdate(Void... values) {
	//
	// super.onProgressUpdate(values);
	//
	// }
	//
	// }
	//
	// public class Progress_Supplier extends AsyncTask<String, Void, String> {
	// public Progress_Supplier() {
	// }
	//
	// @Override
	// protected String doInBackground(String... params) {
	//
	// String URL =
	// "http://test.gardnerjames.eu.com/api/navitas-app/get-suppliers";
	//
	// JSONParserPost mJsonParserPost = new JSONParserPost();
	//
	//
	// Log.d("Suppliers encrypted=====", "" + encrypted_pin);
	// Log.d("Suppliers encrypted=====", "" + encrypted_probeid);
	//
	// HttpEntity se = null;
	// try {
	//
	// json = new JSONObject();
	// json.put("0", encrypted_probeid);
	// json.put("1", encrypted_pin);
	//
	// se = new StringEntity(json.toString());
	//
	// //Log.i("parent====", "" + json);
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	//
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(URL, se);
	//
	//
	//
	//
	// Log.i("mJsonObject string---------", "" + jsonobject.toString());
	//
	// // try {
	// // mString_status_supplier = jsonobject.getString("status");
	// // //Log.i("mString_status===", "" + mString_status_menu);
	// // } catch (JSONException e) {
	// // // TODO Auto-generated catch block
	// // e.printStackTrace();
	// // }
	// //
	// // if (mString_status_supplier.equals("success")) {
	// //
	// // try {
	// // jsonArray = jsonobject.getJSONArray("result");
	// // //Log.i("jsonArray---------", "" + jsonArray.length());
	// // try {
	// // for (int i = 0; i < jsonArray.length(); i++) {
	// // JSONObject jsonObject = jsonArray.getJSONObject(i);
	// //
	// // String mString_email = jsonObject.getString("id");
	// // String mString_username = jsonObject
	// // .getString("name");
	// // String mString_image = jsonObject
	// // .getString("avatar");
	// // mDatabaseHandler.Add_Supplier(mString_email,
	// // mString_username, mString_image);
	// //
	// // }
	// //
	// // } catch (Exception e) {
	// //
	// // }
	// // } catch (JSONException e1) {
	// // // TODO Auto-generated catch block
	// // e1.printStackTrace();
	// // }
	// //
	// // }
	//
	// return null;
	// }
	//
	// @Override
	// protected void onCancelled() {
	//
	// super.onCancelled();
	// }
	//
	// @Override
	// protected void onPostExecute(String result) {
	// // TODO Auto-generated method stub
	// super.onPostExecute(result);
	// pDialog.dismiss();
	// pDialog.cancel();
	// Intent intent_home_screen = new Intent(Pin_Screen.this,
	// MainMenu.class);
	//
	// startActivity(intent_home_screen);
	// finish();
	//
	// }
	//
	// @Override
	// protected void onPreExecute() {
	//
	// super.onPreExecute();
	//
	// }
	//
	// @Override
	// protected void onProgressUpdate(Void... values) {
	//
	// super.onProgressUpdate(values);
	//
	// }
	//
	// }

	private void show_alert(String message, final boolean value) {
		// TODO Auto-generated method stub
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				Pin_Screen.this);

		// Setting Dialog Title
		alertDialog.setTitle("Error!");

		// Setting Dialog Message
		alertDialog.setMessage(message);

		// Setting Icon to Dialog
		alertDialog.setIcon(R.drawable.ic_launcher);

		// Setting Positive "Yes" Button
		alertDialog.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
			
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

}
