package esb.navitas.smartherm;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import esb.navitas.smartherm.CustomFastScrollView.SectionIndexer;

public class CountryAdapter extends ArrayAdapter<String> implements
		SectionIndexer {

	private Context context;
	private int textViewResourceId;
	private ArrayList<String> objects;

	public CountryAdapter(Context context, int textViewResourceId,
			ArrayList<String> objects) {
		super(context, textViewResourceId, objects);
		this.context = context;
		this.textViewResourceId = textViewResourceId;
		this.objects = objects;
	}

	public int getPositionForSection(int section) {
		// TODO Auto-generated method stub
		return objects.size();
	}

	public int getSectionForPosition(int position) {
		// TODO Auto-generated method stub
		return objects.size();
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		ViewHolder holder;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(textViewResourceId, parent, false);
			holder = new ViewHolder();

			holder.text = (TextView) view.findViewById(android.R.id.text1);
			holder.text.setTextColor(Color.BLACK);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		holder.text.setText(objects.get(position));

		return view;
	}

	public Object[] getSections() {
		// TODO Auto-generated method stub
		return null;
	}

	public class ViewHolder {
		public TextView text;

	}

}
